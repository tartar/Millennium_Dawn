ideas = {

	Infantry_Weapon_Company = {

		designer = yes

		ITA_beretta_infantry_weapon_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_beretta_infantry_weapon_company" }

			picture = Beretta_ITA
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.186
			}

			traits = {
				Cat_INF_WEP_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	Vehicle_Company = {

		designer = yes

		ITA_iveco_vehicle_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_iveco_vehicle_company" }

			picture = Iveco_ITA

			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.186
			}

			traits = {
				Cat_ARMOR_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	Helicopter_Company = {

		designer = yes

		ITA_agustawestland_helicopter_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_agustawestland_helicopter_company" }

			picture = AgustaWestland_ITA
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.217
			}

			traits = {
				Cat_HELI_7

			}
			ai_will_do = {
				factor = 1
			}
		}

		ITA_airbus_helicopters_helicopter_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_airbus_helicopters_helicopter_company" }
			picture = Airbus_Helicopters_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = { Cat_HELI_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Ship_Company = {

		designer = yes

		ITA_fincantieri_ship_company = {
			allowed = { original_tag = ITA NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_ship_company" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.248
			}

			traits = { Cat_NAVAL_EQP_8 }
			ai_will_do = {
				factor = 1
			}
		}
		ITA_fincantieri_ship_company_mtg = {
			allowed = { original_tag = ITA  has_dlc = "Man the Guns"  }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_ship_company" }
			name = ITA_fincantieri_ship_company
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.248
			}

			traits = { Cat_NAVAL_EQP_8_MTG }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Submarine_Company = {

		designer = yes

		ITA_fincantieri_submarine_company = {
			allowed = { original_tag = ITA NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_submarine_company" }
			picture = Fincantieri_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.248
			}

			traits = { Cat_D_SUB_8 }
			ai_will_do = {
				factor = 1
			}
		}
		ITA_fincantieri_submarine_company_mtg = {
			allowed = { original_tag = ITA  has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_fincantieri_submarine_company" }
			picture = Fincantieri_ITA
			name = ITA_fincantieri_submarine_company
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.248
			}

			traits = { Cat_D_SUB_8_MTG }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Aircraft_Company = {

		designer = yes

		ITA_alenia_aeronautica_aircraft_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_alenia_aeronautica_aircraft_company" }
			picture = Alenia_Aeronautica_ITA
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_FIXED_WING = 0.217
			}

			traits = {
				Cat_L_Fighter_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	Aircraft_Company = {

		designer = yes

		ITA_airbus_defence_aircraft_company = {
			allowed = { original_tag = ITA }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_airbus_defence_aircraft_company" }
			picture = Airbus_Defence_ITA
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.279
			}

			traits = {
				CAT_FIXED_WING_9

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

}

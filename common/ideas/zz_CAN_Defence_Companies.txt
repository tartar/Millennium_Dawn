ideas = {

	Infantry_Weapon_Company = {
		designer = yes
		CAN_diemaco_infantry_weapon_company = {
			allowed = { original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_diemaco_infantry_weapon_company" }
			picture = Diemaco_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.217
			}

			traits = { Cat_INF_WEP_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Vehicle_Company = {

		designer = yes

		CAN_general_dynamics_land_systems_vehicle_company = {
			allowed = { original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_general_dynamics_land_systems_vehicle_company" }
			picture = General_Dynamics_Land_Systems_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.248
			}

			traits = { Cat_ARMOR_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Ship_Company = {
		designer = yes
		CAN_marine_industries_limited_ship_company = {
			allowed = { NOT = { has_dlc = "Man the Guns" } original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_marine_industries_limited_ship_company" }

			picture = Marine_Industries_Limited_CAN
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = { Cat_NAVAL_EQP_6 }
			ai_will_do = {
				factor = 1
			}
		}
		CAN_marine_industries_limited_ship_company_mtg = {
			allowed = { has_dlc = "Man the Guns" original_tag = CAN }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea CAN_marine_industries_limited_ship_company" }
			picture = Marine_Industries_Limited_CAN
			name = CAN_marine_industries_limited_ship_company
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = { Cat_NAVAL_EQP_6_MTG }
			ai_will_do = {
				factor = 1
			}
		}
	}

}

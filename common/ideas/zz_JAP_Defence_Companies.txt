ideas = {
	Vehicle_Company = {
		designer = yes
		JAP_komatsu_vehicle_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_komatsu_vehicle_company" }
			picture = Komatsu_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_AFV = 0.248
			}

			traits = { Cat_AFV_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_mitsubishi_heavy_industries_vehicle_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_heavy_industries_vehicle_company" }
			picture = Mitsubishi_Heavy_Industries_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_ARMOR = 0.248
			}

			traits = { Cat_ARMOR_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Ship_Company = {
		designer = yes
		JAP_mitsubishi_shipbuilding_ship_company = {
			allowed = { original_tag = JAP NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_ship_company" }
			picture = Mitsubishi_Shipbuilding_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_SURFACE_SHIP = 0.217
			}

			traits = { Cat_SURFACE_SHIP_7 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_mitsubishi_shipbuilding_ship_company_mtg = {
			allowed = { original_tag = JAP has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_ship_company" }
			picture = Mitsubishi_Shipbuilding_JAP
			name = JAP_mitsubishi_shipbuilding_ship_company
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_SURFACE_SHIP = 0.217
			}

			traits = { Cat_SURFACE_SHIP_7_MTG }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_kawasaki_shipbuilding_ship_company = {
			allowed = { original_tag = JAP NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_ship_company" }
			picture = Kawasaki_Shipbuilding_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.279
			}

			traits = { Cat_NAVAL_EQP_9 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_shipbuilding_ship_company_mtg = {
			allowed = { original_tag = JAP has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_ship_company" }
			picture = Kawasaki_Shipbuilding_JAP
			name = JAP_kawasaki_shipbuilding_ship_company
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.279
			}

			traits = { Cat_NAVAL_EQP_9_MTG }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_ihi_corporation_ship_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_ihi_corporation_ship_company" }
			picture = IHI_Corporation_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.248
			}

			traits = { Cat_TRANS_SHIP_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Submarine_Company = {

		designer = yes

		JAP_mitsubishi_shipbuilding_submarine_company = {
			allowed = { original_tag = JAP NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_submarine_company" }
			picture = Mitsubishi_Shipbuilding_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.217
			}

			traits = { Cat_D_SUB_7 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_mitsubishi_shipbuilding_submarine_company_mtg = {
			allowed = { original_tag = JAP has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_shipbuilding_submarine_company" }
			picture = Mitsubishi_Shipbuilding_JAP
			name = JAP_mitsubishi_shipbuilding_submarine_company
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.217
			}

			traits = { Cat_D_SUB_7_MTG }
			ai_will_do = {
				factor = 1
			}
		}

		JAP_kawasaki_shipbuilding_submarine_company = {
			allowed = { original_tag = JAP NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_submarine_company" }
			picture = Kawasaki_Shipbuilding_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.279
			}

			traits = { Cat_D_SUB_9 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_shipbuilding_submarine_company_mtg = {
			allowed = { original_tag = JAP has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_shipbuilding_submarine_company" }
			name = JAP_kawasaki_shipbuilding_submarine_company
			picture = Kawasaki_Shipbuilding_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_D_SUB = 0.279
			}

			traits = { Cat_D_SUB_9_MTG }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Aircraft_Company = {
		designer = yes
		JAP_mitsubishi_aerospace_aircraft_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_aerospace_aircraft_company" }
			picture = Mitsubishi_Aerospace_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_FIXED_WING = 0.248
			}

			traits = { CAT_FIXED_WING_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_aerospace_company_aircraft_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_aerospace_company_aircraft_company" }
			picture = Kawasaki_Aerospace_Company_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_H_AIR = 0.217
			}

			traits = { Cat_H_AIR_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Helicopter_Company = {

		designer = yes

		JAP_mitsubishi_aerospace_helicopter_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_mitsubishi_aerospace_helicopter_company" }
			picture = Mitsubishi_Aerospace_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = { Cat_HELI_8 }
			ai_will_do = {
				factor = 1
			}
		}
		JAP_kawasaki_aerospace_company_helicopter_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_kawasaki_aerospace_company_helicopter_company" }
			picture = Kawasaki_Aerospace_Company_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_HELI = 0.217
			}

			traits = { Cat_HELI_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	Infantry_Weapon_Company = {
		designer = yes
		JAP_howa_machinery_infantry_weapon_company = {
			allowed = { original_tag = JAP }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea JAP_howa_machinery_infantry_weapon_company" }
			picture = Howa_Machinery_JAP
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_INF_WEP = 0.248
			}

			traits = { Cat_INF_WEP_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}
}

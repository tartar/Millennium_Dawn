ideas = {
	Vehicle_Company = {
		designer = yes
		HOL_dutch_defense_vehicle_systems_vehicle_company = {
			allowed = { original_tag = HOL }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_dutch_defense_vehicle_systems_vehicle_company" }
			picture = Dutch_Defense_Vehicle_Systems_HOL
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_AFV = 0.186
			}

			traits = {
				Cat_AFV_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	Ship_Company = {

		designer = yes

		HOL_damen_shipyards_ship_company = {
			allowed = { original_tag = HOL NOT = { has_dlc = "Man the Guns" } }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_damen_shipyards_ship_company" }
			picture = Damen_Shipyards_HOL
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = { Cat_NAVAL_EQP_6 }
			ai_will_do = {
				factor = 1
			}
		}
		HOL_damen_shipyards_ship_company_mtg = {
			allowed = { original_tag = HOL has_dlc = "Man the Guns" }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea HOL_damen_shipyards_ship_company" }
			name = HOL_damen_shipyards_ship_company
			picture = Damen_Shipyards_HOL
			cost = 150
			removal_cost = 10
			research_bonus = {
				Cat_NAVAL_EQP = 0.186
			}

			traits = { Cat_NAVAL_EQP_6 }
			ai_will_do = {
				factor = 1
			}
		}
	}

}

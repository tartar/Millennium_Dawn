
ideas = {

		hidden_ideas = {
			ITA_wasted_foreign_funds = {
				allowed = { always = no }
				on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_wasted_foreign_funds" }
			}

			ITA_mafia = {
				on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_mafia" }
				on_remove = { log = "[GetDateText]: [Root.GetName]: remove idea ITA_mafia" }
				allowed = {
					original_tag = ITA
				}
			}
		}

	country = {

		ITA_north_miracle_industry = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_north_miracle_industry" }

			allowed = {
				always = no
			}

			picture = production_bonus

			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_arms_factory_factor = 0.1
			}
		}

		ITA_victory_over_mafia = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_victory_over_mafia" }

			allowed = {
				original_tag = ITA
			}

			picture = matignon_agreements

			modifier = {
				stability_factor = 0.2
				political_power_factor = 0.2
			}
		}

		ITA_parliament_dismantled = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_parliament_dismantled"
				stop_elections = yes
			}
			on_remove = {
				allow_elections = yes
			}

			allowed = {
				always = no
			}

			picture = fascism2

			modifier = {
				stability_factor = -0.15
				political_power_factor = 0.3
			}
		}

		ITA_kingdom_restored = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_kingdom_restored" }

			allowed = {
				always = no
			}

			picture = monarchy

			modifier = {
				army_org_Factor = 0.05
				nationalist_drift = 0.05
				justify_war_goal_time = -0.1
				conscription = 0.005
			}
		}

		ITA_divide_et_impera = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_divide_et_impera" }
			picture = fascism3

			allowed = {
				always = no
			}

			modifier = {
				justify_war_goal_time = -0.5
				army_org_Factor = 0.1
				army_attack_factor = 0.1
				army_defence_factor = 0.1
			}
		}

		ITA_peaceful_takeovers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_peaceful_takeovers" }
			picture = fascism3

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.3
				political_power_gain = 0.3
				subjects_autonomy_gain = -3
			}
		}

		ITA_new_march_on_rome = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_new_march_on_rome" }

			picture = fascism2

			allowed = {
				always = no
			}

			modifier = {
				justify_war_goal_time = -0.2
				army_org_Factor = 0.05
				stability_factor = 0.05
				nationalist_drift = 0.05
				conscription = 0.005
			}
		}

		ITA_southern_question = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_southern_question" }

			picture = local_self_management
			allowed = {
				always = no
			}
			modifier = {
				political_power_factor = -0.1
				stability_factor = -0.2
			}
		}

		ITA_migrants_from_africa = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_migrants_from_africa" }

			allowed = {
				always = no
			}

			picture = production_bonus

			modifier = {
				MONTHLY_POPULATION = 0.05
				consumer_goods_factor = -0.01
				stability_weekly = -0.001
			}

		}

		ITA_migrants_from_africa_encouraged = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_migrants_from_africa_encouraged" }

			allowed = {
				always = no
			}

			picture = agriculture

			modifier = {
				MONTHLY_POPULATION = 0.2
				consumer_goods_factor = -0.02
				stability_weekly = -0.002
			}

		}

		ITA_migrants_from_africa_limited = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_migrants_from_africa_limited" }

			allowed = {
				always = no
			}

			picture = factory_strikes

			modifier = {
				MONTHLY_POPULATION = 0.01
				consumer_goods_factor = 0.01
				stability_factor = 0.05
			}
		}

		ITA_migration_tolerated = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_migration_tolerated" }

			allowed = {
				always = no
			}

			picture = factory_strikes

			modifier = {
				MONTHLY_POPULATION = 0.025
				stability_factor = -0.05
			}
		}

		ITA_restore_the_currency_idea = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_restore_the_currency_idea"
				add_to_variable = { bond_markets_trust = 1 }
			}
			on_remove = {
				add_to_variable = { bond_markets_trust = -1 }
			}

			picture = trade

			allowed = {
				always = no
			}

			modifier = {
				political_power_gain = 0.2
				consumer_goods_factor = 0.1
			}
		}

		ITA_quality_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_quality_army" }

			picture = morale_bonus

			allowed = {
				always = no
			}
			modifier = {
				army_leader_start_level = 1
				experience_gain_army_factor = 0.05
				army_morale_factor = 0.05
			}
		}

		ITA_quantity_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_quantity_army" }

			picture = manpower_bonus

			allowed = {
				always = no
			}
			modifier = {
				conscription = 0.04
			}
		}

		ITA_roman_integration = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_roman_integration" }
			allowed = { always = no }
			picture = roman_ide
			modifier = {
				political_power_gain = -0.25
				consumer_goods_factor = 0.10
			}
		}

		ITA_zombie_companies = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_zombie_companies" }
			allowed = { always = no }
			picture = inflation3
			modifier = {
				research_speed_factor = -0.1
				stability_factor = 0.3
				stability_weekly = -0.001
				consumer_goods_factor = 0.05
			}
		}

		ITA_dynamic_economy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_dynamic_economy" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_industrial_complex_factor = 0.25
				consumer_goods_factor = -0.02
				stability_factor = -0.2
				stability_weekly = 0.001
			}
		}

		ITA_flexible_labour_market = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_flexible_labour_market" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_industrial_complex_factor = 0.25
				consumer_goods_factor = -0.02
				stability_factor = -0.2
				stability_weekly = 0.001
			}
		}

		ITA_rigid_labour_market = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_rigid_labour_market" }
			allowed = { always = no }
			picture = great_depression
			modifier = {
				research_speed_factor = -0.1
				stability_factor = 0.4
				stability_weekly = -0.001
				production_speed_industrial_complex_factor = -0.1
			}
		}

		ITA_privatizations = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_privatizations" }
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				stability_factor = -0.2
				stability_weekly = 0.001
				consumer_goods_factor = -0.06
			}
		}

		ITA_IRI = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_IRI" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				political_power_factor = 0.20
				consumer_goods_factor = 0.06
				stability_factor = 0.2
				production_speed_buildings_factor = 0.25
				production_speed_industrial_complex_factor = -0.25
			}
		}

		ITA_election_tips = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_election_tips" }
			allowed = { always = no }
			picture = new_deal
			modifier = {
				democratic_drift = 0.01
				stability_weekly = 0.001
				consumer_goods_factor = 0.02
			}
		}

		ITA_election_tips_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_election_tips_2" }
			allowed = { always = no }
			picture = new_deal
			modifier = {
				communism_drift = 0.01
				stability_weekly = 0.001
				consumer_goods_factor = 0.02
			}
		}

		ITA_election_tips_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_election_tips_3" }
			allowed = { always = no }
			picture = new_deal
			modifier = {
				nationalist_drift = 0.01
				stability_weekly = 0.001
				consumer_goods_factor = 0.02
			}
		}

		ITA_inefficient_administration_4 = {
			name = ITA_inefficient_administration
			allowed = { always = no }
			picture = puppet_industry
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_inefficient_administration_4"
				add_political_power = 500
			}
			modifier = {
				political_power_factor = -0.20
				command_power_gain_mult = -0.5
				consumer_goods_factor = 0.04
				production_speed_buildings_factor = -0.2
				stability_weekly = -0.002
			}
		}

		ITA_inefficient_administration_3 = {
			name = ITA_inefficient_administration
			allowed = { always = no }
			picture = puppet_industry
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_inefficient_administration_3"
				ingame_calculate_size_modifier = yes
				update_bureaucracy_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			modifier = {
				political_power_factor = -0.15
				command_power_gain_mult = -0.375
				consumer_goods_factor = 0.03
				production_speed_buildings_factor = -0.15
				stability_weekly = -0.001
			}
		}

		ITA_inefficient_administration_2 = {
			name = ITA_inefficient_administration
			allowed = { always = no }
			picture = puppet_industry
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_inefficient_administration_2"
				ingame_calculate_size_modifier = yes
				update_bureaucracy_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
				}
			modifier = {
				political_power_factor = -0.10
				command_power_gain_mult = -0.25
				consumer_goods_factor = 0.02
				production_speed_buildings_factor = -0.1
				stability_weekly = -0.001
			}
		}

		ITA_inefficient_administration_1 = {
			name = ITA_inefficient_administration
			allowed = { always = no }
			picture = puppet_industry
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_inefficient_administration_1"
				ingame_calculate_size_modifier = yes
				update_bureaucracy_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			on_remove = {
				ingame_calculate_size_modifier = yes
				update_bureaucracy_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			modifier = {
				political_power_factor = -0.05
				command_power_gain_mult = -0.125
				consumer_goods_factor = 0.01
				production_speed_buildings_factor = -0.05
			}
		}

		ITA_legalized_light_drugs = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_legalized_light_drugs"
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.02 }
				ingame_calculate_size_modifier = yes
				update_police_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			on_remove = {
				log = "[GetDateText]: [Root.GetName]: remove idea ITA_legalized_light_drugs"
				add_to_variable = { var = ITA_tax_multiplier_var value = -0.02 }
				ingame_calculate_size_modifier = yes
				update_police_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				stability_weekly = 0.001
			}
		}

		ITA_legalized_all_drugs = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_legalized_all_drugs"
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.03 }
				ingame_calculate_size_modifier = yes
				update_police_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			on_remove = {
				log = "[GetDateText]: [Root.GetName]: remove idea ITA_legalized_all_drugs"
				add_to_variable = { var = ITA_tax_multiplier_var value = -0.03 }
				ingame_calculate_size_modifier = yes
				update_police_rate = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			allowed = { always = no }
			picture = morale_bonus
			modifier = {
				stability_factor = -0.15
				stability_weekly = 0.002
			}
		}

		ITA_unsustainable_pension_system_1 = {
			name = ITA_unsustainable_pension_system
			allowed = { always = no }
			picture = inflation
			modifier = {
				stability_factor = 0.1
				stability_weekly = -0.001
				consumer_goods_factor = 0.02
			}
		}

		ITA_unsustainable_pension_system_2 = {
			name = ITA_unsustainable_pension_system
			allowed = { always = no }
			picture = inflation2
			modifier = {
				stability_factor = 0.2
				stability_weekly = -0.002
				consumer_goods_factor = 0.04
			}
		}

		ITA_unsustainable_pension_system_3 = {
			name = ITA_unsustainable_pension_system
			allowed = { always = no }
			picture = inflation3
			modifier = {
				stability_factor = 0.3
				stability_weekly = -0.002
				consumer_goods_factor = 0.06
			}
		}

		ITA_unsustainable_pension_system_4 = {
			name = ITA_unsustainable_pension_system
			allowed = { always = no }
			picture = inflation4
			modifier = {
				stability_factor = 0.4
				stability_weekly = -0.003
				consumer_goods_factor = 0.08
			}
		}

		ITA_outdated_education_1 = {
			name = ITA_outdated_education
			allowed = { always = no }
			picture = international_treaty2
			modifier = {
				stability_factor = 0.15
				production_speed_industrial_complex_factor = -0.1
				research_speed_factor = -0.1
				consumer_goods_factor = -0.02
			}
		}

		ITA_outdated_education_2 = {
			name = ITA_outdated_education
			allowed = { always = no }
			picture = international_treaty2
			modifier = {
				stability_factor = 0.25
				production_speed_industrial_complex_factor = -0.2
				research_speed_factor = -0.2
				consumer_goods_factor = -0.04
			}
		}

		ITA_modern_education_2 = {
			name = ITA_modern_education
			allowed = { always = no }
			picture = research_bonus
			modifier = {
				stability_factor = -0.15
				production_speed_industrial_complex_factor = 0.2
				research_speed_factor = 0.2
				consumer_goods_factor = 0.02
			}
		}

		ITA_modern_education_1 = {
			name = ITA_modern_education
			allowed = { always = no }
			picture = research_bonus
			modifier = {
				stability_factor = -0.05
				production_speed_industrial_complex_factor = 0.1
				research_speed_factor = 0.1
				consumer_goods_factor = 0.01
			}
		}

		ITA_building_nuclear_reactors = {
			allowed = { always = no }
			picture = construction
			modifier = {
				stability_factor = -0.03
			}
			on_remove = {
			    add_ideas = ITA_nuclear_energy
			}
		}

		ITA_nuclear_energy = {
			allowed = { always = no }
			picture = nuclear_energy
			modifier = {
				production_speed_buildings_factor = 0.25
				research_speed_factor = 0.3
				consumer_goods_factor = -0.05
				MONTHLY_POPULATION = 0.1
			}
		}

		ITA_green_subsidies = {
			allowed = { always = no }
			picture = solar_energy
			modifier = {
				research_speed_factor = 0.1
				consumer_goods_factor = 0.05
				MONTHLY_POPULATION = 0.05
			}
		}

		ITA_gmos_encouraged = {
			allowed = { always = no }
			picture = agriculture
			modifier = {
				research_speed_factor = 0.1
				consumer_goods_factor = -0.02
				MONTHLY_POPULATION = 0.3
			}
		}

		ITA_gmos_banned = {
			allowed = { always = no }
			picture = agriculture
			modifier = {
				research_speed_factor = -0.1
				consumer_goods_factor = 0.01
				MONTHLY_POPULATION = -0.1
			}
		}

		ITA_happy_degrowth_spirit = {
			allowed = { always = no }
			picture = agriculture
			modifier = {
				research_speed_factor = -0.25
				consumer_goods_factor = -0.15
				MONTHLY_POPULATION = -0.25
				production_speed_buildings_factor = -0.5
			}
		}

		ITA_capital_flight = {
			on_add = {
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.01 }
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			on_remove = {
				add_to_variable = { var = ITA_tax_multiplier_var value = -0.01 }
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			allowed = { always = no }
			picture = financial_crisis
			modifier = {
				stability_factor = 0.3
				stability_weekly = -0.001
				consumer_goods_factor = 0.03
				production_speed_industrial_complex_factor = -0.2
			}
		}

		ITA_flat_tax = {
			allowed = { always = no }
			picture = landowners
			modifier = {
				stability_factor = 0.05
				consumer_goods_factor = -0.03
			}
		}

		ITA_carbon_tax = {
			allowed = { always = no }
			picture = sustainable_development
			modifier = {
				consumer_goods_factor = 0.02
				production_speed_industrial_complex_factor = -0.2
			}
		}

		ITA_high_minimum_wage = {
			allowed = { always = no }
			picture = labor_unions
			modifier = {
				consumer_goods_factor = 0.02
				production_speed_industrial_complex_factor = -0.1
				MONTHLY_POPULATION = 0.2
				stability_factor = 0.3
				stability_weekly = -0.001
			}
		}

		ITA_tourist_destination = {
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				consumer_goods_factor = -0.04
				stability_weekly = 0.001
			}
		}

		ITA_international_tourist_destination = {
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				consumer_goods_factor = -0.08
				stability_weekly = 0.002
			}
		}

		ITA_underdeveloped_south_1 = {
		    name = ITA_underdeveloped_south
			allowed = { always = no }
			picture = risk_of_famine
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_underdeveloped_south_2"
			}
			modifier = {
				consumer_goods_factor = 0.02
				stability_weekly = -0.002
				production_speed_industrial_complex_factor = -0.2
				neutrality_drift = 0.06
			}
		}

		ITA_underdeveloped_south_2 = {
		    name = ITA_underdeveloped_south
			allowed = { always = no }
			picture = risk_of_famine
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_underdeveloped_south_1"
			}
			modifier = {
				consumer_goods_factor = 0.01
				stability_weekly = -0.001
				stability_factor = -0.1
				production_speed_industrial_complex_factor = -0.1
				neutrality_drift = 0.03
			}
		}

		ITA_brain_drain = {
			allowed = { always = no }
			picture = low_popular_support
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_brain_drain"
			}
			modifier = {
				MONTHLY_POPULATION = -0.1
				research_speed_factor = -0.2
				production_speed_industrial_complex_factor = -0.1
				democratic_drift = -0.02
			}
		}

		ITA_tax_evasion = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_tax_evasion"
			}
			on_remove = {
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.1 }
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			allowed = { always = no }
			picture = hyper_inflation4
			modifier = {
				stability_weekly = -0.001
				stability_factor = 0.1
			}
		}

		ITA_building_abuse_1 = {
			name = ITA_building_abuse
			allowed = { always = no }
			picture = construction
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_building_abuse"
			}
			on_remove = {
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.1 }
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			modifier = {
				consumer_goods_factor = 0.02
			}
		}

		ITA_building_abuse_2 = {
			name = ITA_building_abuse
			allowed = { always = no }
			picture = construction
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_building_abuse"
			}
			on_remove = {
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			modifier = {
				political_power_gain = 0.20
			}
		}

		ITA_debt_market_trust = {
			allowed = { always = no }
			picture = hyper_inflation3
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_debt_market_trust"
			}
			on_remove = {
				ingame_calculate_size_modifier = yes
				calculate_interest_rate = yes
				calculate_debt_rate = yes
			}
			modifier = {
			}
		}

		ITA_closed_nation = {
			picture = political_repression
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_closed_nation"
			}
			modifier = {
				political_power_factor = 0.25
				democratic_drift = -0.1
				nationalist_drift = -0.1
				drift_defence_factor = 0.1
				encryption_factor = 0.1
				stability_factor = 0.125
				research_speed_factor = -0.2
			}
		}

		ITA_citizenship_income_spirit = {
			allowed = { always = no }
			picture = inflation5
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_citizenship_income_spirit"
			}
			on_remove = {
			}
			modifier = {
				stability_factor = 0.3
				stability_weekly = -0.001
				consumer_goods_factor = 0.04
			}
		}

		ITA_royal_family = {
			allowed = { always = no }
			picture = triumphant_will
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_royal_family"
			}
			on_remove = {
			}
			modifier = {
				stability_factor = 0.2
				stability_weekly = 0.001
			}
		}

		ITA_vatican_support = {
			allowed = { always = no }
			picture = christian_idea
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_vatican_support"
			}
			on_remove = {
			}
			modifier = {
				political_power_factor = 0.15
				stability_weekly = 0.001
			}
		}

		ITA_bloated_local_administrations = {
			allowed = { always = no }
			picture = central_management
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_bloated_local_administrations"
			}
			on_remove = {
			}
			modifier = {
				stability_factor = 0.15
				consumer_goods_factor = 0.02
			}
		}

		ITA_party_approved_media_c = {
			allowed = { always = no }
			picture = communism8
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_party_approved_media"
			}
			on_remove = {
			}
			modifier = {
				political_power_factor = 0.3
				consumer_goods_factor = 0.01
				communism_drift = 0.1
				stability_weekly = 0.001
				drift_defence_factor = 0.5
			}
		}

		ITA_party_approved_media_f = {
			allowed = { always = no }
			picture = fascism5
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_party_approved_media"
			}
			on_remove = {
			}
			modifier = {
				political_power_factor = 0.3
				consumer_goods_factor = 0.01
				nationalist_drift = 0.1
				stability_weekly = 0.001
				drift_defence_factor = 0.5
			}
		}

		ITA_decentralized_nation = {
			allowed = { always = no }
			picture = political_drain
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_decentralized_nation"
				add_to_variable = { var = ITA_tax_multiplier_var value = -0.05 }
			}
			on_remove = {
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.05 }
			}
			modifier = {
				political_power_factor = -0.1
				consumer_goods_factor = -0.1
				conscription = -0.01
				stability_weekly = 0.001
			}
		}

		ITA_war_economy_spirit = {
			allowed = { always = no }
			picture = fascism4
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_war_economy_spirit"
			}
			on_remove = {
			}
			modifier = {
				consumer_goods_factor = 0.05
				conscription = 0.005
				production_speed_arms_factory_factor = 0.25
				production_speed_industrial_complex_factor = -0.25
				local_resources_factor = 0.25
				production_factory_max_efficiency_factor = 0.25
				production_factory_efficiency_gain_factor = 0.25
				production_lack_of_resource_penalty_factor = -0.5
			}
		}

		ITA_economic_freedoms = {
			allowed = { always = no }
			picture = market_liberalism
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_economic_freedoms"
			}
			on_remove = {
			}
			modifier = {
				democratic_drift = 0.02
				production_speed_buildings_factor = 0.1
				conscription = -0.005
				stability_weekly = 0.001
			}
		}

		ITA_party_propaganda = {
			allowed = { always = no }
			picture = political_support
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_party_propaganda"
			}
			on_remove = {
			}
			modifier = {
				consumer_goods_factor = 0.01
				war_support_weekly = 0.002
				conscription = 0.005
			}
		}

		ITA_expropriations = {
			allowed = { always = no }
			picture = generic_purge
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_expropriations"
			}
			on_remove = {
			}
			modifier = {
				MONTHLY_POPULATION = -0.1
				stability_weekly = -0.002
				stability_factor = -0.2
			}
		}

		ITA_traditional_values = {
			allowed = { always = no }
			picture = low_popular_support3
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_traditional_values"
			}
			on_remove = {
			}
			modifier = {
				nationalist_drift = 0.02
				stability_weekly = 0.001
			}
		}

		ITA_legalized_prostitution = {
			allowed = { always = no }
			picture = consumer_goods
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_legalized_prostitution"
				add_to_variable = { var = ITA_tax_multiplier_var value = 0.01 }
				if = { limit = { has_idea = ITA_mafia }
					add_to_variable = { cosa_nostra_strength = -0.1 }
					add_to_variable = { camorra_strength = -0.1 }
					add_to_variable = { ndrangheta_strength = -0.1 }
					add_to_variable = { sacra_corona_unita_strength = -0.1 }
				}
			}
			on_remove = {
				add_to_variable = { var = ITA_tax_multiplier_var value = -0.01 }
				if = { limit = { has_idea = ITA_mafia }
					add_to_variable = { cosa_nostra_strength = 0.07 }
					add_to_variable = { camorra_strength = 0.07 }
					add_to_variable = { ndrangheta_strength = 0.07 }
					add_to_variable = { sacra_corona_unita_strength = 0.07 }
				}
			}
			modifier = {
				stability_factor = -0.1
				stability_weekly = 0.001
				consumer_goods_factor = -0.01
			}
		}

		ITA_legacy_of_the_PCI = {
			allowed = { always = no }
			picture = communism9
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_legacy_of_the_PCI"
			}
			on_remove = {
			}
			modifier = {
				communism_drift = 0.05
			}
		}

		ITA_war_with_Mafia = {
			allowed = { always = no }
			picture = infantry_bonus
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea ITA_war_with_Mafia"
			}
			on_remove = {
			}
			modifier = {
				command_power_gain = 0.5
				command_power_gain_mult = 1
				consumer_goods_factor = 0.05
				stability_factor = -0.1
			}
		}

		Planned_Economy_ITA = {

			picture = radical_leftist

			modifier = {
				research_speed_factor = -0.1
				production_factory_efficiency_gain_factor = -0.1
				production_speed_industrial_complex_factor = -0.1
				trade_opinion_factor = -0.1
				consumer_goods_factor = -0.1
				production_speed_arms_factory_factor = 0.1
				drift_defence_factor = 0.1
				corruption_cost_factor = -0.1
			}
		}

		###Space Stuff
		ITA_idea_ASI_failing = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_idea_ASI_failing" }
			allowed = { always = no }
			picture = consumer_goods
			modifier = {
				consumer_goods_factor = 0.02
				political_power_gain = -0.10
			}
		}
		ITA_idea_prep_the_launch = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_idea_prep_the_launch" }
			allowed = { always = no }
			on_remove = {
				set_country_flag = ITA_launch_ready
			}
			picture = central_management
			modifier = {
				consumer_goods_factor = 0.01
			}
		}
		ITA_idea_space_research = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_idea_space_research" }
			allowed = { always = no }
			picture = central_management
			modifier = {
				research_speed_factor = 0.10
				political_power_factor = 0.10
			}
		}
		ITA_moon_base = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ITA_moon_base" }
			allowed = { always = no }
			picture = central_management
			modifier = {
				research_speed_factor = 0.20
				political_power_factor = 0.20
			}
		}
	}
}
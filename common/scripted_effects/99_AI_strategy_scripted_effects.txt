ai_update_build_units = { #default is not to build units
	if = { #Stop building units
		limit = {
			NOT = {
				has_country_flag = ai_yes_unit_build
				has_country_flag = AI_is_threatened
			}
			OR = {
				has_war = yes
				if = {
					limit = { has_government = democratic }
					threat > 0.25
				}
				else_if = {
					limit = { has_government = neutrality }
					threat > 0.40
				}
				has_government = nationalist
				is_bad_salafist = yes
				any_other_country = {
					OR = {
						is_justifying_wargoal_against = THIS
						has_wargoal_against = THIS
					}
				}
			}
		}
		set_country_flag = ai_yes_unit_build
		add_to_variable = { var = ai_wants_divisions value = 500 }
		log = "[GetDateText]: [Root.GetName]: AI Update - AI Build Unit Flag Set"
	}
	if = {
		limit = {
			OR = {
				has_country_flag = ai_yes_unit_build #check will end here if they don't have the flag thus avoiding the expensive triggers below
				has_country_flag = AI_is_threatened
			}
			has_war = no
			if = {
				limit = { has_government = democratic }
				threat < 0.26
			}
			else_if = {
				limit = { has_government = neutrality }
				threat < 0.41
			}
			NOT = { has_government = nationalist }
			is_bad_salafist = no
			any_other_country = {
				NOT = {
					is_justifying_wargoal_against = this
					has_wargoal_against = this
				}
			}
		}
		clr_country_flag = ai_yes_unit_build
		if = {
			limit = { has_country_flag = AI_is_threatened }
			clr_country_flag = AI_is_threatened
		}
		log = "[GetDateText]: [Root.GetName]: AI Update - AI Build Unit Flag clear"
		add_to_variable = { var = ai_wants_divisions value = -500 } #maybe causing a crash? You can edit vanilla vars? idk who did this
	}
}

#Sample AI Test
effect_ai_test = {
	ai_update_build_units = yes
}
effect_AMERICA_CHINA = {
	declare_war_on = { target = CHI }
}
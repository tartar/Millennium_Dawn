# Written by Yard1
#reworked by Simone-Traiano

##Compendium of Combat Width
#Militia Battalion - 3 CW 			#Motorized Airborne Infantry - 4 CW
#Motorized Militia Battalion - 4 CW #Mechanized Airborne Infantry - 4 CW
#Light Infantry - 3 CW				#Armored Airborne Infantry - 4 CW
#Motorized Infantry - 4 CW			#Marine Light Infantry - 3 CW
#Mechanized Infantry - 4 CW			#Motorized Marine Infantry - 4 CW
#Armored Infantry - 4 CW			#Mechanized Marine Infantry - 4 CW
#Airborne Light Infantry - 3 CW		#Armored Marine Infantry - 4 CW
#Light Air Assault Infantry - 3 CW	#Armored Air Assault Infantry - 4 CW
#Special Forces - 3 CW				#Tank Battalion - 4 CW
# Light Recon - 3 CW				#Mechanized Recon - 4 CW
#Heavy Mechanized  Recon  - 4 CW	#Armored Recce - 4 CW
#Light Engineer - 2 CW				#Heavy Engineer - 4 CW
#Artillery - 3 CW					#SP Arty - 3 CW
#SP AA - 4 CW
#Companies - Support Companies
#Tank Company - 2 CW				#Light Recon - 1 CW
#Motorized Recon - 2 CW				#Mechanized Company - 2 CW
#Armored Recon - 2 CW				#Armored Recce - 2 CW
#Light Engineer - 1 CW				#Heavy Engineer - 2 CW
#Artillery BAttery - 1 CW			#SP Arty - 2 CW
#SP AA - 2 CWf

give_AI_templates = {
	if = {
		limit = {
			OR = {
				original_tag = USA
				original_tag = SOV
				original_tag = CHI
			}
		}
		generic_AI_templates = yes
		Major_Power_AI_templates = yes
	}
	else_if = {
		limit = { original_tag = ITA }
		generic_AI_templates = yes
		#Simone_likes_80_width_SOF = yes
	}
	else = {
		Militia_AI_templates = yes
		generic_AI_templates = yes
	}
}

generic_AI_templates = {
	#Light Infantry - Supports the L_Inf role
	division_template = {
		name = "AI Light Infantry Brigade"
		priority = 0
		regiments = {
			L_Inf_Bat = { x = 0 y = 0 }
			L_Inf_Bat = { x = 0 y = 1 }
			L_Inf_Bat = { x = 0 y = 2 }
			L_Inf_Bat = { x = 0 y = 3 }
			Arty_Bat = { x = 0 y = 4 }
			L_Inf_Bat = { x = 1 y = 0 }
			L_Inf_Bat = { x = 1 y = 1 }
			L_Inf_Bat = { x = 1 y = 2 }
			L_Inf_Bat = { x = 1 y = 3 }
			L_Engi_Bat = { x = 1 y = 4 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
		}
	}
	division_template = {
		name = "AI Light Infantry Division"
		priority = 0
		regiments = {
			L_Inf_Bat = { x = 0 y = 0 }
			L_Inf_Bat = { x = 0 y = 1 }
			L_Inf_Bat = { x = 0 y = 2 }
			L_Inf_Bat = { x = 0 y = 3 }
			L_Inf_Bat = { x = 0 y = 4 }
			L_Inf_Bat = { x = 1 y = 0 }
			L_Inf_Bat = { x = 1 y = 1 }
			L_Inf_Bat = { x = 1 y = 2 }
			L_Inf_Bat = { x = 1 y = 3 }
			L_Inf_Bat = { x = 1 y = 4 }
			L_Inf_Bat = { x = 2 y = 0 }
			L_Inf_Bat = { x = 2 y = 1 }
			L_Inf_Bat = { x = 2 y = 2 }
			L_Inf_Bat = { x = 2 y = 3 }
			L_Inf_Bat = { x = 2 y = 4 }
			Arty_Bat = { x = 3 y = 0 }
			Arty_Bat = { x = 3 y = 1 }
			L_Engi_Bat = { x = 3 y = 2 }
			L_Engi_Bat = { x = 3 y = 3 }
			L_Recce_Bat = { x = 3 y = 4 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
			L_Engi_Comp = { x = 0 y = 1 }
		}
	}

	#Light Marine Brigades - Supports the marines Roles
	division_template = {
		name = "AI Light Marine Brigade"
		priority = 1
		regiments = {
			L_Marine_Bat = { x = 0 y = 0 }
			L_Marine_Bat = { x = 0 y = 1 }
			L_Marine_Bat = { x = 0 y = 2 }
			L_Marine_Bat = { x = 1 y = 0 }
			L_Marine_Bat = { x = 1 y = 1 }
			L_Marine_Bat = { x = 1 y = 2 }
			L_Marine_Bat = { x = 2 y = 0 }
			L_Marine_Bat = { x = 2 y = 1 }
			L_Marine_Bat = { x = 2 y = 2 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
			L_Engi_Comp = { x = 0 y = 1 }
			Arty_Battery = { x = 0 y = 2 }
		}
	}
	division_template = {
		name = "AI Light Marine Division"
		priority = 1
		regiments = {
			L_Marine_Bat = { x = 0 y = 0 }
			L_Marine_Bat = { x = 0 y = 1 }
			L_Marine_Bat = { x = 0 y = 2 }
			L_Marine_Bat = { x = 0 y = 3 }
			L_Marine_Bat = { x = 0 y = 4 }
			L_Marine_Bat = { x = 1 y = 0 }
			L_Marine_Bat = { x = 1 y = 1 }
			L_Marine_Bat = { x = 1 y = 2 }
			L_Marine_Bat = { x = 1 y = 3 }
			L_Marine_Bat = { x = 1 y = 4 }
			L_Marine_Bat = { x = 2 y = 0 }
			L_Marine_Bat = { x = 2 y = 1 }
			L_Marine_Bat = { x = 2 y = 2 }
			L_Marine_Bat = { x = 2 y = 3 }
			L_Marine_Bat = { x = 2 y = 4 }
			Arty_Bat = { x = 3 y = 0 }
			Arty_Bat = { x = 3 y = 1 }
			L_Engi_Bat = { x = 3 y = 2 }
			L_Engi_Bat = { x = 3 y = 3 }
			L_Recce_Bat = { x = 3 y = 4 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
			L_Engi_Comp = { x = 0 y = 1 }
		}
	}

	#AI Special Forces Regiments - Sastifiies the special_forces role
	division_template = {
		name = "AI Special Operation Brigade"
		priority = 2
		regiments = {
			Special_Forces = { x = 0 y = 0 }
			Special_Forces = { x = 0 y = 1 }
			Special_Forces = { x = 0 y = 2 }
			Special_Forces = { x = 1 y = 0 }
			Special_Forces = { x = 1 y = 1 }
			Special_Forces = { x = 1 y = 2 }
			Special_Forces = { x = 2 y = 0 }
			Special_Forces = { x = 2 y = 1 }
			Special_Forces = { x = 2 y = 2 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
			L_Engi_Comp = { x = 0 y = 1 }
			Arty_Battery = { x = 0 y = 2 }
		}
	}
	division_template = {
		name = "AI Special Operation Division"
		priority = 2
		regiments = {
			Special_Forces = { x = 0 y = 0 }
			Special_Forces = { x = 0 y = 1 }
			Special_Forces = { x = 0 y = 2 }
			Special_Forces = { x = 0 y = 3 }
			Special_Forces = { x = 0 y = 4 }
			Special_Forces = { x = 1 y = 0 }
			Special_Forces = { x = 1 y = 1 }
			Special_Forces = { x = 1 y = 2 }
			Special_Forces = { x = 1 y = 3 }
			Special_Forces = { x = 1 y = 4 }
			Special_Forces = { x = 2 y = 0 }
			Special_Forces = { x = 2 y = 1 }
			Special_Forces = { x = 2 y = 2 }
			Special_Forces = { x = 2 y = 3 }
			Special_Forces = { x = 2 y = 4 }
			Special_Forces = { x = 3 y = 0 }
			Special_Forces = { x = 3 y = 1 }
			Special_Forces = { x = 3 y = 2 }
			L_Engi_Bat = { x = 3 y = 3 }
			L_Recce_Bat = { x = 3 y = 4 }
		}
		support = {
			Arty_Battery = { x = 0 y = 0 }
		}
	}

	#AI Air Assault Brigades
	division_template = {
		name = "AI Light Air Assault Brigade"
		priority = 2
		regiments = {
			L_Air_assault_Bat = { x = 0 y = 0 }
			L_Air_assault_Bat = { x = 0 y = 1 }
			L_Air_assault_Bat = { x = 0 y = 2 }
			L_Air_assault_Bat = { x = 0 y = 3 }
			L_Air_assault_Bat = { x = 1 y = 0 }
			L_Air_assault_Bat = { x = 1 y = 1 }
			L_Air_assault_Bat = { x = 1 y = 2 }
			L_Air_assault_Bat = { x = 1 y = 3 }
			H_Engi_Bat = { x = 2 y = 0 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
		}
	}
	division_template = {
		name = "AI Light Air Assault Division"
		priority = 2
		regiments = {
			L_Air_assault_Bat = { x = 0 y = 0 }
			L_Air_assault_Bat = { x = 0 y = 1 }
			L_Air_assault_Bat = { x = 0 y = 2 }
			L_Air_assault_Bat = { x = 0 y = 3 }
			L_Air_assault_Bat = { x = 0 y = 4 }
			L_Air_assault_Bat = { x = 1 y = 0 }
			L_Air_assault_Bat = { x = 1 y = 1 }
			L_Air_assault_Bat = { x = 1 y = 2 }
			L_Air_assault_Bat = { x = 1 y = 3 }
			L_Air_assault_Bat = { x = 1 y = 4 }
			L_Air_assault_Bat = { x = 2 y = 0 }
			L_Air_assault_Bat = { x = 2 y = 1 }
			L_Air_assault_Bat = { x = 2 y = 2 }
			L_Air_assault_Bat = { x = 2 y = 3 }
			L_Air_assault_Bat = { x = 2 y = 4 }
			L_Air_assault_Bat = { x = 3 y = 0 }
			L_Air_assault_Bat = { x = 3 y = 1 }
			L_Air_assault_Bat = { x = 3 y = 2 }
			H_Engi_Bat = { x = 4 y = 0 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
		}
	}

	#extensions of marines
	division_template = {
		name = "AI Motorized Marine Brigade"
		priority = 1
		regiments = {
			Mot_Marine_Bat = { x = 0 y = 0 }
			Mot_Marine_Bat = { x = 0 y = 1 }
			Mot_Marine_Bat = { x = 0 y = 2 }
			Mot_Marine_Bat = { x = 1 y = 0 }
			Mot_Marine_Bat = { x = 1 y = 1 }
			Mot_Marine_Bat = { x = 1 y = 2 }
			Mot_Recce_Bat = { x = 0 y = 3 }
		}
		support = {
			H_Engi_Comp = { x = 0 y = 0 }
		}
	}
	division_template = {
		name = "AI Motorized Marine Division"
		priority = 1
		regiments = {
			Mot_Marine_Bat = { x = 0 y = 0 }
			Mot_Marine_Bat = { x = 0 y = 1 }
			Mot_Marine_Bat = { x = 0 y = 2 }
			Mot_Marine_Bat = { x = 0 y = 3 }
			Mot_Marine_Bat = { x = 0 y = 4 }
			Mot_Marine_Bat = { x = 1 y = 0 }
			Mot_Marine_Bat = { x = 1 y = 1 }
			Mot_Marine_Bat = { x = 1 y = 2 }
			Mot_Marine_Bat = { x = 1 y = 3 }
			Mot_Marine_Bat = { x = 1 y = 4 }
			Mot_Marine_Bat = { x = 2 y = 0 }
			Mot_Marine_Bat = { x = 2 y = 1 }
			Mot_Marine_Bat = { x = 2 y = 2 }
			Mot_Marine_Bat = { x = 2 y = 3 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
			H_Engi_Comp = { x = 0 y = 1 }
		}
	}

	division_template = {
		name = "AI Motorized Infantry Brigade"
		priority = 1
		regiments = {
			Mot_Inf_Bat = { x = 0 y = 0 }
			Mot_Inf_Bat = { x = 0 y = 1 }
			Mot_Inf_Bat = { x = 0 y = 2 }
			Mot_Inf_Bat = { x = 1 y = 0 }
			Mot_Inf_Bat = { x = 1 y = 1 }
			Mot_Inf_Bat = { x = 1 y = 2 }
			H_Engi_Bat = { x = 2 y = 0 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Motorized Infantry Division"
		priority = 1
		regiments = {
			Mot_Inf_Bat = { x = 0 y = 0 }
			Mot_Inf_Bat = { x = 0 y = 1 }
			Mot_Inf_Bat = { x = 0 y = 2 }
			Mot_Inf_Bat = { x = 0 y = 3 }
			Mot_Inf_Bat = { x = 0 y = 4 }
			Mot_Inf_Bat = { x = 1 y = 0 }
			Mot_Inf_Bat = { x = 1 y = 1 }
			Mot_Inf_Bat = { x = 1 y = 2 }
			Mot_Inf_Bat = { x = 1 y = 3 }
			Mot_Inf_Bat = { x = 1 y = 4 }
			Mot_Inf_Bat = { x = 2 y = 0 }
			Mot_Inf_Bat = { x = 2 y = 1 }
			Mot_Inf_Bat = { x = 2 y = 2 }
			Mot_Inf_Bat = { x = 2 y = 3 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
			H_Engi_Comp = { x = 0 y = 1 }
		}
	}

	division_template = {
		name = "AI Heavy Air Assault Brigade"
		priority = 2
		regiments = {
			Arm_Air_assault_Bat = { x = 0 y = 0 }
			Arm_Air_assault_Bat = { x = 0 y = 1 }
			Arm_Air_assault_Bat = { x = 0 y = 2 }
			Arm_Air_assault_Bat = { x = 1 y = 0 }
			Arm_Air_assault_Bat = { x = 1 y = 1 }
			Arm_Air_assault_Bat = { x = 1 y = 2 }
			Arm_Air_assault_Bat = { x = 2 y = 0 }
			Arm_Air_assault_Bat = { x = 2 y = 1 }
			Arm_Air_assault_Bat = { x = 2 y = 2 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
			H_Engi_Comp = { x = 0 y = 1 }
		}
	}

	division_template = {
		name = "AI Mechanized Brigade"
		priority = 1
		regiments = {
			Mech_Inf_Bat = { x = 0 y = 0 }
			Mech_Inf_Bat = { x = 0 y = 1 }
			Mech_Inf_Bat = { x = 0 y = 2 }
			Mech_Inf_Bat = { x = 1 y = 0 }
			Mech_Inf_Bat = { x = 1 y = 1 }
			Mech_Inf_Bat = { x = 1 y = 2 }
		}
		support = {
			armor_Comp = { x = 0 y = 0 }
			Mech_Recce_Comp = { x = 0 y = 1 }
			H_Engi_Comp = { x = 0 y = 2 }
		}
	}

	division_template = {
		name = "AI Mechanized Division"
		priority = 1
		regiments = {
			Mech_Inf_Bat = { x = 0 y = 0 }
			Mech_Inf_Bat = { x = 0 y = 1 }
			Mech_Inf_Bat = { x = 0 y = 2 }
			Mech_Inf_Bat = { x = 0 y = 3 }
			Mech_Inf_Bat = { x = 0 y = 4 }
			Mech_Inf_Bat = { x = 1 y = 0 }
			Mech_Inf_Bat = { x = 1 y = 1 }
			Mech_Inf_Bat = { x = 1 y = 2 }
			Mech_Inf_Bat = { x = 1 y = 3 }
			Mech_Inf_Bat = { x = 1 y = 4 }
			H_Engi_Bat = { x = 2 y = 0 }
			Mech_Recce_Bat  = { x = 2 y = 1 }
			SP_AA_Bat = { x = 2 y = 2 }
			SP_Arty_Bat = { x = 2 y = 3 }
			SP_Arty_Bat = { x = 2 y = 4 }
		}
		support = {
			armor_Comp = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Mechanized Marine Brigade"
		priority = 1
		regiments = {
			Mech_Marine_Bat = { x = 0 y = 0 }
			Mech_Marine_Bat = { x = 0 y = 1 }
			Mech_Marine_Bat = { x = 0 y = 2 }
			Mech_Marine_Bat = { x = 1 y = 0 }
			Mech_Marine_Bat = { x = 1 y = 1 }
			Mech_Marine_Bat = { x = 1 y = 2 }
			H_Engi_Bat = { x = 2 y = 0 }
		}
		support = {
			Mech_Recce_Comp  = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Armored Marine Brigade"
		priority = 2
		regiments = {
			Arm_Marine_Bat = { x = 0 y = 0 }
			Arm_Marine_Bat = { x = 0 y = 1 }
			Arm_Marine_Bat = { x = 0 y = 2 }
			Arm_Marine_Bat = { x = 1 y = 0 }
			Arm_Marine_Bat = { x = 1 y = 1 }
			Arm_Marine_Bat = { x = 1 y = 2 }
			H_Engi_Bat = { x = 2 y = 0 }
		}
		support = {
			Arm_Recce_Comp = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Armored Infantry Brigade"
		priority = 2
		regiments = {
			Arm_Inf_Bat = { x = 0 y = 0 }
			Arm_Inf_Bat = { x = 0 y = 1 }
			Arm_Inf_Bat = { x = 0 y = 2 }
			Arm_Inf_Bat = { x = 1 y = 0 }
			Arm_Inf_Bat = { x = 1 y = 1 }
			Arm_Inf_Bat = { x = 1 y = 2 }
		}
		support = {
			armor_Comp = { x = 0 y = 0 }
			Arm_Recce_Comp = { x = 0 y = 1 }
			H_Engi_Comp = { x = 0 y = 2 }
		}
	}

	division_template = {
		name = "AI Armored Infantry Division"
		priority = 2
		regiments = {
			Arm_Inf_Bat = { x = 0 y = 0 }
			Arm_Inf_Bat = { x = 0 y = 1 }
			Arm_Inf_Bat = { x = 0 y = 2 }
			Arm_Inf_Bat = { x = 0 y = 3 }
			Arm_Inf_Bat = { x = 0 y = 4 }
			Arm_Inf_Bat = { x = 1 y = 0 }
			Arm_Inf_Bat = { x = 1 y = 1 }
			Arm_Inf_Bat = { x = 1 y = 2 }
			Arm_Inf_Bat = { x = 1 y = 3 }
			Arm_Inf_Bat = { x = 1 y = 4 }
			H_Engi_Bat = { x = 2 y = 0 }
			Arm_Recce_Bat   = { x = 2 y = 1 }
			SP_AA_Bat = { x = 2 y = 2 }
			SP_Arty_Bat = { x = 2 y = 3 }
			SP_Arty_Bat = { x = 2 y = 4 }
		}
		support = {
			armor_Comp = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Armored Brigade"
		priority = 2
		regiments = {
			armor_Bat = { x = 0 y = 0 }
			armor_Bat = { x = 0 y = 1 }
			armor_Bat = { x = 0 y = 2 }
			Arm_Inf_Bat = { x = 1 y = 0 }
			Arm_Inf_Bat = { x = 1 y = 1 }
			Arm_Inf_Bat = { x = 1 y = 2 }
			armor_Recce_Bat  = { x = 2 y = 0 }
			SP_Arty_Bat = { x = 2 y = 1 }
			SP_Arty_Bat = { x = 2 y = 2 }
		}
		support = {
			SP_AA_Battery = { x = 0 y = 0 }
			armor_Recce_Comp  = { x = 0 y = 1 }
			H_Engi_Comp = { x = 0 y = 2 }
		}
	}
}

Major_Power_AI_templates = {

	division_template = {
		name = "AI Armored Division"
		priority = 2
		regiments = {
			H_Engi_Bat = { x = 0 y = 0 }
			armor_Bat = { x = 0 y = 1 }
			armor_Bat = { x = 0 y = 2 }
			armor_Bat = { x = 0 y = 3 }
			SP_Arty_Bat = { x = 0 y = 4 }
			H_Engi_Bat = { x = 1 y = 0 }
			armor_Bat = { x = 1 y = 1 }
			armor_Bat = { x = 1 y = 2 }
			armor_Bat = { x = 1 y = 3 }
			SP_Arty_Bat = { x = 1 y = 4 }
			armor_Recce_Bat  = { x = 2 y = 0 }
			Arm_Inf_Bat = { x = 2 y = 1 }
			Arm_Inf_Bat = { x = 2 y = 2 }
			Arm_Inf_Bat = { x = 2 y = 3 }
			SP_Arty_Bat = { x = 2 y = 4 }
			armor_Recce_Bat = { x = 3 y = 0 }
			Arm_Inf_Bat = { x = 3 y = 1 }
			Arm_Inf_Bat = { x = 3 y = 2 }
			Arm_Inf_Bat = { x = 3 y = 3 }
			SP_Arty_Bat = { x = 3 y = 4 }
		}
		support = {
			SP_AA_Battery = { x = 0 y = 0 }
			H_Engi_Comp = { x = 0 y = 1 }
		}
	}

	division_template = {
		name = "AI Heavy Air Assault Division"
		priority = 2
		regiments = {
			Arm_Air_assault_Bat = { x = 0 y = 0 }
			Arm_Air_assault_Bat = { x = 0 y = 1 }
			Arm_Air_assault_Bat = { x = 0 y = 2 }
			Arm_Air_assault_Bat = { x = 0 y = 3 }
			Arm_Air_assault_Bat = { x = 0 y = 4 }
			Arm_Air_assault_Bat = { x = 1 y = 0 }
			Arm_Air_assault_Bat = { x = 1 y = 1 }
			Arm_Air_assault_Bat = { x = 1 y = 2 }
			Arm_Air_assault_Bat = { x = 1 y = 3 }
			Arm_Air_assault_Bat = { x = 1 y = 4 }
			Arm_Air_assault_Bat = { x = 2 y = 0 }
			Arm_Air_assault_Bat = { x = 2 y = 1 }
			Arm_Air_assault_Bat = { x = 2 y = 2 }
			Arm_Air_assault_Bat = { x = 2 y = 3 }
			Arm_Air_assault_Bat = { x = 2 y = 4 }
			Arm_Air_assault_Bat = { x = 3 y = 0 }
			H_Engi_Bat = { x = 3 y = 1 }
			H_Engi_Bat = { x = 3 y = 2 }
			Mot_Recce_Bat = { x = 4 y = 0 }
			Mot_Recce_Bat = { x = 4 y = 1 }
		}
	}

	division_template = {
		name = "AI Armored Marine Division"
		priority = 2
		regiments = {
			Arm_Marine_Bat = { x = 0 y = 0 }
			Arm_Marine_Bat = { x = 0 y = 1 }
			Arm_Marine_Bat = { x = 0 y = 2 }
			Arm_Marine_Bat = { x = 0 y = 3 }
			Arm_Marine_Bat = { x = 1 y = 0 }
			Arm_Marine_Bat = { x = 1 y = 1 }
			Arm_Marine_Bat = { x = 1 y = 2 }
			Arm_Marine_Bat = { x = 1 y = 3 }
			Arm_Marine_Bat = { x = 2 y = 0 }
			Arm_Marine_Bat = { x = 2 y = 1 }
			Arm_Marine_Bat = { x = 2 y = 2 }
			Arm_Marine_Bat = { x = 2 y = 3 }
			H_Engi_Bat = { x = 3 y = 0 }
			H_Engi_Bat  = { x = 3 y = 1 }
		}
		support = {
			Arm_Recce_Comp  = { x = 0 y = 0 }
			armor_Recce_Comp  = { x = 0 y = 1 }
		}
	}

	division_template = {
		name = "AI Mechanized Marine Division"
		priority = 1
		regiments = {
			Mech_Marine_Bat = { x = 0 y = 0 }
			Mech_Marine_Bat = { x = 0 y = 1 }
			Mech_Marine_Bat = { x = 0 y = 2 }
			Mech_Marine_Bat = { x = 0 y = 3 }
			Mech_Marine_Bat = { x = 1 y = 0 }
			Mech_Marine_Bat = { x = 1 y = 1 }
			Mech_Marine_Bat = { x = 1 y = 2 }
			Mech_Marine_Bat = { x = 1 y = 3 }
			Mech_Marine_Bat = { x = 2 y = 0 }
			Mech_Marine_Bat = { x = 2 y = 1 }
			Mech_Marine_Bat = { x = 2 y = 2 }
			Mech_Marine_Bat = { x = 2 y = 3 }
			H_Engi_Bat = { x = 3 y = 0 }
			H_Engi_Bat  = { x = 3 y = 1 }
		}
		support = {
			Mech_Recce_Comp   = { x = 0 y = 0 }
			armor_Recce_Comp  = { x = 0 y = 1 }
		}
	}
}

Militia_AI_templates = {
	##civil war nations use these. Its to mimic the need for men with low IC cost.
	division_template = {
		name = "AI Militia Brigade - 15w"
		priority = 0
		regiments = {
			Militia_Bat = { x = 0 y = 0 }
			Militia_Bat = { x = 0 y = 1 }
			Militia_Bat = { x = 0 y = 2 }
			Militia_Bat = { x = 0 y = 3 }
			Militia_Bat = { x = 0 y = 4 }
		}
	}

	division_template = {
		name = "AI Militia Brigade - 30w"
		priority = 0
		regiments = {
			Militia_Bat = { x = 0 y = 0 }
			Militia_Bat = { x = 0 y = 1 }
			Militia_Bat = { x = 0 y = 2 }
			Militia_Bat = { x = 0 y = 3 }
			Militia_Bat = { x = 0 y = 4 }
			Militia_Bat = { x = 1 y = 0 }
			Militia_Bat = { x = 1 y = 1 }
			Militia_Bat = { x = 1 y = 2 }
			Militia_Bat = { x = 1 y = 3 }
			L_Engi_Bat = { x = 1 y = 4 }
		}
		support = {
			L_Recce_Comp = { x = 0 y = 0 }
		}
	}

	division_template = {
		name = "AI Motorized Militia Brigade - 30w"
		priority = 0
		regiments = {
			Mot_Militia_Bat = { x = 0 y = 0 }
			Mot_Militia_Bat = { x = 0 y = 1 }
			Mot_Militia_Bat = { x = 0 y = 2 }
			Mot_Militia_Bat = { x = 1 y = 0 }
			Mot_Militia_Bat = { x = 1 y = 1 }
			Mot_Militia_Bat = { x = 1 y = 2 }
			H_Engi_Bat = { x = 2 y = 0 }
		}
		support = {
			Mot_Recce_Comp = { x = 0 y = 0 }
		}
	}
}

Simone_likes_80_width_SOF = {
	#Simone. AI doesn't need this :tongue:
	division_template = {
		name = "Simone's Special Operation Division"
		priority = 2
		regiments = {
			Special_Forces = { x = 0 y = 0 }
			Special_Forces = { x = 0 y = 1 }
			Special_Forces = { x = 0 y = 2 }
			Special_Forces = { x = 0 y = 3 }
			Special_Forces = { x = 0 y = 4 }
			Special_Forces = { x = 1 y = 0 }
			Special_Forces = { x = 1 y = 1 }
			Special_Forces = { x = 1 y = 2 }
			Special_Forces = { x = 1 y = 3 }
			Special_Forces = { x = 1 y = 4 }
			Special_Forces = { x = 2 y = 0 }
			Special_Forces = { x = 2 y = 1 }
			Special_Forces = { x = 2 y = 2 }
			Special_Forces = { x = 2 y = 3 }
			Special_Forces = { x = 2 y = 4 }
			Special_Forces = { x = 3 y = 0 }
			Special_Forces = { x = 3 y = 1 }
			Special_Forces = { x = 3 y = 2 }
			Special_Forces = { x = 3 y = 3 }
			Special_Forces = { x = 3 y = 4 }
			Special_Forces = { x = 4 y = 0 }
			Special_Forces = { x = 4 y = 1 }
			Special_Forces = { x = 4 y = 2 }
			Special_Forces = { x = 4 y = 3 }
			Special_Forces = { x = 4 y = 4 }
		}
		support = {
			Arm_Recce_Comp  = { x = 0 y = 0 }
			H_Engi_Comp = { x = 0 y = 1 }
			L_Engi_Comp = { x = 0 y = 2 }
		}
	}
}
### New setup ###

defined_text = {
   name = tax_button_req

	text = {
	   trigger = { ROOT = { has_political_power > 50 } }
	   localization_key = tax_action_button_req_enable_TT
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = tax_action_button_req_disable_TT
	}
}

defined_text = {
   name = defence_00_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_01_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_02_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_03_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_04_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_05_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^5]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_06_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^6]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_07_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^7]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_08_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^8]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}
defined_text = {
   name = defence_09_cost

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_defence_spend^9]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.defence_gain]"
	}
}

defined_text = {
   name = interest_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_06_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^5]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_07_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^6]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_08_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^7]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}
defined_text = {
   name = interest_09_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_interest_rate^8]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.interest_rate]"
	}
}

defined_text = {
   name = bureau_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_bureaucracy_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.bureaucracy_gain]"
	}
}
defined_text = {
   name = bureau_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_bureaucracy_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.bureaucracy_gain]"
	}
}
defined_text = {
   name = bureau_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_bureaucracy_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.bureaucracy_gain]"
	}
}
defined_text = {
   name = bureau_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_bureaucracy_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.bureaucracy_gain]"
	}
}
defined_text = {
   name = bureau_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_bureaucracy_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.bureaucracy_gain]"
	}
}


defined_text = {
   name = police_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_security_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.security_gain]"
	}
}
defined_text = {
   name = police_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_security_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.security_gain]"
	}
}
defined_text = {
   name = police_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_security_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.security_gain]"
	}
}
defined_text = {
   name = police_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_security_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.security_gain]"
	}
}
defined_text = {
   name = police_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_security_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.security_gain]"
	}
}

defined_text = {
   name = education_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_education_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.education_gain]"
	}
}
defined_text = {
   name = education_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_education_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.education_gain]"
	}
}
defined_text = {
   name = education_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_education_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.education_gain]"
	}
}
defined_text = {
   name = education_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_education_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.education_gain]"
	}
}
defined_text = {
   name = education_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_education_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.education_gain]"
	}
}

defined_text = {
   name = health_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
   name = health_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
   name = health_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
   name = health_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
   name = health_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
   name = health_06_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_health_spend^5]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.health_gain]"
	}
}

defined_text = {
   name = social_01_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^0]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
   name = social_02_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^1]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
   name = social_03_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^2]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
   name = social_04_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^3]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
   name = social_05_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^4]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
   name = social_06_rate

	text = {
	   trigger = { ROOT = { is_ai = no } }
	   localization_key = "[?THIS.array_social_spend^5]"
	}
	text = {
	   #trigger = { always = yes }
	   localization_key = "[?THIS.welfare_gain]"
	}
}
defined_text = {
	name = bureau_rate
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = bureau_01
			}
		}
		localization_key = "[?THIS.array_bureaucracy_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = bureau_02
			}
		}
		localization_key = "[?THIS.array_bureaucracy_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = bureau_03
			}
		}
		localization_key = "[?THIS.array_bureaucracy_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = bureau_04
			}
		}
		localization_key = "[?THIS.array_bureaucracy_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = bureau_05
			}
		}
		localization_key = "[?THIS.array_bureaucracy_spend^4]"
	}
	text = { ##AI Cost
		localization_key = "[?THIS.bureaucracy_gain]"
	}
}

defined_text = {
	name = military_spending_rate_loc
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_00
			}
		}
		localization_key = "[?THIS.array_defence_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_01
			}
		}
		localization_key = "[?THIS.array_defence_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_02
			}
		}
		localization_key = "[?THIS.array_defence_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_03
			}
		}
		localization_key = "[?THIS.array_defence_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_04
			}
		}
		localization_key = "[?THIS.array_defence_spend^4]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_05
			}
		}
		localization_key = "[?THIS.array_defence_spend^5]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_06
			}
		}
		localization_key = "[?THIS.array_defence_spend^6]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_07
			}
		}
		localization_key = "[?THIS.array_defence_spend^7]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_08
			}
		}
		localization_key = "[?THIS.array_defence_spend^8]"
	}
	text = {
		trigger = {
			ROOT = {
				is_ai = no
				has_idea = defence_09
			}
		}
		localization_key = "[?THIS.array_defence_spend^9]"
	}
	text = {
		localization_key = "[?THIS.defence_gain]"
	}
}

defined_text = {
	name = police_rate_loc
	text = {
		trigger = {
			ROOT = {
				has_idea = police_01
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_security_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = police_02
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_security_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = police_03
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_security_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = police_04
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_security_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = police_05
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_security_spend^4]"
	}
	text = {
		localization_key = "[?THIS.security_gain]"
	}
}

defined_text = {
	name = education_rate_loc
	text = {
		trigger = {
			ROOT = {
				has_idea = edu_01
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_education_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = edu_02
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_education_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = edu_03
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_education_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = edu_04
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_education_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = edu_05
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_education_spend^4]"
	}
	text = {
		localization_key = "[?THIS.education_gain]"
	}
}

defined_text = {
	name = health_rate_loc
	text = {
		trigger = {
			ROOT = {
				has_idea = health_01
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = health_02
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = health_03
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = health_04
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = health_05
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^4]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = health_06
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_health_spend^5]"
	}
	text = {
		localization_key = "[?THIS.health_gain]"
	}
}
defined_text = {
	name = social_rate_loc
	text = {
		trigger = {
			ROOT = {
				has_idea = social_01
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^0]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = social_02
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^1]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = social_03
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^2]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = social_04
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^3]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = social_05
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^4]"
	}
	text = {
		trigger = {
			ROOT = {
				has_idea = social_06
				is_ai = no
			}
		}
		localization_key = "[?THIS.array_social_spend^5]"
	}
	text = {
		localization_key = "[?THIS.welfare_gain]"
	}
}

##Bankruptcy Decisions
defined_text = {
	name = bankruptcy_treasury_scl
	text = {
		trigger = {
			num_of_factories < 10
			check_variable = { treasury < 2 }
		}
		localization_key = has_less_than_2_tt
	}
	text = {
		trigger = {
			num_of_factories > 9
			num_of_factories < 25
			check_variable = { treasury < 5 }
		}
		localization_key = has_less_than_5_tt
	}
	text = {
		trigger = {
			num_of_factories > 24
			num_of_factories < 50
			check_variable = { treasury < 10 }
		}
		localization_key = has_less_than_10_tt
	}
	text = {
		trigger = {
			num_of_factories > 49
			num_of_factories < 75
			check_variable = { treasury < 15 }
		}
		localization_key = has_less_than_15_tt
	}
	text = {
		trigger = {
			num_of_factories > 74
			num_of_factories < 100
			check_variable = { treasury < 20 }
		}
		localization_key = has_less_than_20_tt
	}
	text = {
		trigger = {
			num_of_factories > 99
			check_variable = { treasury < 25 }
		}
		localization_key = has_less_than_25_tt
	}
}
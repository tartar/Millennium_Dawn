ITA_supports_afghanistan = {
	allowed = { tag = ITA }
	enable = {
		NOT = { has_war_with = AFG }
		AFG = { NOT = { has_government = fascism } }
	}
	abort = {
		OR = {
			has_war_with = AFG
			AFG = { has_government = fascism }
		}
	}
	ai_strategy = { type = befriend id = "AFG" value = 50 }
	ai_strategy = { type = support id = "AFG" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "AFG" value = 150 }
}

ITA_specops_in_libya = {
	allowed = { tag = ITA }
	enable = {
		NOT = { has_war_with = GNA }
	}
	abort = {
		has_war_with = GNA
	}
	ai_strategy = { type = befriend id = "GNA" value = 100 }
	ai_strategy = { type = protect id = "GNA" value = 100 }
	ai_strategy = { type = influence id = "GNA" value = 100 }
}

ITA_factory_target = {
	allowed = { tag = ITA }
	enable = { always = yes }

	ai_strategy = {
		type = building_target
		id = industrial_complex
		value = 60
	}
}
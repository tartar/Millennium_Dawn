﻿MD_desperately_need_guns = {
	enable = { has_equipment = { Inf_equipment < 0 } }
	abort = { has_equipment = { Inf_equipment > 2500 } }
	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = 500 }
	ai_strategy = { type = equipment_production_factor id = anti_tank value = -150 }
	ai_strategy = { type = equipment_production_factor id = artillery value = -150 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = -150 }
	ai_strategy = { type = equipment_production_factor id = mechanized value = -150 }
	ai_strategy = { type = equipment_production_factor id = armor value = -150 }
	ai_strategy = { type = role_ratio id = L_Inf value = -500 }
	ai_strategy = { type = role_ratio id = infantry value = -500 }
	ai_strategy = { type = role_ratio id = garrison value = -500 }
	ai_strategy = { type = role_ratio id = mechanized value = -500 }
	ai_strategy = { type = role_ratio id = armor value = -500 }
	ai_strategy = { type = role_ratio id = marines value = -500 }
	ai_strategy = { type = role_ratio id = Air_assault value = -500 }
	ai_strategy = { type = role_ratio id = Air_Inf value = -500 }
	ai_strategy = { type = role_ratio id = Special_Forces value = -500 }
}
MD_excess_inf_equipment = {
	enable = {
		has_equipment = { Inf_equipment > 50000 }
	}
	abort = {
		has_equipment = { Inf_equipment < 35000 }
	}

	ai_strategy = { type = equipment_variant_production_factor id = Inf_equipment value = -150 }
}
MD_ignore_command_equipment = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = { has_equipment = { command_control_equipment > 15000 } }
	abort = { has_equipment = { command_control_equipment < 10000 } }

	ai_strategy = { type = equipment_variant_production_factor  id = cnc_equipment value = -150 }
}

MD_ignore_util_vehicles = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = { has_equipment = { util_vehicle_equipment > 20000 } }
	abort = { has_equipment = { util_vehicle_equipment < 5000 } }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = -200 }
}

MD_need_util_vehicles = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = { has_equipment = { util_vehicle_equipment < 5000 } }
	abort = { has_equipment = { util_vehicle_equipment < 20000 } }
	ai_strategy = { type = equipment_variant_production_factor id = util_vehicle_equipment value = 50 }
}


MD_build_command_equipment = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = { has_equipment = { command_control_equipment < 1000 } }
	abort = { has_equipment = { command_control_equipment > 15000 } }
	ai_strategy = { type = equipment_variant_production_factor  id = cnc_equipment value = 150 }
}
MD_shortage_InfEqp = {
	enable = {
		OR = {
			has_equipment = { Inf_equipment < 1000 }
			has_equipment = { command_control_equipment < 500 }
		}
	}
	abort = {
		OR = {
			has_equipment = { Inf_equipment > 1000 }
			has_equipment = { command_control_equipment > 500 }
		}
	}

	ai_strategy = { type = equipment_production_factor id = infantry value = 100 }
	ai_strategy = { type = role_ratio id = L_Inf value = -500 }
	ai_strategy = { type = role_ratio id = infantry value = -500 }
	ai_strategy = { type = role_ratio id = garrison value = -500 }
	ai_strategy = { type = role_ratio id = mechanized value = -500 }
	ai_strategy = { type = role_ratio id = armor value = -500 }
	ai_strategy = { type = role_ratio id = marines value = -500 }
	ai_strategy = { type = role_ratio id = Air_assault value = -500 }
	ai_strategy = { type = role_ratio id = Air_Inf value = -500 }
	ai_strategy = { type = role_ratio id = Special_Forces value = -500 }
}
MD_arty_shortage = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { artillery_equipment < 1000 }
	}
	abort = {
		has_equipment = { artillery_equipment > 2000 }
	}

	ai_strategy = { type = equipment_production_factor id = artillery value = 50 }
}
MD_arty_no_needed = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { artillery_equipment > 2000 }
	}
	abort = {
		has_equipment = { artillery_equipment < 1000 }
	}

	ai_strategy = { type = equipment_production_factor id = artillery value = -100 }
}
MD_excess_AT = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { L_AT_Equipment > 1000 }
	}
	abort = {
		has_equipment = { L_AT_Equipment < 500 }
	}

	ai_strategy = { type = equipment_production_factor id = anti_tank value = -150 }
}
MD_shortage_AT = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { L_AT_Equipment < 500 }
	}
	abort = {
		has_equipment = { L_AT_Equipment > 1000 }
	}

	ai_strategy = { type = equipment_production_factor id = anti_tank value = 100 }
	ai_strategy = { type = equipment_production_factor id = artillery value = -150 }
	ai_strategy = { type = equipment_variant_production_factor id = cnc_equipment value = -150 }
	ai_strategy = { type = role_ratio id = L_Inf value = -500 }
	ai_strategy = { type = role_ratio id = infantry value = -500 }
	ai_strategy = { type = role_ratio id = garrison value = -500 }
	ai_strategy = { type = role_ratio id = mechanized value = -500 }
	ai_strategy = { type = role_ratio id = armor value = -500 }
	ai_strategy = { type = role_ratio id = marines value = -500 }
	ai_strategy = { type = role_ratio id = Air_assault value = -500 }
	ai_strategy = { type = role_ratio id = Air_Inf value = -500 }
	ai_strategy = { type = role_ratio id = Special_Forces value = -500 }
}
default_stockpile_management = {
	enable = { has_war = no }
	abort_when_not_enabled = yes

	ai_strategy = { type = equipment_stockpile_surplus_ratio value = 45 } # double base stockpile #from 25 to 35
}

##AI Make Less
MD_ai_make_less_ifv_apc_cuz_too_many = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		AND = {
			has_equipment = { APC_Equipment > 5000 }
			has_equipment = { IFV_Equipment > 5000 }
		}
	}
	abort = {
		OR = {
			has_equipment = { APC_Equipment < 2000 }
			has_equipment = { IFV_Equipment < 2000 }
		}
	}
	ai_strategy = { type = equipment_production_factor id = mechanized value = -100 }
}

MD_mechanized_here_we_come = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		OR = {
			has_equipment = { APC_Equipment < 2000 }
			has_equipment = { IFV_Equipment < 2000 }
		}
	}
	abort = {
		AND = {
			has_equipment = { APC_Equipment > 5000 }
			has_equipment = { IFV_Equipment > 5000 }
		}
	}
	ai_strategy = { type = equipment_production_factor id = mechanized value = 100 }
}

MD_need_armor = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		OR = {
			has_equipment = { MBT_Equipment < 1500 }
			has_equipment = { Rec_tank_Equipment < 1500 }
		}
	}
	abort = {
		AND = {
			has_equipment = { MBT_Equipment > 2000 }
			has_equipment = { Rec_tank_Equipment > 2000 }
		}
	}
	ai_strategy = { type = equipment_production_factor id = armor value = 100 }
}


MD_dont_need_armor = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		OR = {
			has_equipment = { MBT_Equipment > 2000 }
			has_equipment = { Rec_tank_Equipment > 2000 }
		}
	}
	abort = {
		AND = {
			has_equipment = { MBT_Equipment > 2000 }
			has_equipment = { Rec_tank_Equipment > 2000 }
		}
	}
	ai_strategy = { type = equipment_production_factor id = armor value = 100 }
}

MD_need_ifv = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { IFV_Equipment < 2000 }
	}
	abort = { has_equipment = { IFV_Equipment > 10000 } }
	ai_strategy = { type = equipment_variant_production_factor id = IFV_Equipment value = 100 }
}

MD_no_ifv = {
	allowed = {
		NOT = {
			original_tag = MNC #Monaco
			original_tag = ADO #Andorra
			original_tag = LIC #Lic
			original_tag = BHU #Bhutan
			original_tag = BRU #Brunei
			original_tag = MIC #Micronesia
			original_tag = PLY #Polynesia
			original_tag = STK #Saint Kitts
			original_tag = DMI #Dominica
			original_tag = STL #Saint Lucia
			original_tag = STV #Saint Vincent
			original_tag = BAR #Barbados
			original_tag = BAH #Bahamas
			original_tag = GRA #Grenada
			original_tag = JAM #Jamacia
			original_tag = SEY #Seychelles
			original_tag = GUY #Guyana
			original_tag = SUR #Suriname
			original_tag = COS #Costa Rica
			original_tag = VER #Cape Verde
			original_tag = GAM #Gambia
			original_tag = GUB #Guinea - Bissau
			original_tag = EGU #Equatorial Guinea
			original_tag = SAO #Sao Tome
			original_tag = DJI #Djibouti
			original_tag = MLD #Maldives
			original_tag = COM #Comoros
			original_tag = MRT #Mauritius
			original_tag = SOL #Solomon Islands
			original_tag = SHA #Sahrawi
			original_tag = LUX #Luxembourg
			original_tag = ICE #Iceland
		}
	}
	enable = {
		has_equipment = { IFV_Equipment > 10000 }
	}
	abort = { has_equipment = { IFV_Equipment < 2000 } }
	ai_strategy = { type = equipment_variant_production_factor id = IFV_Equipment value = -100 }
}
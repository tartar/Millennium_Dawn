TAL_support_SHB = {
	allowed = { 
		original_tag = TAL 
	}
	enable = {
		has_government = fascism
		SHB = { has_government = fascism }
	}
	abort_when_not_enabled = yes
	
	ai_strategy = { type = befriend id = "SHB" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "SHB" value = 100 }
	ai_strategy = { type = support id = "SHB" value = 200 }

}

TAL_support_TTP = {
	allowed = { 
		original_tag = TAL 
	}
	enable = {
		has_government = fascism
		TTP = { has_government = fascism }
	}
	abort_when_not_enabled = yes
	
	ai_strategy = { type = befriend id = "TTP" value = 100 }
	ai_strategy = { type = protect id = "TTP" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "TTP" value = 300 }
	ai_strategy = { type = support id = "TTP" value = 200 }

}
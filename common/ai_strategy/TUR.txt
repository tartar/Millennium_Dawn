TUR_support_gna = {
	allowed = { tag = TUR }
	enable = {
		NOT = { has_war_with = GNA }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "GNA" value = 100 }
	ai_strategy = { type = protect id = "GNA" value = 100 }
	ai_strategy = { type = influence id = "GNA" value = 100 }
	ai_strategy = { type = support id = "GNA" value = 100 }
}

TUR_support_fsa = {
	allowed = { tag = TUR }
	enable = {
		country_exists = FSA
		NOT = { has_war_with = FSA }
		FSA = { NOT = { has_government = fascism } }
	}
	abort = {
		OR = {
			has_war_with = FSA
			FSA = { has_government = fascism }
		}
	}

	ai_strategy = { type = befriend id = "FSA" value = 100 }
	ai_strategy = { type = protect id = "FSA" value = 100 }
	ai_strategy = { type = support id = "FSA" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "FSA" value = 100 }
}

TUR_support_hts_after_fsa = {
	allowed = { tag = TUR }
	enable = {
		country_exists = NUS
		NOT = { has_war_with = NUS }
		NOT = { country_exists = FSA }
	}
	abort = {
		OR = {
			has_war_with = NUS
			country_exists = FSA
		}
	}

	ai_strategy = { type = befriend id = "NUS" value = 100 }
	ai_strategy = { type = protect id = "NUS" value = 100 }
	ai_strategy = { type = support id = "NUS" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "NUS" value = 100 }
}

TUR_support_alwaites = {
	allowed = { tag = TUR }
	enable = {
		NOT = { has_war_with = ALA }
		158 = {
			NOT = { is_core_of = ALA }
			NOT = { is_claimed_by = ALA }
		}
	}
	abort = {
		OR = {
			has_war_with = FSA
			158 = {
				OR = {
					is_core_of = ALA
					is_claimed_by = ALA
				}
			}
		}
	}

	ai_strategy = { type = support id = "ALA" value = 200 }
	ai_strategy = { type = send_volunteers_desire id = "ALA" value = 200 }
}

TUR_syria_supports_PKK = {
	allowed = { tag = TUR }
	enable = {
		SYR = { has_completed_focus = SYR_provide_intelligence_to_pkk }
	}
	abort = {
		is_in_faction_with = SYR
	}

	ai_strategy = { type = befriend id = "SYR" value = -50 }
	ai_strategy = { type = antagonize id = "SYR" value = 50 }
	ai_strategy = { type = contain id = "SYR" value = 50 }
}

TUR_syria_supports_PKK_more = {
	allowed = { tag = TUR }
	enable = {
		SYR = { has_completed_focus = SYR_pkk_training_camps }
	}
	abort = {
		is_in_faction_with = SYR
	}

	ai_strategy = { type = befriend id = "SYR" value = -50 }
	ai_strategy = { type = antagonize id = "SYR" value = 50 }
	ai_strategy = { type = contain id = "SYR" value = 50 }
}

TUR_syria_supports_PKK_even_more = {
	allowed = { tag = TUR }
	enable = {
		SYR = { has_completed_focus = SYR_weapons_to_pkk }
	}
	abort = {
		is_in_faction_with = SYR
	}

	ai_strategy = { type = befriend id = "SYR" value = -50 }
	ai_strategy = { type = antagonize id = "SYR" value = 50 }
	ai_strategy = { type = contain id = "SYR" value = 50 }
}
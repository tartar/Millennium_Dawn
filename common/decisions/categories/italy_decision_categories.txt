################
##### ITA ######
################

romanization_ITA = {
	picture = GFX_decision_cat_generic_roman_empire
	icon = political_actions
	priority = 46

	allowed = {
		original_tag = ITA
	}
	
	visible = {
		has_country_flag = ITA_romanization_decisions
	}
}
generic_ITA = {
	picture = GFX_decision_generic_political_discourse
	icon = political_actions
	priority = 49

	allowed = {
		original_tag = ITA
	}
}
integrative_restoration_ITA = {
	picture = GFX_decision_cat_generic_roman_empire
	icon = political_actions
	priority = 48

	allowed = {
		original_tag = ITA
	}
	
	visible = {
		has_completed_focus = ITA_integrative_restoration
	}
}
mafia_ITA = {
	scripted_gui = ITA_mafia_gui
	visible_when_empty = yes	
	icon = political_actions
	priority = 50

	allowed = {
		original_tag = ITA
	}
	
	visible = {
		has_idea = ITA_mafia
	}
}
ITA_italian_space_decision_category = {
	icon = GFX_decision_asteroid_mining
	allowed = { original_tag = ITA }
	priority = 47

	visible = {
		has_completed_focus = ITA_increase_ASI_funding
		is_puppet = no
		has_capitulated = no
	}
}
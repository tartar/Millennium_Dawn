EP_main_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	visible = {
		always = yes
	}
}

EP_inf_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	visible = {
		has_country_flag = inf_cat_open
	}
}


EP_vhc_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	visible = {
		has_country_flag = vhc_cat_open
	}
}

EP_air_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	visible = {
		has_country_flag = air_cat_open
	}
}

EP_navy_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	#visible = {
	#	has_country_flag = navy_cat_open
	#}
}

EP_smug_main_cat = {
	allowed = { is_ai = no }
	icon = generic_economy

	visible = {
		always = yes
	}
}
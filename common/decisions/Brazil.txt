BRA_workers_party_alignment_decision_category = {

	BRA_decision_support_radicals = {

		cost = 50
		icon = unknown

		available = {
			has_government = communism
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_radicals"
			subtract_from_variable = { BRA_brazilian_party_alignment = 5.0 }
			custom_effect_tooltip = BRA_lowers_party_alignment_by_5_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_support_radicals"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_support_reformists = {

		cost = 50
		icon = unknown

		available = {
			has_government = communism
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_reformists"
			add_to_variable = { BRA_brazilian_party_alignment = 5.0 }
			custom_effect_tooltip = BRA_raise_party_alignment_by_5_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_support_reformists"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 2

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}
}

BRA_amazon_resource_development_decision_category = {

	BRA_decision_intial_investment = {

		cost = 75

		available = {
			check_variable = { treasury	> 7.5 }
		}


		days_remove = 30
		days_re_enable = 1
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_intial_investment"
			# add_timed_idea = {
			# 	idea = BRA_idea_initial_investiment
			# 	days = 60
			# }
			subtract_from_variable = { treasury = 7.5 }
			custom_effect_tooltip = BRA_decision_intial_investment_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_intial_investment"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 2

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_the_road_to_el_dorado = {
		cost = 75

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# # has_idea = BRA_idea_initial_investiment
		}


		days_remove = 45
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_the_road_to_el_dorado"
			892 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			subtract_from_variable = { treasury = 10 }
			increase_industrial_conglomerates_opinion = yes
			custom_effect_tooltip = BRA_decision_the_road_to_el_dorado_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_the_road_to_el_dorado"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 2

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_expand_the_mining_industry = {

		cost = 100

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_expand_the_mining_industry"
			add_resource = {
				type = aluminium
				amount = 2
				state = 892
			}
			subtract_from_variable = { treasury = 15 }
			custom_effect_tooltip = BRA_decision_expand_the_mining_industry_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_expand_the_mining_industry"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_increase_the_oil_production = {

		cost = 75

		available = {
			has_full_control_of_state = 891
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 90
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_increase_the_oil_production"
			subtract_from_variable = { treasury = 20 }
			custom_effect_tooltip = BRA_decision_increase_the_oil_production_tt
			add_resource = {
				type = oil
				amount = 2
				state = 891
			}
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_increase_the_oil_production"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_increase_military_production = {

		cost = 150

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 90
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_increase_military_production"
			subtract_from_variable = { treasury = 30 }
			custom_effect_tooltip = BRA_decision_increase_military_production_tt
			892 = {
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_increase_military_production"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_tech_metal_industry_in_the_amazon = {

		cost = 125

		available = {
			has_full_control_of_state = 892
			has_decision = BRA_decision_intial_investment
			# has_idea = BRA_idea_initial_investiment
		}


		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_tech_metal_industry_in_the_amazon"
			subtract_from_variable = { treasury = 30 }
			custom_effect_tooltip = BRA_decision_increase_military_production_tt
			add_resource = {
				type = tungsten
				amount = 4
				state = 892
			}
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_tech_metal_industry_in_the_amazon"
 }


		highlight_states = { }

		ai_will_do = {
			factor = 6

			modifier = {
				add = 4
				is_historical_focus_on = Yes
			}
		}
	}
}

BRA_car_wash_decision_category = {

}

#Amazon Protection
BRA_amazon_protection_decision_category = {

	BRA_decision_reduce_fires = {
		cost = 75

		available = {
			has_full_control_of_state = 892
			has_full_control_of_state = 891
		}


		days_remove = 45
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_the_road_to_el_dorado"
			subtract_from_variable = { treasury = 10 }
			custom_effect_tooltip = BRA_decision_reduce_fires_tt
		}

		modifier = { }

		remove_effect = {
			892 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_stability = 0.05
			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_reduce_fires"
 		}
		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 4
				is_historical_focus_on = no
			}
		}
	}

	BRA_decision_safe_mining_expansion = {

		cost = 100

		available = {
			has_full_control_of_state = 892
			has_full_control_of_state = 891
		}


		days_remove = 100
		days_re_enable = 0
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_expand_the_mining_industry"
			subtract_from_variable = { treasury = 15 }
			custom_effect_tooltip = BRA_decision_expand_the_mining_industry_tt
		}

		modifier = { }

		remove_effect = {
			add_resource = {
				type = aluminium
				amount = 2
				state = 892
			}
			add_resource = {
				type = aluminium
				amount = 2
				state = 891
			}
			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_expand_the_mining_industry"
 		}
	}
}

#Bolsonaro Alignment
BRA_bolsonaro_alignment_decision_category = {

	BRA_decision_gatter_conservative_support = {

		cost = 50
		icon = generic_civil_support

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_bolsonaro_conservative_influence
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_gatter_conservative_support"
			add_timed_idea = {
				idea = BRA_idea_bolsonaro_conservative_influence
				days = 365
			}
			custom_effect_tooltip = BRA_bolsonaro_influence_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_gatter_conservative_support"
 		}


		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = Yes
			}
		}
	}

	BRA_decision_gatter_liberal_support = {

		cost = 50
		icon = eng_propaganda_campaigns

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_bolsonaro_liberal_influence
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_gatter_liberal_support"
			add_timed_idea = {
				idea = BRA_idea_bolsonaro_liberal_influence
				days = 365
			}
			custom_effect_tooltip = BRA_bolsonaro_influence_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_gatter_liberal_support"
 		}


		highlight_states = { }

		ai_will_do = {
			factor = 2

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}

	BRA_decision_support_private_sector = {

		cost = 50
		icon = generic_factory

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_supporting_private_sector
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_private_sector"
			add_timed_idea = {
				idea = BRA_idea_supporting_private_sector
				days = 730
			}
			custom_effect_tooltip = BRA_supporting_private_sector_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_support_private_sector"
 }
		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}

	BRA_decision_support_militaries = {

		cost = 50
		icon = brazilian_guns

		available = {
			has_government = nationalist
			NOT = {
				has_idea = BRA_idea_supporting_military
			}
		}


		days_remove = 7
		days_re_enable = 45
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision BRA_decision_support_militaries"
			add_timed_idea = {
				idea = BRA_idea_supporting_military
				days = 730
			}
			custom_effect_tooltip = BRA_supporting_militaries_tt
		}

		modifier = { }

		remove_effect = {

			log = "[GetDateText]: [Root.GetName]: Decision remove BRA_decision_support_private_sector"
 }
		highlight_states = { }

		ai_will_do = {
			factor = 1

			modifier = {
				add = 3
				is_historical_focus_on = No
			}
		}
	}
}

##Major development projects
BRA_major_development_projects_decision_category = {
	BRA_transposition_saofrancisco = {

		icon = resource_production

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 887
			owns_state = 887
		}

		cost = 100
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 30.0 }
			custom_effect_tooltip = BRA_decision_transposition_sao_francisco_tt
		}

		remove_effect = {

			887 = {
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			add_manpower = 50000
		}
	}
	BRA_bacia_tocantins = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			890 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}

			885 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
	BRA_army_projects = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			890 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}

			891 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}
	BRA_improve_dockyards = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 890
			owns_state = 890
			has_full_control_of_state = 885
			owns_state = 885
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			883 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = dockyard
					level = 2
					instant_build = yes
				}
			}

			889 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = dockyard
					level = 2
					instant_build = yes
				}
			}
		}
	}
	BRA_amazonian_projects = {

		icon = generic_construction

		allowed = {
			original_tag = BRA
		}

		available = {
			has_full_control_of_state = 892
			owns_state = 892
		}

		cost = 150
		days_remove = 150 fire_only_once = yes

		ai_will_do = {
			factor = 40
			modifier = {
				political_power_daily < 0.5
				factor = 0
			}
		}

		visible = {
			has_completed_focus = BRA_major_development_projects
			original_tag = BRA
		}

		modifier = { }

		complete_effect = {
			add_manpower = -5000
			add_command_power = -5
			subtract_from_variable = { treasury = 10.0 }
			custom_effect_tooltip = BRA_decision_bacia_tocantins_tt
		}

		remove_effect = {

			892 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
}
#BRA_war_on_the_cartels_decision_category = {

	#BRA_decision_find_the_dealers = {

		#cost = 100

		#available = { }

		#
		#days_remove = 21
		#days_re_enable = 14
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 10 }
			#subtract_from_variable = { BRA_cartel_influence = 5 }
			#custom_effect_tooltip = "Costs $10 Million to Lower Cartel Influence by 5"
		#}

		#modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}

	#BRA_decision_crack_down_on_suppliers = {

		#cost = 175
		#icon = unknown

		#available = {
			#check_variable = { BRA_cartel_influence < 40 }
		#}

		#
		#days_remove = 60
		#days_re_enable = 0
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 12.5 }
			#swap_ideas = {
				#remove_idea = cartels_4
				#add_idea = BRA_idea_cartels_stage_one
			#}
			#custom_effect_tooltip = "Costs $12.5 Million to Crack Down On Suppliers"
		#}

		#modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}

	#BRA_decision_elimnate_the_local_bosses = {

		#cost = 200
		#icon = unknown

		#available = { }

		#
		#days_remove = 30
		#days_re_enable = 0
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 15 }
			#swap_ideas = {
				#remove_idea = BRA_idea_cartels_stage_one
				#add_idea = BRA_idea_cartels_stage_two
			#}
		#}

		#modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}

	#BRA_decision_seize_and_arrest = {

		#cost = 250
		#icon = unknown

		#available = {
			#has_decision = BRA_decision_crack_down_on_suppliers
		#}

		#
		#days_remove = 90
		#days_re_enable = 0
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 12.5 }
			#swap_ideas = {
				#remove_idea = BRA_idea_cartels_stage_two
				#add_idea = BRA_idea_cartels_stage_three
			#}
		#}

		#modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}

	#BRA_decision_take_out_kingpins = {

		#cost = 250
		#icon = unknown

		#available = {
			#has_decision = BRA_decision_seize_and_arrest
		#}

		#
		#days_remove = 45
		#days_re_enable = 0
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 10 }
			#swap_ideas = {
				#remove_idea = BRA_idea_cartels_stage_three
				#add_idea = BRA_idea_cartels_stage_four
			#}
		#}

		#modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}

	#BRA_decision_help_the_youth = {

		#cost = 250
		#icon = unknown

		#available = {
			#has_decision = BRA_decision_take_out_kingpins
		#}

		#
		#days_remove = 45
		#days_re_enable = 0
		#fire_only_once = no

		#complete_effect = {
			#subtract_from_variable = { treasury = 20 }
			#swap_ideas = {
				#remove_idea = BRA_idea_cartels_stage_three
				#add_idea = BRA_idea_cartels_troubled_youth
			#}
		#}

		##modifier = { }

		#remove_effect = { }

		#highlight_states = { }

		#ai_will_do = {
			#factor = 2

			#modifier = {
				#add = 1
				#is_historical_focus_on = No
			#}
		#}
	#}
#}
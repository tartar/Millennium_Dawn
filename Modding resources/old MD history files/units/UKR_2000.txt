﻿division_template = {
	name = "Mechanized Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }


	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }

	}
}
division_template = {
	name = "Mountain Warfare Brigade"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 1 y = 1 }

	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Armored Brigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		H_Engi_Bat = { x = 0 y = 3 }

	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

units = {

	division = {
		name = "Ukrainian 1st Armored Brigade"
		location = 525
		division_template = "Armored Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "Ukrainian 2nd Armored Brigade"
		location = 3686
		division_template = "Armored Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Mountain Warfare Brigade"
		location = 6460
		division_template = "Mountain Warfare Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "2nd Mountain Warfare Brigade"
		location = 6571
		division_template = "Mountain Warfare Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "1st Mechanized Brigade"
		location = 525
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "2nd Mechanized Brigade"
		location = 11479
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "3rd Mechanized Brigade"
		location = 418
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "4th Mechanized Brigade"
		location = 525
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "5th Mechanized Brigade"
		location = 11557
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "6th Mechanized Brigade"
		location = 525
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "7th Mechanized Brigade"
		location = 525
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "8th Mechanized Brigade"
		location = 3470
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "9th Mechanized Brigade"
		location = 3686
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "10th Mechanized Brigade"
		location = 11557
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "11th Mechanized Brigade"
		location = 3701
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "12th Mechanized Brigade"
		location = 3686
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	division = {
		name = "13th Mechanized Brigade"
		location = 11437
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}

	navy = {
		name = "Wijskowo-Morski"
		base = 11670
		location = 11670

		ship = { name = "Hetman Sahaydachniy" definition = frigate experience = 50 equipment = { frigate_2 = { amount = 1 owner = UKR creator = SOV } } }
		ship = { name = "Vinnytsia" definition = corvette experience = 50 equipment = { corvette_2 = { amount = 1 owner = UKR creator = SOV  version_name = "Grisha-Class" } } }
		ship = { name = "Chernihiv" definition = corvette experience = 50 equipment = { corvette_2 = { amount = 1 owner = UKR creator = SOV  version_name = "Grisha-Class" } } }
		ship = { name = "Izyaslav" definition = corvette experience = 50 equipment = { corvette_2 = { amount = 1 owner = UKR creator = SOV  version_name = "Grisha-Class" } } }
		ship = { name = "Prydniprovya" definition = corvette experience = 50 equipment = { missile_corvette_1 = { amount = 1 owner = UKR creator = SOV } } }
		ship = { name = "Uzhhorod" definition = corvette experience = 50 equipment = { missile_corvette_1 = { amount = 1 owner = UKR creator = SOV version_name = "Pauk-Class" } } }
	}

}

instant_effect = {

	add_equipment_to_stockpile = {
		type = MBT_Equipment_1 #T64
		amount = 2215
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_2 #T-72
		amount = 1650
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_3 #T-80BV
		amount = 271
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_3 #T-80BV
		amount = 271
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_3 #T-84
		amount = 6
    }
	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0 #BRDM2
		amount = 600
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_1 #BMP-1
		amount = 600
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Rec_tank_Equipment_0 #BRM-1K
		amount = 458
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_3 #BMP-2
		amount = 1434
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_4 #BMP-3
		amount = 4
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Air_IFV_Equipment_1 #BMD-1
		amount = 61
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Air_IFV_Equipment_3 #BMD-2
		amount = 78
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #BTR-60
		amount = 176
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_3 #BTR-70
		amount = 1026
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_4 #BTR-80
		amount = 456
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Air_APC_Equipment_2 #BTR-D
		amount = 44
    }

	add_equipment_to_stockpile = {
		type = artillery_equipment_0
		amount = 669
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = artillery_equipment_1 #2A65
		amount = 474
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_1 #2S19
		amount = 678
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_0 #2S3
		amount = 496
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_1 #2S5
		amount = 24
		producer = SOV
    }
	add_equipment_to_stockpile = { #BM-21 Grad
		type = SP_R_arty_equipment_0
		amount = 332
		producer = SOV
    }
	add_equipment_to_stockpile = { #9P140 Uragan
		type = SP_R_arty_equipment_1
		amount = 159
		producer = SOV
    }
	add_equipment_to_stockpile = { #9A52 Smerch
		type = SP_R_arty_equipment_2
		amount = 94
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #SS-21 Scarab
		amount = 90
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #SA-8 Osa
		amount = 950
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_1 #SA-11 Buk
		amount = 60
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_1
		#version_name = "SA-13 Strela-10"
		amount = 150
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #Mil Mi-24 Hind
		amount = 205
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Mil Mi-8
		amount = 485
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = strategic_bomber_equipment_2 #Tu-22M Blinder
		amount = 32
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = strategic_bomber_equipment_2 #Tu-22M Blinder
		amount = 32
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Strike_fighter_equipment_2 #Su-24
		amount = 104
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = CAS_equipment_2 #SU-25
		amount = 63
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter_equipment_2 #MiG-29
		amount = 219
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = AS_Fighter_equipment_2 #SU-27
		amount = 292
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_1 #Il-76 Candid
		amount = 60
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_3 #An-70
		amount = 45
    }
	add_equipment_to_stockpile = {
		type = L_Strike_fighter_equipment_1 #Aero L-39 Albatros
		amount = 345
		producer = SOV
	}
	add_equipment_production = { #An-70
		equipment = {
			type = transport_plane_equipment_3 #An-70
			#creator = "UKR"
		}
		requested_factories = 1
		progress = 0.2
		efficiency = 50
		amount = 4
	}
	if = {
		limit = { has_dlc = "Death or Dishonor" }
		SOV = {
			create_production_license = {
				target = UKR
				equipment = {
					type = Strike_fighter_equipment_2 #Su-24
					creator = "SOV"
				}
				cost_factor = 0
			}
			create_production_license = {
				target = UKR
				equipment = {
					type = CV_MR_Fighter_equipment_2 #MiG-29
					creator = "SOV"
				}
				cost_factor = 0
			}
			create_production_license = {
				target = UKR
				equipment = {
					type = CAS_equipment_1 #SU-25
					creator = "SOV"
				}
				cost_factor = 0
			}

		}

		else = {
			add_equipment_production = { #Su-24
				equipment = {
					type = Strike_fighter_equipment_2 #Su-24
					creator = "SOV"
				}
				requested_factories = 1
				progress = 0.2
				efficiency = 50
				amount = 4
			}
			add_equipment_production = { #MiG-29
				equipment = {
					type = CV_MR_Fighter_equipment_2 #MiG-29
					creator = "SOV"
				}
				requested_factories = 1
				progress = 0.2
				efficiency = 50
				amount = 4
			}
			add_equipment_production = { #SU-25
				equipment = {
					type = CAS_equipment_1 #SU-25
					creator = "SOV"
				}
				requested_factories = 1
				progress = 0.2
				efficiency = 50
				amount = 4
			}
		}
	}
}


division_template = {
	name = "Piechota" #territorial brigade
	
	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brygada P.Gorskiej" #Mountaineer brigade
	
	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		
	}
}
division_template = {
	name = "Dywizja Zmechanizowana" #Armored Cavalry Division
	
	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 2 y = 0 }
		H_Engi_Bat = { x = 2 y = 1 }
		SP_AA_Bat = { x = 2 y = 2 }
		Mech_Recce_Bat = { x = 2 y = 3 }
	}
	support = {
		
	}
}
division_template = {
	name = "Dywizja Kawalerii Pancernej" #Mechanized Division
	
	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		Mech_Inf_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 2 y = 0 }
		SP_AA_Bat = { x = 2 y = 1 }
		Mech_Recce_Bat = { x = 2 y = 2 }
		
	}
	support = {
		field_hospital = { x = 0 y = 0 }
		recon = { x = 0 y = 1 }
		engineer = { x = 0 y = 2 }
		maintenance_company = { x = 0 y = 3 }
		anti_air = { x = 0 y = 4}

	}
	priority = 2
}
division_template = {
	name = "Strzelcy Podchalanscy" #Mechanized Brigade
	
	regiments = {
		armor_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		SP_Arty_Bat = { x = 0 y = 4 }
		SP_AA_Bat = { x = 1 y = 0 }
		L_Engi_Bat = { x = 2 y = 0 }
		
	}
	support = {
	}
}
division_template = {
	name = "Kawaleria Powietrzna"
	
	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
		L_Air_assault_Bat = { x = 0 y = 1 }
	}
	support = {
		
	}
}
division_template = {
	name = "Pluk Rozpoznawczy"
	
	regiments = {
		Mech_Recce_Bat = { x = 0 y = 0 }
		Mech_Recce_Bat = { x = 0 y = 1 }
	}
	support = {
		
	}
}
division_template = {
	name = "Komandosi"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}
	support = {
	}
	priority = 2
}
division_template = {
	name = "Coastal Brigade"

	regiments = {
		Arm_Marine_Bat = { x = 0 y = 0 }
		Arm_Marine_Bat = { x = 0 y = 1 }
		Arm_Marine_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		SP_AA_Bat = { x = 0 y = 4 }
		Mech_Recce_Bat = { x = 1 y = 0 }
		H_Engi_Bat = { x = 1 y = 1 }
		
	}
	support = {
	}
}
division_template = {
	name = "Airborne Brigade"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
		
	}
	support = {
	}
}

units = {
###POW###
	division = {
		name = "2 Pomorska Dywizja Zmechanizowana"
		location = 11260		#Szczecinek
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "12 Szczecinska Dywizja Zmechanizowana"
		location = 6282		#Szczecin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "16 Pomorska Dywizja Zmechanizowana"
		location = 3380		#Elbl�g
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "8 Baltycka Dywizja Obrony Wybrzeza"
		location = 11372		#S�upsk/Koszalin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "1 Gdanska Brygada Obrony Terytorialnej" #brygaga - 50%-33% str.
		location = 362		#Danzig
		division_template = "Piechota"
		start_experience_factor = 0.25
		start_equipment_factor = 0.5
	}
###SOW###
	division = {
		name = "4 Lubuska Dywizja Zmechanizowana"
		location = 11478		#Mi�dzyrzecz
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "5 Kresowa Dywizja Zmechanizowana"
		location = 3572		#Gubin
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "10 Sudecka Dywizja Zmechanizowana"
		location = 9511		#Opole
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "11 Dywizja Kawalerii Pancernej"
		location = 3438		#Zagan
		division_template = "Dywizja Kawalerii Pancernej"
		start_experience_factor = 0.4
	}
###WOW###
	division = {
		name = "1 Warszawska Dywizja Zmechanizowana"
		location = 524		#Legionowo
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "15 Warminsko-Mazurska Dywizja Zmechanizowana"
		location = 6375		#Olsztyn
		division_template = "Dywizja Zmechanizowana"
		start_experience_factor = 0.3
	}
	division = {
		name = "25 Dywizja Kawalerii Powietrznej"
		location = 9508		#Lodz
		division_template = "Kawaleria Powietrzna"
		start_experience_factor = 0.3
	}
###KOW###
	division = {
		name = "21 Brygada Strzelcow Podhalanskich"
		location = 6499		#Rzeszow
		division_template = "Strzelcy Podchalanscy"
		start_experience_factor = 0.3
	}
	division = {
		name = "6 Brygada Desantowo-Szturmowa"
		location = 9427		#Krakow
		division_template = "Airborne Brigade"
		start_experience_factor = 0.6
	}
###
	division = {
		name = "22 Karpacka Brygada Piechoty Gorskiej"
		location = 6512		#Nysa
		division_template = "Brygada P.Gorskiej"
		start_experience_factor = 0.3
	}
	division = {
		name = "1 Pulk Specjalny Komandosow"
		location = 3544		#Warsaw
		division_template = "Komandosi"
		start_experience_factor = 0.75
	}
	division = {
		name = "2 Hrubieszowski Pulk Rozpoznawczy"
		location = 6580		#Hrubiesz�w
		division_template = "Pluk Rozpoznawczy"
		start_experience_factor = 0.3
	}
	division = {
		name = "Fromoza Gdansk"
		location = 362		#Danzig
		division_template = "Coastal Brigade"
		start_experience_factor = 0.5
	}

### NAVY OOB ###

	navy = {
		name = "3 Flotylla Okretow"
		base = 362
		location = 362

		ship = { name = "ORP Orzeł" definition = destroyer experience = 75 equipment = { destroyer_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Wilk" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV version_name = "Foxtrot class" } } }
		ship = { name = "ORP Dzik" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = SOV version_name = "Foxtrot class" } } }
		ship = { name = "ORP Orzeł" definition = diesel_attack_submarine experience = 75 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = POL creator = NOR } } }
		ship = { name = "ORP Kaszub" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL } } }
		ship = { name = "ORP Gornik" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Hutnik" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Metalowiec" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Rolnik" definition = corvette experience = 75 equipment = { missile_corvette_1 = { amount = 1 owner = POL creator = SOV } } }
		ship = { name = "ORP Generał Kazimierz Pułaski" definition = frigate experience = 75 equipment = { frigate_2 = { amount = 1 owner = POL creator = USA } } }
		
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = MBT_Equipment_2 #T-72
		amount = 586
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #PT-91
		amount = 233
    }
	add_equipment_to_stockpile = {
		type = MBT_Equipment_4 #Leopard 2A4
		amount = 128
		producer = GER
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #BDRM-2
		amount = 435
    }
	add_equipment_to_stockpile = {
		type = IFV_Equipment_1 #BMP1
		amount = 1281
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = APC_Equipment_1 #OT-64
		amount = 33
		#version_name = "OT-64"
    }
	add_equipment_to_stockpile = {
		type = artillery_equipment_0 #D-30
		amount = 364
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_0 #2S1 Gvozdika
		amount = 541
    }
	add_equipment_to_stockpile = {
		type = SP_arty_equipment_0 #M-77 Dana
		amount = 111
		producer = CZE
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #ZSU-23-4 Shilka
		amount = 420
		producer = SOV
		#version_name = "ZSU-23-4 Shilka"
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #SA-6 Gainful/SA-7
		amount = 656
		producer = SOV
		#version_name = "SA-6 Gainful"
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #SA-8 Osa
		amount = 64
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = SP_AA_Equipment_0 #SA-9 Strela-1
		amount = 232
		producer = SOV
		#version_name = "SA-9 Strela-1"
    }
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #MI-24
		amount = 43
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_helicopter_equipment_1 #Mil Mi-8
		amount = 35
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = L_Strike_fighter_equipment_1 #PZL TS-11 Iskra
		amount = 12
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_1 #Antonov An-26
		amount = 36
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_2 #MiG-29 Fulcrum
		amount = 45
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = Strike_fighter_equipment_1 #Su-22 Fitter
		amount = 98
		producer = SOV
		version_name = "Su-22 Fitter"
    }
	add_equipment_to_stockpile = {
		type = MR_Fighter_equipment_1 #MiG-21 Fishbed
		amount = 81
		producer = SOV
    }
	add_equipment_to_stockpile = {
		type = transport_plane_equipment_3 #C-295
		amount = 8
		producer = SOV
    }
	
}
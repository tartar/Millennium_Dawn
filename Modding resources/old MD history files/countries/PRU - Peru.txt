﻿capital = 303

oob = "PRU_2000"

set_convoys = 110
set_stability = 0.5

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	frigate2 = 2
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	cruiser_1 = 1
	cruiser_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	limited_conscription
}

set_politics = {
	parties = {		
		nationalist = {
			popularity = 9
		}
		reactionary = {
			popularity = 66
		}
		conservative = {
			popularity = 15
		}
		market_liberal = {
			popularity = 5
		}
		social_liberal = {
			popularity = 5
		}
	}
	
	ruling_party = reactionary
	last_election = "1995.7.28"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Alberto Fujimori"
	picture = "Alberto_Fujimori.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Valentín Paniagua"
	picture = "Valentin_Paniagua.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Alejandro Toledo"
	picture = "Alejandro_Toledo.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Alan Garcia"
	picture = "Alan_Garcia.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Ollanta Humala"
	picture = "Ollanta_Humala.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Martín Mayta"
	picture = "Martin_Matya.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Abimael Guzman"
	picture = "Abimael_Guzman.dds"
	ideology = maoist
}

create_corps_commander = {
	name = "Victor Carrera"
	picture = "generals/Victoria_Carrera.dds"
	skill = 1
}

create_corps_commander = {
	name = "Cesar Salcedo"
	picture = "generals/Cesar_Salcedo.dds"
	skill = 1
}

add_namespace = {
	name = "pru_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Leonel Cabrera Pino"
	picture = "generals/Leonel_Cabrera_Pino.dds"
	traits = { old_guard thorough_planner }
	skill = 2
}

create_corps_commander = {
	name = "Cesar Augusto Astudillo Salcedo"
	picture = "generals/Cesar_Augusto_Astudillo_Salcedo.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Dante Antonio Arevalo Abate"
	picture = "generals/Dante_Antonio_Arevalo_Abate.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Javier Ramirez Guillen"
	picture = "generals/Javier_Ramirez_Guillen.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Julio Valdez Pomareda"
	picture = "generals/Julio_Valdez_Pomareda.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Augusto Javier Villarroel Rossi"
	picture = "generals/Augusto_Javier_Villarroel_Rossi.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Luis Ramos Hume"
	picture = "generals/Luis_Ramos_Hume.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Victor Abraham Najar Carrera"
	picture = "generals/Victor_Abraham_Najar_Carrera.dds"
	traits = { ranger }
	skill = 1
}

create_navy_leader = {
	name = "Carlos Tijeda"
	picture = "admirals/Carlos_Tijeda.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Luis Paredes Lora"
	picture = "admirals/Luis_Paredes_Lora.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}

create_navy_leader = {
	name = "Victor Emanuel Pomar Calderon"
	picture = "admirals/Victor_Emanuel_Pomar_Calderon.dds"
	traits = { seawolf }
	skill = 1
}

2011.7.28 = {
	create_country_leader = {
		name = "Keiko Fujimori"
		picture = "Keiko_Fujimori.dds"
		ideology = counter_progressive_democrat
	}
}

2016.1.1 = {
	set_politics = {
		parties = {		
			nationalist = {
				popularity = 7
			}
			reactionary = {
				popularity = 40
			}
			conservative = {
				popularity = 6
			}
			market_liberal = {
				popularity = 28
			}
			social_liberal = {
				popularity = 19
			}
		}
		
		ruling_party = democratic_socialist
		last_election = 2011.7.28
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Pedro Kuczynski"
		picture = "Pedro_Kuczynski.dds"
		ideology = libertarian
	}
	create_country_leader = {
		name = "Alfredo Barnechea"
		picture = "Alfredo_Barnechea.dds"
		ideology = national_democrat
	}
	create_country_leader = {
		name = "Veronika Mendoza"
		picture = "Veronika_Mendoza.dds"
		ideology = liberalist
	}
}


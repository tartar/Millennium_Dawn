﻿capital = 104

oob = "BOS_2000"

set_convoys = 5
set_stability = 0.5

set_country_flag = country_language_bosnian
set_country_flag = country_language_croatian
set_country_flag = country_language_serbian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	limited_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 30
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 25
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "1998.12.1"
	election_frequency = 48
	elections_allowed = yes
}

add_namespace = {
	name = "bos_unit_leader"
	type = unit_leader
}

create_country_leader = {
	name = "Alija Izetbegović"
	ideology = constitutionalist 
	picture = "Alija_Izetbegovic.dds"
}

create_country_leader = {
	name = "Sefer Halilovic"
	ideology = proto_fascist 
	picture = "Sefer_Halilovic.dds"
}

create_field_marshal = {
	name = "Anto Jeleč"
	picture = "generals/Anto_Jelec.dds"
	traits = { old_guard thorough_planner }
	skill = 1
}

create_corps_commander = {
	name = "Mirko Tepšić"
	picture = "generals/Mirko_Tepsic.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Senad Mašović"
	picture = "generals/Senad_Masovic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Dragan Vuković"
	picture = "generals/Dragan_Vukovic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Husein Tursunović"
	picture = "generals/Husein_Tursunovic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Mirsad Ahmić"
	picture = "generals/Mirsad_Ahmic.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Ivica Jerkić"
	picture = "generals/Ivica_Jerkic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marko Stojčić"
	picture = "generals/Marko_Stojcic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gojko Knežević"
	picture = "generals/Gojko_Knesevic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kenan Dautović"
	picture = "generals/Kenan_Dautovic.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Tomo Kolenda"
	picture = "generals/Tomo_Kolenda.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Radovan Ilić"
	picture = "generals/Radovan_Ilic.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Enes Husejnović"
	picture = "generals/Enes_Huseinovic.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Amir Čorbo"
	picture = "generals/Amir_Corbo.dds"
	traits = { trait_engineer }
	skill = 1
}

2006.1.1 = {
	add_ideas = {
		volunteer_only
	}
}


2015.1.1 = {
	create_country_leader = {
		name = "Bakir Izetbegović"
		ideology = constitutionalist
		picture = "Bakir_Izetbegovic.dds"
	}
}

2017.1.1 = {

oob = "BOS_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	SP_R_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	unrestrained_corruption
	gdp_3
	sunni
		stable_growth
		defence_01
	edu_03
	health_05
	social_03
	bureau_03
	police_03
	volunteer_army
	volunteer_women
	small_medium_business_owners
	international_bankers
	The_Ulema
	civil_law
}
set_country_flag = gdp_3
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_international_bankers

set_country_flag = NATO_Aspirant

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 74
		}

		fascism = {
			popularity = 1
		}
		
		communism = {
			popularity = 13
		}
		
		neutrality = {
			popularity = 12
		}
		
		nationalist = {
			popularity = 0
		}
	}
	
	ruling_party = democratic
	last_election = "2014.10.12"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Nermin Niksic"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "nermin_niksic.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Milorad Dodik"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "milorad_dodik.dds"
	expire = "2065.1.1"
	ideology = Conservative
	traits = {
		#
	}
}

create_country_leader = {
	name = "Mladen Bosic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "mladen_bosic.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Fahrudin Radoncic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "fahrudin_radoncic.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Bakir Izetbegovic"
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "bakir_izetbegovic.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Denis Zvizdić"
	desc = ""
	picture = "BOS_Denis_Zvizdic.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Anto Jeleč"
	picture = "Portrait_Anto_Jelec.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_corps_commander = {
	name = "Mirko Tepšić"
	picture = "Portrait_Mirko_Tepsic.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Senad Mašović"
	picture = "Portrait_Senad_Masovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Dragan Vuković"
	picture = "Portrait_Dragan_Vukovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Husein Tursunović"
	picture = "Portrait_Husein_Tursunovic.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Mirsad Ahmić"
	picture = "Portrait_Mirsad_Ahmic.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Ivica Jerkić"
	picture = "Portrait_Ivica_Jerkic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Marko Stojčić"
	picture = "Portrait_Marko_Stojcic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gojko Knežević"
	picture = "Portrait_Gojko_Knesevic.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kenan Dautović"
	picture = "Portrait_Kenan_Dautovic.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Tomo Kolenda"
	picture = "Portrait_Tomo_Kolenda.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Radovan Ilić"
	picture = "Portrait_Radovan_Ilic.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Enes Husejnović"
	picture = "Portrait_Enes_Huseinovic.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Amir Čorbo"
	picture = "Portrait_Amir_Corbo.dds"
	traits = { trait_engineer }
	skill = 1
}

}
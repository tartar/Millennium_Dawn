﻿capital = 43

oob = "HUN_2000"

set_convoys = 140
set_stability = 0.5

set_country_flag = country_language_hungarian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	free_trade
	visegrad_group_member
}

set_politics = {

	parties = {
		conservative = {
			popularity = 26
		}
		social_democrat = {
			popularity = 29.8
		}
		reactionary = {
			popularity = 13.3
		}
		market_liberal = {
			popularity = 10.2
		}
		nationalist = {
			popularity = 5.6
		}
		communist = {
			popularity = 3.7
		}
	}
	
	ruling_party = conservative
	last_election = "1998.5.24"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = CZE
	modifier = visegrad_group
}

add_opinion_modifier = {
	target = POL
	modifier = visegrad_group
	modifier = declaration_of_friendship
}

add_opinion_modifier = {
	target = SLO
	modifier = visegrad_group
}

create_country_leader = {
	name = "Viktor Orbán"
	ideology = right_wing_conservative
	picture = "Viktor_Orban.dds"
}

2004.1.1 = {
	add_ideas = {
		idea_eu_member
	}
}

2016.1.1 = {
	set_politics = {

		parties = {
			conservative = {
				popularity = 30
			}
			social_democrat = {
				popularity = 25
			}
			reactionary = {
				popularity = 20
			}
			nationalist = {
				popularity = 10
			}
			social_liberal = {
				popularity = 5
			}
			market_liberal = {
				popularity = 5
			}
			communist = {
				popularity = 5
			}
		}
		
		ruling_party = conservative
		last_election = "1998.5.24"
		election_frequency = 48
		elections_allowed = yes
	}
}
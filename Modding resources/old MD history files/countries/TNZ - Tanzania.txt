﻿capital = 546

oob = "TNZ_2000"

set_convoys = 300
set_stability = 0.5

set_country_flag = country_language_swahili
set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
	islamist = {
	popularity = 0
	}
	fascist = {
	popularity = 0
	}
	nationalist = {
	popularity = 0
	}
	reactionary = {
	popularity = 0
	}
	conservative = {
	popularity = 10
	}
	market_liberal = {
	popularity = 0
	}
	social_liberal = {
	popularity = 5
	}
	social_democrat = {
	popularity = 25
	}
	progressive = {
	popularity = 60
	}
	democratic_socialist = {
	popularity = 0
	}
	communist = {
	popularity = 0
	}
	}
	
	ruling_party = progressive
	last_election = "1995.11.29"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Benjamin Mkapa"
	picture = "Benjamin_Mkapa.dds"
	ideology = progressive_ideology
}
create_country_leader = {
	name = "Augustine Mrema"
	picture = "Augustine_Mrema.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Ibrahim Lipumba"
	picture = "Ibrahim_Lipumba.dds"
	ideology = moderate
}
create_country_leader = {
	name = "Freeman Mbowe"
	picture = "Freeman_Mbowe.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Zitto Kabwe"
	picture = "Zitto_Kabwe.dds"
	ideology = democratic_socialist_ideology
}
create_country_leader = {
	name = "Christopher Mtikila"
	picture = "Christopher_Mtikila.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Augusto Mrema"
	picture = "Augusto_Mrema.dds"
	ideology = marxist
}
	
create_field_marshal = {
	name = "Davis Mwamunyange"
	picture = "generals/Davis_Mwamunyange.dds"
	skill = 1
}
create_corps_commander = {
	name = "Venance Mabeyo"
	picture = "generals/Venance_Mabeyo.dds"
	skill = 2
}
create_corps_commander = {
	name = "James Mwakibolwa"
	picture = "generals/James_Mwakibolwa.dds"
	skill = 1
}

create_navy_leader = {
	name = "Shabani Laswai"
	picture = "admirals/Shabani_Laswai.dds"
	skill = 1
}


2015.10.25 = {
	set_politics = {
		parties = {
			conservative = { popularity = 30 }
			social_liberal = { popularity = 10 }
			social_democrat = { popularity = 5 }
			progressive = { popularity = 55 }
		}
		ruling_party = progressive
		last_election = "2015.10.25"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "John Magufuli"
		picture = "John_Magufuli.dds"
		ideology = progressive_ideology
	}
	create_country_leader = {
		name = "Edward Lowassa"
		picture = "Edward_Lowassa.dds"
		ideology = fiscal_conservative
	}
}
capital = 353

set_convoys = 20

oob = "CDI_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1

	landing_craft = 1
}

2017.1.1 = {

add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_2
		economic_boom
		defence_01
	edu_02
	health_01
	social_01
	bureau_02
	police_02
	draft_army
	drafted_women
	international_bankers
	landowners
	small_medium_business_owners
	hybrid
}
set_country_flag = gdp_2
set_country_flag = positive_international_bankers
set_country_flag = negative_landowners
set_country_flag = positive_small_medium_business_owners

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot




set_politics = {

	parties = {
		democratic = { 
			popularity = 20
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 15
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 40
		}
		
		nationalist = { 
			popularity = 25
		}
	}
	
	ruling_party = neutrality
	last_election = "2015.10.25"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Alassane Ouattara"
	desc = ""
	picture = "CDI_Alassane_Ouattara.dds"
	expire = "2050.1.1"
	ideology = Neutral_Libertarian
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Sekou Touré"
	picture = "Portrait_Sekou_Toure.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Soumaïla Bakayoko"
	picture = "Portrait_Soumaila_Bakayko.dds"
	traits = { old_guard logistics_wizard }
	skill = 2
}

create_field_marshal = {
	name = "Richard Donwahi"
	picture = "Portrait_Richard_Donwahi.dds"
	traits = { defensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Nicolas Kouakou Kouadio"
	picture = "Portrait_Nicolas_Kouakou_Kouadio.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Chérif Ousmane"
	picture = "Portrait_Cherif_Ousmane.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "D. Doumbia"
	picture = "Portrait_D_Doumbia.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Kouame Akissi"
	picture = "Portrait_Kouame_Akissi.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Gervais Kouakou Kouassi"
	picture = "Portrait_Gervais_Kouakou_Kouassi.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_navy_leader = {
	name = ""
	picture = ""
	traits = {  }
	skill = 1
}
}
﻿capital = 868

oob = "AST_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1


	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

give_guarantee = ETI
give_guarantee = FIJ
give_guarantee = NZL

add_ideas = {
	population_growth_steady
	commonwealth_of_nations_member
	free_trade
}

set_politics = {
	parties = {
		reactionary = { popularity = 9 }
		conservative = { popularity = 6 }
		market_liberal = { popularity = 38 }
		social_liberal = { popularity = 2 }
		social_democrat = { popularity = 41 }
		progressive = { popularity = 3 }
		communist = { popularity = 1 }
	}
	
	ruling_party = market_liberal
	last_election = "1998.10.3"
	election_frequency = 36
	elections_allowed = yes
}

add_opinion_modifier = {
	target = CAN
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = ENG
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = NZL
	modifier = ANZUS
}

add_opinion_modifier = {
	target = NZL
	modifier = ANZUS_trade
}

add_opinion_modifier = {
	target = NZL
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = USA
	modifier = ANZUS
}

add_opinion_modifier = {
	target = USA
	modifier = ANZUS_trade
}

add_opinion_modifier = {
	target = USA
	modifier = five_eyes_agreement
}

create_country_leader = {
	name = "Diaa Mohamed"
	picture = "Diaa_Mohamed.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Jim Saleam"
	picture = "Jim_Saleam.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Pauline Hanson"
	picture = "Pauline_Hanson.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Henry I of Windsor"
	picture = "Harry.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Bob Katter"
	picture = "Bob_Katter.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "John Anderson"
	picture = "John_Anderson.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "John Howard"
	picture = "John_Howard.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Meg Lees"
	picture = "Meg_Lees.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Kim Beazley"
	picture = "Kim_Beazley.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Bob Brown"
	picture = "Bob_Brown.dds"
	ideology = green
}

create_country_leader = {
	name = "Collective Leadership"
	picture = "Collective_Leadership.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Peter Symon"
	picture = "Peter_Symon.dds"
	ideology = marxist
}

create_corps_commander = {
	name = "Angus Campbell"
	picture = "generals/Angus_Campbell.dds"
	skill = 1
}
create_corps_commander = {
	name = "David Morrison"
	picture = "generals/David_Morrison.dds"
	skill = 1
}
create_corps_commander = {
	name = "Sir Peter Cosgrove"
	picture = "generals/Peter_Cosgrove.dds"
	skill = 2
}
create_navy_leader = {
	name = "Timothy Barrett"
	picture = "admirals/Timothy_Barrett.dds"
	skill = 1
}
create_navy_leader = {
	name = "Mark Bonser"
	picture = "admirals/Mark_Bonser.dds"
	skill = 1
}
create_navy_leader = {
	name = "Mark Campbell"
	picture = "admirals/Mark_Campbell.dds"
	skill = 1
}
add_namespace = {
	name = "ast_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Mark Binskin"
	picture = "Portrait_Mark_Binskin.dds"
	traits = { old_guard thorough_planner }
	skill = 3
}

create_corps_commander = {
	name = "Rick Burr"
	picture = "Portrait_Rick_Burr.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Gus Gilmore"
	picture = "Portrait_Gus_Gilmore.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Paul McLachlan"
	picture = "Portrait_Paul_McLachlan.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Ben James"
	picture = "Portrait_Ben_James.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Stephen Porter"
	picture = "Portrait_Stephen_Porter.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "David Westphalen"
	picture = "Portrait_David_Westphalen.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Jeff Sengelman"
	picture = "Portrait_Jeff_Sengelman.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Gavin Leo Davis"
	picture = "Portrait_Gavin_Leo_Davis.dds"
	traits = {  }
	skill = 4
}

create_corps_commander = {
	name = "Warren McDonald"
	picture = "Portrait_Warren_McDonald.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Gavin Turnbull"
	picture = "Portrait_Gavin_Turnbull.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Robert Swanwick"
	picture = "Portrait_Robert_Swanwick.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sharon Pearce"
	picture = "Portrait_Sharon_Pearce.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "John Cantwell"
	picture = "Portrait_John_Cantwell.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Shane Caughey"
	picture = "Portrait_Shane_Coughey.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Mike Hindmarsh"
	picture = "Portrait_Mike_Hindmarsh.dds"
	traits = { desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Stuart Smith"
	picture = "Portrait_Stuart_Smith.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Simone Wilkie"
	picture = "Portrait_Simone_Wilkie.dds"
	traits = { trait_engineer }
	skill = 2
}

create_navy_leader = {
	name = "Ray Griggs"
	picture = "Portrait_Ray_Griggs.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Michael Noonan"
	picture = "Portrait_Michael_Noonan.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Stuart Mayer"
	picture = "Portrait_Stuart_Mayer.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "David Johnston"
	picture = "Portrait_David_Johnston.dds"
	traits = { air_controller }
	skill = 3
}

create_navy_leader = {
	name = "Jonathan Mead"
	picture = "Portrait_Jonathan_Mead.dds"
	traits = { fly_swatter }
	skill = 2
}

create_navy_leader = {
	name = "Michael Uzzell"
	picture = "Portrait_Michael_Uzzell.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Brett Brace"
	picture = "Portrait_Brett_Brace.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Bruce Kafer"
	picture = "Portrait_Bruce_Kafer.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Gary Wight"
	picture = "Portrait_Gary_Wight.dds"
	traits = {  }
	skill = 1
}

2001.11.22 = {
    create_country_leader = {
		name = "Simon Crean"
		picture = "Simon_Crean.dds"
		ideology = social_democrat_ideology
	}
}

2003.12.2 = {
    create_country_leader = {
		name = "Mark Latham"
		picture = "Mark_Latham.dds"
		ideology = social_democrat_ideology
	}
}

2006.12.4 = {
    create_country_leader = {
		name = "Kevin Rudd"
		picture = "Kevin_Rudd.dds"
		ideology = social_democrat_ideology
	}
}

2007.10.29 = {
    create_country_leader = {
		name = "Brendon Nelson"
		picture = "Brendan_Nelson.dds"
		ideology = libertarian
	}
}

2008.9.16 = {
    create_country_leader = {
		name = "Malcolm B. Turnbull"
		picture = "Malcolm_Turnbull.dds"
		ideology = libertarian
	}
}

2009.12.1 = {
    create_country_leader = {
		name = "Tony Abbott"
		picture = "Tony_Abbott.dds"
		ideology = libertarian
	}
}

2010.6.24 = {
    create_country_leader = {
		name = "Julia Gillard"
		picture = "Julia_Gillard.dds"
		ideology = social_democrat_ideology
	}
}

2013.1.1 = {
    create_country_leader = {
		name = "Warren Truss"
		picture = "Warren_Truss.dds"
		ideology = fiscal_conservative
	}
    create_country_leader = {
		name = "Christine Milne"
		picture = "Christine_Milne.dds"
		ideology = green
	}
}

2013.9.13 = {
    create_country_leader = {
		name = "Bill Shorten"
		picture = "Bill_Shorten.dds"
		ideology = social_democrat_ideology
	}
}

2015.4.16 = {
	set_party_name = {
		ideology = social_liberal
		long_name = AST_social_liberal_Liberal_Democrats_long
		name = AST_social_liberal_Liberal_Democrats
	}
	create_country_leader = {
		name = "David Leyonhjelm"
		picture = "David_Leyonhjelm.dds"
		ideology = centrist
	}
}

2016.6.1 = {
	set_politics = {
        last_election = "2013.9.7"
		ruling_party = market_liberal
		elections_allowed = yes
		parties = {
			social_liberal = {
				popularity = 0.5
			}
			conservative = {
				popularity = 4.6
			}
			market_liberal = {
				popularity = 42.85
			}
			progressive = {
				popularity = 10.2
			}
			reactionary = {
				popularity = 0.5
            }
            social_democrat = {
				popularity = 40.35
			}
			nationalist = {
				popularity = 1
			}
	    }
	}
	
	create_country_leader = {
		name = "Richard Di Natale"
		picture = "Richard_Di_Natale.dds"
		ideology = green
	}
	create_country_leader = {
		name = "Barnaby Joyce"
		picture = "Barnaby_Joyce.dds"
		ideology = fiscal_conservative
	}

	create_country_leader = {
		name = "Bob Briton"
		picture = "Bob_Briton.dds"
		ideology = marxist
	}
}
2017.1.1 = {

oob = "AST_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1 
	infantry_weapons5 = 1 
	
	 #2005
	
	combat_eng_equipment = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1 #1985
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	command_control_equipment4 = 1 #2015
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1
	land_Drone_equipment3 = 1 #2015
	
	Early_APC = 1 #Vehicle Design
	
	APC_1 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	
	MBT_1 = 1
	
	ENG_MBT_1 = 1
	
	Rec_tank_0 = 1 #1965
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	util_vehicle_equipment_5 = 1
	
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	
	L_Strike_fighter1 = 1

	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1
	
	#Anzac-Class
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	
	#Collins-Class
	diesel_attack_submarine_1 = 1
	diesel_attack_submarine_2 = 1
	diesel_attack_submarine_3 = 1
	
	#Canberra-Class
	LHA_0 = 1
	LHA_1 = 1
	LHA_2 = 1
	
	landing_craft = 1
	
	early_helicopter = 1
	transport_helicopter1 = 1
	attack_helicopter1 = 1

}

set_variable = { var = debt value = 429 }
set_variable = { var = int_investments value = 135 }
set_variable = { var = treasury value = 58 }
set_variable = { var = tax_rate value = 40 }

add_ideas = {
	pop_050
	modest_corruption
	pluralist
	gdp_9
	    stable_growth
		defence_02
	edu_04
	export_economy
	health_04
	social_05
	bureau_02
	police_02
	rentier_state
    export_economy
	volunteer_army
	volunteer_women
	Major_Non_NATO_Ally
	intervention_limited_interventionism
	western_country
	medium_far_right_movement
	industrial_conglomerates
	landowners
	small_medium_business_owners
	common_law
}

set_country_flag = gdp_9
set_country_flag = TPP_Signatory
set_country_flag = Major_Non_NATO_Ally
set_country_flag = Major_Importer_US_Arms #Trends-in-international-arms-transfers-2016.pdf
set_country_flag =enthusiastic_industrial_conglomerates
set_country_flag =positive_landowners
set_country_flag =positive_small_medium_business_owners


add_opinion_modifier = { target = SOL modifier = melanesian_diplomacy }
add_opinion_modifier = { target = PAP modifier = melanesian_diplomacy }
add_opinion_modifier = { target = FIJ modifier = melanesian_diplomacy }

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot
	set_politics = {

	parties = {
		democratic = { 
			popularity = 93
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 2
		}
		
		nationalist = { 
			popularity = 5
		}
	}
	
	ruling_party = democratic
	last_election = "2016.7.2"
	election_frequency = 36
	elections_allowed = yes
}

create_country_leader = {
	name = "Malcolm Turnbull"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "AST_malcolm-turnbull.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}
create_corps_commander = {
	name = "Angus Campbell"
		picture = "gen_Angus_Campbell.dds"
	traits = {  }
	skill = 2
}
create_corps_commander = {
	name = "Gus Gilmore"
		picture = "gen_Gus_Gilmore.dds"
	traits = {  }
	skill = 3
}
create_corps_commander = {
	name = "Rick Burr"
		picture = "gen_Rick_Burr.dds"
	traits = {  }
	skill = 2
}
create_navy_leader = {
	name = "Tim Barret"
		picture = "adm_Tim_Barret.dds"
	traits = {  }
	skill = 4
}
create_navy_leader = {
	name = "Ray Griggs"
		picture = "adm_Ray_Griggs.dds"
	traits = {  }
	skill = 3
}
create_field_marshal = {
	name = "Mark Binskin"
	picture = "Portrait_Mark_Binskin.dds"
	traits = { old_guard thorough_planner }
	skill = 4
}

create_corps_commander = {
	name = "Rick Burr"
	picture = "Portrait_Rick_Burr.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Paul McLachlan"
	picture = "Portrait_Paul_McLachlan.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Ben James"
	picture = "Portrait_Ben_James.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Stephen Porter"
	picture = "Portrait_Stephen_Porter.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "David Westphalen"
	picture = "Portrait_David_Westphalen.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Jeff Sengelman"
	picture = "Portrait_Jeff_Sengelman.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Gavin Leo Davis"
	picture = "Portrait_Gavin_Leo_Davis.dds"
	traits = {  }
	skill = 4
}

create_corps_commander = {
	name = "Warren McDonald"
	picture = "Portrait_Warren_McDonald.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Gavin Turnbull"
	picture = "Portrait_Gavin_Turnbull.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Robert Swanwick"
	picture = "Portrait_Robert_Swanwick.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Sharon Pearce"
	picture = "Portrait_Sharon_Pearce.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "John Cantwell"
	picture = "Portrait_John_Cantwell.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Shane Caughey"
	picture = "Portrait_Shane_Coughey.dds"
	traits = { hill_fighter }
	skill = 2
}

create_corps_commander = {
	name = "Mike Hindmarsh"
	picture = "Portrait_Mike_Hindmarsh.dds"
	traits = { desert_fox }
	skill = 2
}

create_corps_commander = {
	name = "Stuart Smith"
	picture = "Portrait_Stuart_Smith.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Simone Wilkie"
	picture = "Portrait_Simone_Wilkie.dds"
	traits = { trait_engineer }
	skill = 2
}

create_navy_leader = {
	name = "Ray Griggs"
	picture = "Portrait_Ray_Griggs.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Tim Barrett"
	picture = "Portrait_Tim_Barrett.dds"
	traits = { ironside }
	skill = 3
}

create_navy_leader = {
	name = "Michael Noonan"
	picture = "Portrait_Michael_Noonan.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Stuart Mayer"
	picture = "Portrait_Stuart_Mayer.dds"
	traits = { spotter }
	skill = 2
}

create_navy_leader = {
	name = "David Johnston"
	picture = "Portrait_David_Johnston.dds"
	traits = { air_controller }
	skill = 3
}

create_navy_leader = {
	name = "Jonathan Mead"
	picture = "Portrait_Jonathan_Mead.dds"
	traits = { fly_swatter }
	skill = 2
}

create_navy_leader = {
	name = "Michael Uzzell"
	picture = "Portrait_Michael_Uzzell.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Brett Brace"
	picture = "Portrait_Brett_Brace.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Bruce Kafer"
	picture = "Portrait_Bruce_Kafer.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Gary Wight"
	picture = "Portrait_Gary_Wight.dds"
	traits = {  }
	skill = 1
}

}
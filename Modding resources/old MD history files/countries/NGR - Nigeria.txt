﻿capital = 767

oob = "NGR_2000"

set_convoys = 260
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_hausa
set_country_flag = country_language_yoruba
set_country_flag = country_language_igbo

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {
	parties = {
		islamist = { popularity = 2 }
		fascist = { popularity = 0 }
		nationalist = { popularity = 2 }
		reactionary = { popularity = 20 }
		conservative = { popularity = 49 }
		market_liberal = { popularity = 0 }
		social_liberal = { popularity = 3 }
		social_democrat = { popularity = 3 }
		progressive = { popularity = 15 }
		democratic_socialist = { popularity = 0 }
		communist = { popularity = 6 }
	}
	ruling_party = conservative
	last_election = "1999.2.20"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Goodluck Jonathan"
	picture = "Goodluck_Jonathan.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Muhammadu Buhari"
	picture = "Muhammadu_Buhari.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Okey Nwosu"
	picture = "Okey_Nwosu.dds"
	ideology = oligarchist
}

create_country_leader = {
	name = "Adebisi Akande"
	picture = "Adebisi_Akande.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Joseph Avazi"
	picture = "Joseph_Avazi.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Segun Sango"
	picture = "Segun_Sango.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Alhaji Abdulkadir Abdulsalam"
	picture = "Alhaji_A_Abdulsalam.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Tanko Yinusa"
	picture = "Tanko_Yinusa.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Abubakar Shekau"
	picture = "Abubakar_Shekau.dds"
	ideology = islamic_authoritarian
}

add_namespace = {
	name = "ngr_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "T.Y. Buratai"
	picture = "generals/T_Y_Buratai.dds"
	traits = { old_guard logistics_wizard }
	skill = 1
}

create_field_marshal = {
	name = "K.T.J. Minimah"
	picture = "generals/K_T_J_Minimah.dds"
	traits = { inspirational_leader }
	skill = 1
}

create_corps_commander = {
	name = "A. Oyebade"
	picture = "generals/A_Oyebade.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "C.M. Abraham"
	picture = "generals/C_M_Abraham.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "P.J. Dauke"
	picture = "generals/P_J_Dauke.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "E.O. Udoh"
	picture = "generals/E_O_Udoh.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "V.O. Ezugwu"
	picture = "generals/V_O_Ezugwu.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "E.B. Oyefolu"
	picture = "generals/E_B_Oyefolu.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "A.B. Abubakar"
	picture = "generals/A_B_Abubakar.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "M.A. Koleoso"
	picture = "generals/M_A_Koleoso.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "S. Abubakar"
	picture = "generals/Sadiq_Abubakar.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kenneth Minimah"
	picture = "generals/Kenneth_Minimah.dds"
	skill = 2
}

create_corps_commander = {
	name = "Martin Luther Agwai"
	picture = "generals/Martin_Luther_Agwai.dds"
	skill = 2
}

create_corps_commander = {
	name = "Abayomi Olonisakin"
	picture = "generals/Abayomi_Olonisakin.dds"
	skill = 1
}

create_corps_commander = {
	name = "Abdulrahman Bello Dambazau"
	picture = "generals/Abdulrahman_B_Dambazau.dds"
	skill = 1
}

create_corps_commander = {
	name = "Alexander Ogomudia"
	picture = "generals/Alexander_Ogomudia.dds"
	skill = 1
}

create_corps_commander = {
	name = "Andrew Azazi"
	picture = "generals/Andrew_Azazi.dds"
	skill = 1
}

create_corps_commander = {
	name = "Turker Yusuf Buratai"
	picture = "generals/Turker_Yusuf_Buratai.dds"
	skill = 1
}

create_navy_leader = {
	name = "D. J. Ezeoba"
	picture = "admirals/DJ_Ezeoba.dds"
	skill = 1
}

create_navy_leader = {
	name = "Ishaya Ibrahim"
	picture = "admirals/.dds"
	skill = 1
}

create_navy_leader = {
	name = "U. O. Jibrin"
	picture = "admirals/UO_Jibrin.dds"
	traits = { old_guard_navy }
	skill = 2
}

create_navy_leader = {
	name = "I.E. Ibas"
	picture = "admirals/Ibok_Ete_Ekwe_Ibas.dds"
	traits = { old_guard_navy }
	skill = 2
}

create_navy_leader = {
	name = "U.O. Jibrin"
	picture = "admirals/U_O_Jibrin.dds"
	traits = { blockade_runner }
	skill = 1
}

2014.1.1 = { add_stability = 0.05 }

2015.3.28 = {
	set_politics = {
		parties = {
			islamist = { popularity = 18 }
			fascist = { popularity = 4 }
			nationalist = { popularity = 0 }
			reactionary = { popularity = 2 }
			conservative = { popularity = 30 }
			market_liberal = { popularity = 2 }
			social_liberal = { popularity = 40 }
			social_democrat = { popularity = 4 }
			progressive = { popularity = 0 }
			democratic_socialist = { popularity = 0 }
			communist = { popularity = 0 }
		}
		ruling_party = social_liberal
		last_election = "2015.3.28"
		election_frequency = 48
		elections_allowed = yes
	}
}
﻿capital = 293

oob = "YEM_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	islamic_sectarian_conflicts
}

set_politics = {
	parties = {
		islamist = {
			popularity = 19
		}
		nationalist = {
			popularity = 21
		}
		reactionary = {
			popularity = 31
		}
		conservative = {
			popularity = 10
		}
		communist = {
			popularity = 11
		}
		social_liberal = {
			popularity = 8
		}
	}
	ruling_party = reactionary
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Ali Abdullah Saleh"
	picture = "Ali_Abdullah_Saleh.dds"
	ideology = oligarchist
}

create_country_leader = {
	name = "Ageel bin Muhammad al-Badr"
	picture = "Ageel_bin_Muhammadd_al-Badr.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Abdul-Malik al-Houthi"
	picture = "Abdul-Malik_al-Houthi.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Hassan Zaid"
	picture = "Hassaan_Zaid.dds"
	ideology = right_wing_conservative
}

create_country_leader = {
	name = "Sadiq al-Ahmar of Hashid"
	picture = "Sadiq.dds"
	ideology = falangist
}

create_country_leader = {
	name = "Abdulrahman Al-Saqqaf"
	picture = "Saqqaf.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = { 
    name = "Muhammad Al-Makhlafi"
	picture = "al-Makhlafi.dds"
 	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Aidaroos al-Zubaidi"
	picture = "al-Zubaidi.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Mohammed Abdul Rahman al-Rubaie"
	picture = "Rubaie.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Razia Al-Mutawakkil"
	picture = "Razia_Mutawakkil.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Qassem Salam"
	picture = "Kasim_Salam.dds"
	ideology = autocrat
}

create_field_marshal = {
	name = "Ahmed Saleh"
	picture = generals/Ahmed_bin_Saleh.dds
	skill = 1
}

create_field_marshal = {
	name = "Ali Mohsen al-Ahmar" 
	picture = generals/Ahmar.dds
	skill = 1
}

create_field_marshal = { 
	name = "Salem Nasser Qatn"
	picture = generals/Salem_Qatn.dds
	skill = 2
}

create_corps_commander = {
	name = "Mahmoud al-Subaihi" 
	picture = generals/Subaihi.dds
	skill = 1
}

create_corps_commander = { 
	name = "Thabt Jawas"
	picture = generals/Jawas.dds
	skill = 2
}

create_corps_commander = { 
	name = "Salem Ali Qatan"
	picture = "generals/Qataan.dds"
	skill = 2
}

create_corps_commander = { 
	name = "Ali al-Jaafy"
	picture = "generals/Ali_Jaafy.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Abdulrab al-Shadadi"
	picture = "generals/Abdulrab.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Adel Al-Qumeiri"
	picture = "generals/Adel_Alqumeiri.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Abdul Hakim Al-Safwani"
	picture = "generals/Al_Safwai.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Abdulrahman Al-Halili"
	picture = "generals/Al-Halili.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Mfarrih Bahibh"
	picture = "generals/Mfarrih_Bahih.dds"
	skill = 2
}

create_corps_commander = { 
	name = "Hussein Saleh Ziad"
	picture = "generals/Saleh_Ziad.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Adnan Al-Hammadi"
	picture = "generals/Adnan.dds"
	skill = 2
}

create_corps_commander = { 
	name = "Abdrabbo Ahmed Al-Kashibi"
	picture = "generals/Kashibi.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Faraj Al-Bassani"
	picture = "generals/Faraj_Al-Bassani.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Saif Al-Yafei"
	picture = "generals/Yafie.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Ali Mohsen Muthanna"
	picture = "generals/Muthanna.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Ahmed Ali al-Ashwal"
	picture = "generals/Ahmed_Ali.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Mohammed Nasir Al-Eatifi"
	picture = "generals/Nasir_Eatifi.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Mohammed Ali Al-Numeiri"
	picture = "generals/Numeiri.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Ahmed Hussein Dahan"
	picture = "generals/Ahmed_Dahan.dds"
	skill = 1
}

create_corps_commander = { 
	name = "Nasir Al-Nuba"
	picture = "generals/Naisr.dds"
	skill = 2
}

create_corps_commander = { 
	name = "Ahmed Ali Jaafar"
	picture = "generals/Jaafar.dds"
	traits = { trait_mountaineer } 
	skill = 1
}

create_corps_commander = { 
	name = "Askar Hamoud Dars"
	picture = "generals/Ahmed_Askar.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = { 
	name = "Naji Al-Surhi"
	picture = "generals/Surhi.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = { 
	name = "Ahmed Jamara"
	picture = "generals/Jamara.dds"
	traits = { panzer_leader } 
	skill = 1
}

create_corps_commander = { 
	name = "Samir Al-Haj"
	picture = "generals/Haj.dds"
	traits = { trickster }
	skill = 1
}

create_navy_leader = { 
	name = "Abdullah Ali Al-Nakhaie" 
	picture = "admirals/Ali_Nakhaie"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}

2012.1.1 = {
	set_politics = {
	
		parties = {
			islamist = {
				popularity = 23
			}
			nationalist = {
				popularity = 15
			}
			reactionary = {
				popularity = 30
			}
			conservative = {
				popularity = 7
			}
			communist = {
				popularity = 16
			}
			social_liberal = {
				popularity = 9
			}
		}
		
		ruling_party = reactionary
		last_election = "2012.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	create_country_leader = {
		name = "Abd Rabbuh Mansur Hadi"
		picture = "Abd_Rabbuh_Mansuh_Hadi.dds"
		ideology = oligarchist
	}
}

2015.1.1 = {	#Yemeni Civil War
	set_stability = 0.5
}
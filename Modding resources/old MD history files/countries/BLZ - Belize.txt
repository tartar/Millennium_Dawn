﻿capital = 311

oob = "BLZ_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_kriol

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 35
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 20
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}


2000.1.1 = {
	create_country_leader = {
		name = "Said Musa"
		ideology = christian_democrat
		picture = "Said_Musa.dds"
	}
}

2015.1.1 = {
	create_country_leader = {
		name = "Dean Barrow"
		ideology = constitutionalist
		picture = "Dean_Barrow.dds"
	}
}
2017.1.1 = {

oob = "BLZ_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	command_control_equipment = 1
	Anti_tank_0 = 1
	Anti_Air_0 = 1
}

add_ideas = {
	pop_050
	rampant_corruption
	christian
	gdp_3
	    stagnation
		defence_01
	edu_04
	health_03
	social_01
	bureau_02
	police_03
	volunteer_army
	volunteer_women
	international_bankers
	farmers
	fossil_fuel_industry
	common_law
	cartels_1
}
set_country_flag = gdp_3
set_country_flag = enthusiastic_international_bankers
set_country_flag = positive_fossil_fuel_industry

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 99
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 1
			#banned = no #default is no
		}
	}
	
	ruling_party = democratic
	last_election = "2015.11.4"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Dean Barrow"
	desc = "POLITICS_AGUSTIN_PEDRO_JUSTO_DESC"
	picture = "BLZ_Dean_Barrow.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "David Nejemiah Jones"
	picture = "Portrait_David_Jones.dds"
	traits = { organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Steven Ortega"
	picture = "Portrait_Steven_Ortega.dds"
	traits = { fast_planner }
	skill = 1
}

create_corps_commander = {
	name = "Roberto Pop"
	picture = "Portrait_Roberto_Pop.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Allen Whylie"
	picture = "Portrait_Allen_Whylie.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Gilbert Gordon"
	picture = "Portrait_Gilbert_Gordon.dds"
	traits = { trickster }
	skill = 1
}

create_navy_leader = {
	name = "John Borland"
	picture = "Portrait_John_Borland.dds"
	traits = { superior_tactician }
	skill = 1
}

}
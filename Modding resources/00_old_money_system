calculate_interest_rate_ai = {
	set_variable = { var = interest_rate value = debt }

	#Base interest  rate is determined by your GDP/C
	if = {
		limit = { has_idea = gdp_1 }
		multiply_variable = { var = interest_rate value = 27 }
	}
	else_if = {
		limit = { has_idea = gdp_2 }
		multiply_variable = { var = interest_rate value = 23 }
	}
	else_if = {
		limit = { has_idea = gdp_3 }
		multiply_variable = { var = interest_rate value = 19 }
	}
	else_if = {
		limit = { has_idea = gdp_4 }
		multiply_variable = { var = interest_rate value = 16.5 }
	}
	else_if = {
		limit = { has_idea = gdp_5 }
		multiply_variable = { var = interest_rate value = 13.5 }
	}
	else_if = {
		limit = { has_idea = gdp_6 }
		multiply_variable = { var = interest_rate value = 10.6 }
	}
	else_if = {
		limit = { has_idea = gdp_7 }
		multiply_variable = { var = interest_rate value = 8.2 }
	}
	else_if = {
		limit = { has_idea = gdp_8 }
		multiply_variable = { var = interest_rate value = 6.1 }
	}
	else = { multiply_variable = { var = interest_rate value = 5 } }

	#interest  rate is modifier by the size_modifier variable ("Size of the country")
	divide_variable = { var = interest_rate value = size_modifier }
	#You divide by 365 as you don't pay say 10% interest  every day but yearly
	divide_variable = { var = interest_rate value = 365 }

	#interest  rate is modified based on some ideas
	if = {
		limit = { has_idea = petro_dollar }
		subtract_from_variable = { var = interest_rate value = 2 }
	}
	if = {
		limit = { has_idea = the_euro }
		subtract_from_variable = { var = interest_rate value = 1 }
	}
	if = {
		limit = { has_idea = JAP_deflation }
		subtract_from_variable = { var = interest_rate value = 9 }
	}
	if = {
		limit = {
			OR = {
				has_idea = wall_street
				has_idea = international_bankers
			}
		}
		subtract_from_variable = { var = interest_rate value = 1 }
	}
	if = {
		limit = { has_country_flag = bailout_given }
		subtract_from_variable = { var = interest_rate value = 10 }
	}
	if = {
		limit = { has_idea = NIG_efficent_government_spending_reform_idea }
		subtract_from_variable = { var = interest_rate value = 3 }
	}
	if = {
		limit = { has_country_flag = SOV_overcoming_the_1998_financial_crisis }
		subtract_from_variable = { var = interest_rate value = 2 }
	}
	if = {
		limit = { has_idea = ITA_debt_market_trust }
		add_to_variable = { var = interest_rate value = bond_markets_trust }
	}

	clamp_variable = {
		var = interest_rate
		min = 0.8
		max = 50
	}
}

update_military_rate_ai = {
	### Defence Spending ###

	#There are two parts to Mil spending "Military spending" and personel cost
	#Military spending is basically Mil/Nav factories modified by defense spending and GDP
	set_temp_variable = { temp_defence_spend = num_of_military_factories }
	add_to_temp_variable = { temp_defence_spend = num_of_naval_factories }

	if = { limit = { has_idea = defence_00 }
		divide_temp_variable = { temp_defence_spend = 3000 }
	}
	else_if = {
		limit = { has_idea = defence_01 }
		divide_temp_variable = { temp_defence_spend = 300 }
	}
	else_if = {
		limit = { has_idea = defence_02 }
		divide_temp_variable = { temp_defence_spend = 150 }
	}
	else_if = {
		limit = { has_idea = defence_03 }
		divide_temp_variable = { temp_defence_spend = 100 }
	}
	else_if = {
		limit = { has_idea = defence_04 }
		divide_temp_variable = { temp_defence_spend = 75 }
	}
	else_if = {
		limit = { has_idea = defence_05 }
		divide_temp_variable = { temp_defence_spend = 50 }
	}
	else_if = {
		limit = { has_idea = defence_06 }
		divide_temp_variable = { temp_defence_spend = 37 }
	}
	else_if = {
		limit = { has_idea = defence_07 }
		divide_temp_variable = { temp_defence_spend = 30 }
	}
	else_if = {
		limit = { has_idea = defence_08 }
		divide_temp_variable = { temp_defence_spend = 24 }
	}
	else_if = {
		limit = { has_idea = defence_09 }
		divide_temp_variable = { temp_defence_spend = 15 }
	}

	### Land ###
	#Below sets up the "personel cost for mil spending
	#Each units has a cost which is modified by gdp, conscription laws and training laws
	set_temp_variable = { special_btn = num_of_battalions_with_type@Special_Forces }

	set_temp_variable = { elite_btn = num_of_battalions_with_type@L_Air_assault_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Arm_Air_assault_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@L_Air_Inf_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mot_Air_Inf_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mech_Air_Inf_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Arm_Air_Inf_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@L_Marine_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mot_Marine_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mech_Marine_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Arm_Marine_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@SP_AA_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@L_Engi_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@H_Engi_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@L_Recce_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mot_Recce_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Mech_Recce_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@Arm_Recce_Bat }
	add_to_temp_variable = { elite_btn = num_of_battalions_with_type@armor_Recce_Bat }

	set_temp_variable = { regular_btn = num_of_battalions_with_type@L_Inf_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@Mot_Inf_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@Mech_Inf_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@Arm_Inf_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@armor_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@Arty_Bat }
	add_to_temp_variable = { regular_btn = num_of_battalions_with_type@SP_Arty_Bat }

	set_temp_variable = { irregular_btn = num_of_battalions_with_type@Militia_Bat }
	add_to_temp_variable = { irregular_btn = num_of_battalions_with_type@Mot_Militia_Bat }

	multiply_temp_variable = { special_btn = 5 }
	set_temp_variable = { land_count = special_btn }
	multiply_temp_variable = { elite_btn = 3 }
	add_to_temp_variable = { land_count = elite_btn }
	multiply_temp_variable = { regular_btn = 2 }
	add_to_temp_variable = { land_count = regular_btn }
	add_to_temp_variable = { land_count = irregular_btn }

	divide_temp_variable = { land_count = 4 } #control factor

	### Navy ###
	set_temp_variable = { carrier_count = num_ships_with_type@Nuclear_carrier }
	add_to_temp_variable = { carrier_count = num_ships_with_type@carrier }

	set_temp_variable = { capital_count = num_ships_with_type@nuclear_cruiser }
	add_to_temp_variable = { capital_count = num_ships_with_type@cruiser }
	add_to_temp_variable = { capital_count = num_ships_with_type@LHA }
	add_to_temp_variable = { capital_count = num_ships_with_type@LPD }
	add_to_temp_variable = { capital_count = num_ships_with_type@attack_submarine }
	add_to_temp_variable = { capital_count = num_ships_with_type@missile_submarine }

	set_temp_variable = { destroyer_count = num_ships_with_type@destroyer }

	set_temp_variable = { minor_count = num_ships_with_type@frigate }
	add_to_temp_variable = { minor_count = num_ships_with_type@corvette }
	add_to_temp_variable = { minor_count = num_ships_with_type@diesel_attack_submarine }

	multiply_temp_variable = { carrier_count = 20 }
	set_temp_variable = { navy_count = carrier_count }
	multiply_temp_variable = { capital_count = 4 }
	add_to_temp_variable = { navy_count = capital_count }
	multiply_temp_variable = { destroyer_count = 2 }
	add_to_temp_variable = { navy_count = destroyer_count }
	add_to_temp_variable = { navy_count = minor_count }

	### Airforce ###
	set_temp_variable = { cheap_airforce = num_equipment@attack_helicopter_equipment }
	add_to_temp_variable = { cheap_airforce = num_equipment@L_Strike_fighter_equipment }
	add_to_temp_variable = { cheap_airforce = num_equipment@CV_L_Strike_fighter_equipment }
	set_temp_variable = { airforce_count = cheap_airforce }

	set_temp_variable = { regular_airforce = num_equipment@MR_Fighter_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@CV_MR_Fighter_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@AS_Fighter_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@Strike_fighter_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@Int_Fighter_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@Air_UAV_equipment }
	add_to_temp_variable = { regular_airforce = num_equipment@CAS_equipment }


	multiply_temp_variable = { regular_airforce = 3 }
	add_to_temp_variable = { airforce_count = regular_airforce }

	set_temp_variable = { expensive_airforce = num_equipment@transport_plane_equipment }
	add_to_temp_variable = { expensive_airforce = num_equipment@nav_plane_equipment }
	multiply_temp_variable = { expensive_airforce = 5 }
	add_to_temp_variable = { airforce_count = expensive_airforce }

	set_temp_variable = { strategic_airforce = num_equipment@strategic_bomber_equipment }
	multiply_temp_variable = { strategic_airforce = 14 }
	add_to_temp_variable = { airforce_count = strategic_airforce }

	divide_temp_variable = { airforce_count = 14 } #control factor

	### Prevent exploit
	set_temp_variable = { army_extra = num_equipment@command_control_equipment } #to prevent disband exploit
	divide_temp_variable = { army_extra = 30 } #control factor

	### Add up armed forces
	set_temp_variable = { personnel_cost = land_count }
	add_to_temp_variable = { personnel_cost = army_extra }
	add_to_temp_variable = { personnel_cost = navy_count }
	add_to_temp_variable = { personnel_cost = airforce_count }

	if = { limit = { has_idea = partial_draft_army }
		divide_temp_variable = { personnel_cost = 1.4 }
	}
	else_if = {
		limit = { has_idea = draft_army }
		divide_temp_variable = { personnel_cost = 1.8 }
	}
	else_if = {
		limit = { has_idea = no_military }
		divide_temp_variable = { personnel_cost = 2.2 }
	}

	if = {
		limit = { has_idea = officer_baptism_by_fire }
		#do nothing
	}
	else_if = {
		limit = { has_idea = officer_basic_training }
		multiply_temp_variable = { personnel_cost = 1.05 }
	}
	else_if = {
		limit = { has_idea = officer_advanced_training }
		multiply_temp_variable = { personnel_cost = 1.1 }
	}
	else_if = {
		limit = { has_idea = officer_military_school }
		multiply_temp_variable = { personnel_cost = 1.15 }
	}
	else_if = {
		limit = { has_idea = officer_military_academy }
		multiply_temp_variable = { personnel_cost = 1.2 }
	}
	else_if = {
		limit = { has_idea = officer_international_education }
		multiply_temp_variable = { personnel_cost = 1.25 }
	}

	divide_temp_variable = { personnel_cost = 500 } #control factor

	if = {
		limit = { has_idea = gdp_12 }
		multiply_temp_variable = { personnel_cost = 0.7 }
	}
	else_if = { limit = { has_idea = gdp_11 }
		multiply_temp_variable = { personnel_cost = 0.565 }
		divide_temp_variable = { temp_defence_spend = 1.6 }
	}
	else_if = { limit = { has_idea = gdp_10 }
		multiply_temp_variable = { personnel_cost = 0.43 }
		divide_temp_variable = { temp_defence_spend = 1.6 }
	}
	else_if = { limit = { has_idea = gdp_9 }
		multiply_temp_variable = { personnel_cost = 0.35 }
		divide_temp_variable = { temp_defence_spend = 2 }
	}
	else_if = { limit = { has_idea = gdp_8 }
		multiply_temp_variable = { personnel_cost = 0.3 }
		divide_temp_variable = { temp_defence_spend = 2.5 }
	}
	else_if = { limit = { has_idea = gdp_7 }
		multiply_temp_variable = { personnel_cost = 0.245 }
		divide_temp_variable = { temp_defence_spend = 3 }
	}
	else_if = { limit = { has_idea = gdp_6 }
		multiply_temp_variable = { personnel_cost = 0.205 }
		divide_temp_variable = { temp_defence_spend = 3.6 }
	}
	else_if = { limit = { has_idea = gdp_5 }
		multiply_temp_variable = { personnel_cost = 0.165 }
		divide_temp_variable = { temp_defence_spend = 4.2 }
	}
	else_if = { limit = { has_idea = gdp_4 }
		multiply_temp_variable = { personnel_cost = 0.125 }
		divide_temp_variable = { temp_defence_spend = 4.9 }
	}
	else_if = { limit = { has_idea = gdp_3 }
		multiply_temp_variable = { personnel_cost = 0.098 }
		divide_temp_variable = { temp_defence_spend = 5.7 }
	}
	else_if = { limit = { has_idea = gdp_2 }
		multiply_temp_variable = { personnel_cost = 0.071 }
		divide_temp_variable = { temp_defence_spend = 6.7 }
	}
	else_if = { limit = { has_idea = gdp_1 }
		multiply_temp_variable = { personnel_cost = 0.05 }
		divide_temp_variable = { temp_defence_spend = 7.8 }
	}


	set_variable = { var = tooltip_personnel_cost value = personnel_cost }
	add_to_temp_variable = { personnel_cost = temp_defence_spend }

	if = { limit = { check_variable = { var = defence_gain value = personnel_cost compare = not_equals } }
		#See Treasury reset comment at top of page
		subtract_from_variable = { var = expense_gain value = defence_gain }
		add_to_variable = { var = treasury_rate value = defence_gain }

		set_variable = { var = defence_gain value = personnel_cost }

		add_to_variable = { var = expense_gain value = defence_gain }
		subtract_from_variable = { var = treasury_rate value = defence_gain }
		update_display_expense = yes
	}
}

update_bureaucracy_rate_ai = {

	if = {
		limit = { has_idea = bureau_01 }
		subtract_from_variable = { var = expense_gain value = bureaucracy_gain }
		add_to_variable = { var = treasury_rate value = bureaucracy_gain }
		set_variable = { var = bureaucracy_gain value = size_modifier }
		multiply_variable = { bureaucracy_gain = 100 }
		multiply_variable = { var = bureaucracy_gain value = 0.01 }
		round_variable = bureaucracy_gain
		divide_variable = { bureaucracy_gain = 100 }
	}
	else_if = {
		limit = { has_idea = bureau_02 }
		subtract_from_variable = { var = expense_gain value = bureaucracy_gain }
		add_to_variable = { var = treasury_rate value = bureaucracy_gain }
		set_variable = { var = bureaucracy_gain value = size_modifier }
		multiply_variable = { bureaucracy_gain = 100 }
		multiply_variable = { var = bureaucracy_gain value = 0.02 }
		round_variable = bureaucracy_gain
		divide_variable = { bureaucracy_gain = 100 }

	}
	else_if = {
		limit = { has_idea = bureau_03 }
		subtract_from_variable = { var = expense_gain value = bureaucracy_gain }
		add_to_variable = { var = treasury_rate value = bureaucracy_gain }
		set_variable = { var = bureaucracy_gain value = size_modifier }
		multiply_variable = { bureaucracy_gain = 100 }
		multiply_variable = { var = bureaucracy_gain value = 0.03 }
		round_variable = bureaucracy_gain
		divide_variable = { bureaucracy_gain = 100 }

	}
	else_if = {
		limit = { has_idea = bureau_04 }
		subtract_from_variable = { var = expense_gain value = bureaucracy_gain }
		add_to_variable = { var = treasury_rate value = bureaucracy_gain }
		set_variable = { var = bureaucracy_gain value = size_modifier }
		multiply_variable = { bureaucracy_gain = 100 }
		multiply_variable = { var = bureaucracy_gain value = 0.04 }
		round_variable = bureaucracy_gain
		divide_variable = { bureaucracy_gain = 100 }

	}
	else_if = {
		limit = { has_idea = bureau_05 }
		subtract_from_variable = { var = expense_gain value = bureaucracy_gain }
		add_to_variable = { var = treasury_rate value = bureaucracy_gain }
		set_variable = { var = bureaucracy_gain value = size_modifier }
		multiply_variable = { bureaucracy_gain = 100 }
		multiply_variable = { var = bureaucracy_gain value = 0.05 }
		round_variable = bureaucracy_gain
		divide_variable = { bureaucracy_gain = 100 }
	}
	if = {
		limit = { has_idea = ITA_inefficient_administration_4 } ##50% increase in cost for bureaucracy
		multiply_variable = { var = array_bureaucracy_spend^bureau_index value = 1.5 }
	}
	else_if = {
		limit = { has_idea = ITA_inefficient_administration_3 } ##37,5% increase in cost for bureaucracy
		multiply_variable = { var = array_bureaucracy_spend^bureau_index value = 1.375 }
	}
	else_if = {
		limit = { has_idea = ITA_inefficient_administration_2 } ##25% increase in cost for bureaucracy
		multiply_variable = { var = array_bureaucracy_spend^bureau_index value = 1.25 }
	}
	else_if = {
		limit = { has_idea = ITA_inefficient_administration_1 } ##12,5% increase in cost for bureaucracy
		multiply_variable = { var = array_bureaucracy_spend^bureau_index value = 1.125 }
	}
	add_to_variable = { var = expense_gain value = bureaucracy_gain }
	subtract_from_variable = { var = treasury_rate value = bureaucracy_gain }

}

update_police_rate_ai = {
	if = {
		limit = { has_idea = police_01 }
		subtract_from_variable = { var = expense_gain value = security_gain }
		add_to_variable = { var = treasury_rate value = security_gain }
		set_variable = { var = security_gain value = size_modifier }
		multiply_variable = { security_gain = 100 }
		multiply_variable = { var = security_gain value = 0.005 }
		round_variable = security_gain
		divide_variable = { security_gain = 100 }
	}
	else_if = {
		limit = { has_idea = police_02 }
		subtract_from_variable = { var = expense_gain value = security_gain }
		add_to_variable = { var = treasury_rate value = security_gain }
		set_variable = { var = security_gain value = size_modifier }
		multiply_variable = { security_gain = 100 }
		multiply_variable = { var = security_gain value = 0.01 }
		round_variable = security_gain
		divide_variable = { security_gain = 100 }

	}
	else_if = {
		limit = { has_idea = police_03 }
		subtract_from_variable = { var = expense_gain value = security_gain }
		add_to_variable = { var = treasury_rate value = security_gain }
		set_variable = { var = security_gain value = size_modifier }
		multiply_variable = { security_gain = 100 }
		multiply_variable = { var = security_gain value = 0.02 }
		round_variable = security_gain
		divide_variable = { security_gain = 100 }

	}
	else_if = {
		limit = { has_idea = police_04 }
		subtract_from_variable = { var = expense_gain value = security_gain }
		add_to_variable = { var = treasury_rate value = security_gain }
		set_variable = { var = security_gain value = size_modifier }
		multiply_variable = { security_gain = 100 }
		multiply_variable = { var = security_gain value = 0.03 }
		round_variable = security_gain
		divide_variable = { security_gain = 100 }

	}
	else_if = {
		limit = { has_idea = police_05 }
		subtract_from_variable = { var = expense_gain value = security_gain }
		add_to_variable = { var = treasury_rate value = security_gain }
		set_variable = { var = security_gain value = size_modifier }
		multiply_variable = { security_gain = 100 }
		multiply_variable = { var = security_gain value = 0.04 }
		round_variable = security_gain
		divide_variable = { security_gain = 100 }
	}
	if = {
		limit = { has_tech = DNA_fingerprinting } ##5% reduction in cost for security
		multiply_variable = { var = security_gain value = 0.95 }
	}
	if = {
		limit = { has_idea = ITA_legalized_light_drugs } ##10% reduction in cost for security
		multiply_variable = { var = security_gain value = 0.90 }
	}
	if = {
		limit = { has_idea = ITA_legalized_all_drugs } ##15% reduction in cost for security
		multiply_variable = { var = security_gain value = 0.85 }
	}
	add_to_variable = { var = expense_gain value = security_gain }
	subtract_from_variable = { var = treasury_rate value = security_gain }
}

update_education_rate_ai = {
	if = {
		limit = { has_idea = edu_01 }
		subtract_from_variable = { var = expense_gain value = education_gain }
		add_to_variable = { var = treasury_rate value = education_gain }
		set_variable = { var = education_gain value = size_modifier }
		multiply_variable = { education_gain = 100 }
		multiply_variable = { var = education_gain value = 0.02 }
		if = { limit = { has_idea = gdp_1 }
			multiply_variable = { var = education_gain value = 2.25 }
		}
		else_if = {
			limit = { has_idea = gdp_2 }
			multiply_variable = { var = education_gain value = 2 }
		}
		else_if = {
			limit = { has_idea = gdp_3 }
			multiply_variable = { var = education_gain value = 1.75 }
		}
		else_if = {
			limit = { has_idea = gdp_4 }
			multiply_variable = { var = education_gain value = 1.5 }
		}
		else_if = {
			limit = { has_idea = gdp_5 }
			multiply_variable = { var = education_gain value = 1.25 }
		}

		###Education Cost Reductions: == Better internet cheaper schooling cuz greater access to educational resources
		if = {
			limit = {
				has_tech = internet1
			}
			multiply_variable = { var = education_gain value = 0.97 } #3% education cost
		}
		if = {
			limit = {
				has_tech = internet2
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		if = {
			limit = {
				has_tech = internet3
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet4
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		if = {
			limit = {
				has_tech = internet5
			}
			multiply_variable = { var = education_gain value = 0.97 }
		}
		if = {
			limit = {
				has_tech = internet6
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		round_variable = education_gain
		divide_variable = { education_gain = 100 }
		add_to_variable = { var = expense_gain value = education_gain }
		subtract_from_variable = { var = treasury_rate value = education_gain }
	}
	else_if = {
		limit = { has_idea = edu_02 }
		subtract_from_variable = { var = expense_gain value = education_gain }
		add_to_variable = { var = treasury_rate value = education_gain }
		set_variable = { var = education_gain value = size_modifier }
		multiply_variable = { education_gain = 100 }
		multiply_variable = { var = education_gain value = 0.04 }
		if = { limit = { has_idea = gdp_1 }
			multiply_variable = { var = education_gain value = 2.25 }
		}
		else_if = {
			limit = { has_idea = gdp_2 }
			multiply_variable = { var = education_gain value = 2 }
		}
		else_if = {
			limit = { has_idea = gdp_3 }
			multiply_variable = { var = education_gain value = 1.75 }
		}
		else_if = {
			limit = { has_idea = gdp_4 }
			multiply_variable = { var = education_gain value = 1.5 }
		}
		else_if = {
			limit = { has_idea = gdp_5 }
			multiply_variable = { var = education_gain value = 1.25 }
		}
		###Education Cost Reductions: == Better internet cheaper schooling cuz greater access to educational resources
		if = {
			limit = {
				has_tech = internet1
			}
			multiply_variable = { var = education_gain value = 0.97 } #3% education cost
		}
		if = {
			limit = {
				has_tech = internet2
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		if = {
			limit = {
				has_tech = internet3
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet4
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		if = {
			limit = {
				has_tech = internet5
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet6
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		round_variable = education_gain
		divide_variable = { education_gain = 100 }
		add_to_variable = { var = expense_gain value = education_gain }
		subtract_from_variable = { var = treasury_rate value = education_gain }

	}
	else_if = {
		limit = { has_idea = edu_03 }
		subtract_from_variable = { var = expense_gain value = education_gain }
		add_to_variable = { var = treasury_rate value = education_gain }
		set_variable = { var = education_gain value = size_modifier }
		multiply_variable = { education_gain = 100 }
		multiply_variable = { var = education_gain value = 0.06 }
		if = { limit = { has_idea = gdp_1 }
			multiply_variable = { var = education_gain value = 2.25 }
		}
		else_if = {
			limit = { has_idea = gdp_2 }
			multiply_variable = { var = education_gain value = 2 }
		}
		else_if = {
			limit = { has_idea = gdp_3 }
			multiply_variable = { var = education_gain value = 1.75 }
		}
		else_if = {
			limit = { has_idea = gdp_4 }
			multiply_variable = { var = education_gain value = 1.5 }
		}
		else_if = {
			limit = { has_idea = gdp_5 }
			multiply_variable = { var = education_gain value = 1.25 }
		}
		###Education Cost Reductions: == Better internet cheaper schooling cuz greater access to educational resources
		if = {
			limit = {
				has_tech = internet1
			}
			multiply_variable = { var = education_gain value = 0.97 } #3% education cost
		}
		if = {
			limit = {
				has_tech = internet2
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		if = {
			limit = {
				has_tech = internet3
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet4
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		if = {
			limit = {
				has_tech = internet5
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet6
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		round_variable = education_gain
		divide_variable = { education_gain = 100 }
		add_to_variable = { var = expense_gain value = education_gain }
		subtract_from_variable = { var = treasury_rate value = education_gain }

	}
	else_if = {
		limit = { has_idea = edu_04 }
		subtract_from_variable = { var = expense_gain value = education_gain }
		add_to_variable = { var = treasury_rate value = education_gain }
		set_variable = { var = education_gain value = size_modifier }
		multiply_variable = { education_gain = 100 }
		multiply_variable = { var = education_gain value = 0.08 }
		if = { limit = { has_idea = gdp_1 }
			multiply_variable = { var = education_gain value = 2.25 }
		}
		else_if = {
			limit = { has_idea = gdp_2 }
			multiply_variable = { var = education_gain value = 2 }
		}
		else_if = {
			limit = { has_idea = gdp_3 }
			multiply_variable = { var = education_gain value = 1.75 }
		}
		else_if = {
			limit = { has_idea = gdp_4 }
			multiply_variable = { var = education_gain value = 1.5 }
		}
		else_if = {
			limit = { has_idea = gdp_5 }
			multiply_variable = { var = education_gain value = 1.25 }
		}
		###Education Cost Reductions: == Better internet cheaper schooling cuz greater access to educational resources
		if = {
			limit = {
				has_tech = internet1
			}
			multiply_variable = { var = education_gain value = 0.97 } #3% education cost
		}
		if = {
			limit = {
				has_tech = internet2
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		if = {
			limit = {
				has_tech = internet3
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet4
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		if = {
			limit = {
				has_tech = internet5
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet6
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		round_variable = education_gain
		divide_variable = { education_gain = 100 }
		add_to_variable = { var = expense_gain value = education_gain }
		subtract_from_variable = { var = treasury_rate value = education_gain }

	}
	else_if = {
		limit = { has_idea = edu_05 }
		subtract_from_variable = { var = expense_gain value = education_gain }
		add_to_variable = { var = treasury_rate value = education_gain }
		set_variable = { var = education_gain value = size_modifier }
		multiply_variable = { education_gain = 100 }
		multiply_variable = { var = education_gain value = 0.10 }
		if = { limit = { has_idea = gdp_1 }
			multiply_variable = { var = education_gain value = 2.25 }
		}
		else_if = {
			limit = { has_idea = gdp_2 }
			multiply_variable = { var = education_gain value = 2 }
		}
		else_if = {
			limit = { has_idea = gdp_3 }
			multiply_variable = { var = education_gain value = 1.75 }
		}
		else_if = {
			limit = { has_idea = gdp_4 }
			multiply_variable = { var = education_gain value = 1.5 }
		}
		else_if = {
			limit = { has_idea = gdp_5 }
			multiply_variable = { var = education_gain value = 1.25 }
		}
		###Education Cost Reductions: == Better internet cheaper schooling cuz greater access to educational resources
		if = {
			limit = {
				has_tech = internet1
			}
			multiply_variable = { var = education_gain value = 0.97 } #3% education cost
		}
		if = {
			limit = {
				has_tech = internet2
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		if = {
			limit = {
				has_tech = internet3
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet4
			}
			multiply_variable = { var = education_gain value = 0.95}
		}

		if = {
			limit = {
				has_tech = internet5
			}
			multiply_variable = { var = education_gain value = 0.97}
		}
		if = {
			limit = {
				has_tech = internet6
			}
			multiply_variable = { var = education_gain value = 0.95}
		}
		round_variable = education_gain
		divide_variable = { education_gain = 100 }
		add_to_variable = { var = expense_gain value = education_gain }
		subtract_from_variable = { var = treasury_rate value = education_gain }
	}
}

update_health_rate_ai = {
	if = {
		limit = { has_idea = health_01 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.01 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }
	}
	else_if = {
		limit = { has_idea = health_02 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.02 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }

	}
	else_if = {
		limit = { has_idea = health_03 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.04 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }

	}
	else_if = {
		limit = { has_idea = health_04 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.06 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }

	}
	else_if = {
		limit = { has_idea = health_05 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.08 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }
	}
	else_if = {
		limit = { has_idea = health_06 }
		subtract_from_variable = { var = expense_gain value = health_gain }
		add_to_variable = { var = treasury_rate value = health_gain }
		set_variable = { var = health_gain value = size_modifier }
		multiply_variable = { health_gain = 100 }
		multiply_variable = { var = health_gain value = 0.1 }
		round_variable = health_gain
		divide_variable = { health_gain = 100 }
	}
	if = {
		limit = {
			has_tech = nanocellulose_in_medicine
		}
		multiply_variable = { var = health_gain value = 0.90 }
	}
	if = {
		limit = {
			has_tech = medical_nanorobots
		}
		multiply_variable = { var = health_gain value = 0.95 }
	}
	add_to_variable = { var = expense_gain value = health_gain }
	subtract_from_variable = { var = treasury_rate value = health_gain }

}

update_social_rate_ai = {
	if = {
		limit = { has_idea = social_01 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.02 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }
	}
	else_if = {
		limit = { has_idea = social_02 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.045 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }

	}
	else_if = {
		limit = { has_idea = social_03 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.07 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }

	}
	else_if = {
		limit = { has_idea = social_04 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.095 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }

	}
	else_if = {
		limit = { has_idea = social_05 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.13 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }
	}
	else_if = {
		limit = { has_idea = social_06 }
		subtract_from_variable = { var = expense_gain value = welfare_gain }
		add_to_variable = { var = treasury_rate value = welfare_gain }
		set_variable = { var = welfare_gain value = size_modifier }
		multiply_variable = { welfare_gain = 100 }
		multiply_variable = { var = welfare_gain value = 0.165 }
		round_variable = welfare_gain
		divide_variable = { welfare_gain = 100 }
		add_to_variable = { var = expense_gain value = welfare_gain }
		subtract_from_variable = { var = treasury_rate value = welfare_gain }
	}

}

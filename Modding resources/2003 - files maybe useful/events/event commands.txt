TRIGGERS:
and
or
not
event = x  #event has happened.
random = x 			( % )
intel_diff = x 		( >= x, x = our intel - enemy intel )
dissent = x ( >= x )
leader = x #leader exists
government = [communist/fascist/democratic]
ideology = [nazi/fascist/paternal_autocrat/social_conservative/market_liberal/social_liberal/social_democrat/left_wing_radical/leninist/stalinist]
atwar = [yes/no/country tag] # Country tag added to see if other countries are at war
minister = [minister id] # Is minister alive?
major = [yes/no]	# True if the country is ENG, FRA, GER, JAP, USA, ITA or SOV
ispuppet = [Country Tag]
puppet = { country = [tag 1] country = [tag 2] } # True if tag 1 is puppet of tag 2
headofgovernment = [minister id]
headofstate = [minister id]
ai = [yes/no/country tag] # Is the country AI controlled?
technology = x 			 		# True if a certain tech is known.
technology = { country = TAG value = x }	# True if a certain tech is known by the given country.
is_tech_active = [tech id]		# True if the tech is researchable.
manpower = x (>=x)#true if a nation have at least that amount of manpower.
flag = [name]  				#true if the global flag exists and is set to true.
local_flag = [name]			#true if the local flag exists and is set to true.
owned = { province = a data = tag }	# checks if province is owned by country x (if data == -1 then its for country receievin event)
control = { province = a data = tag }	# checks if province is controlled by country x (if data == -1 then its for country receievin event)
exists = country #if country exists.
alliance = { country =a  country = b }	# checks if a and b is in the same military alliance
access = { country = [tag1] country = [tag2] } # checks if tag1 is granting military access to tag2
non_aggression = { country = [tag1] country = [tag2] } # checks if tag & tag have a non_agg_pact active
trade = { country = [tag1] country = [tag2] } # checks if tag1 and tag2 have an active trade agreement
guarantee = { country = [tag1] country = [tag2] } # checks if tag1 is guaranteeing the independence of tag2
war = { country =a  country = b }	# checks if a and b is at war
year = x (>=x)
energy = x
oil = x
rare_materials = x
metal = x
supplies = x
lost_VP = { country = [tag] value = X }			# X% or more of _owned_ VPs in enemy hands
lost_national = { country = [tag] value = X }	# X% or more of national provs in enemy hands
lost_IC = { country = [tag] value = X }			# X% or more of national IC in enemy hands
domestic = { type = field value = x }
division_exists = { type = [id type] id = [id id] }
division_in_province = { id = { type = [id type] id = [id id] } province = X }
axis = X # axis holds at least X vp
allies = X # allies holds at least X vp
comintern = X # comintern holds at least X vp
vp = X											# Currently held Key Points + extra VP >= X
range = { min = x max = x }	# true if vp is within (>= && <= the range)
belligerence = { country = [tag] value = X }	# No country field means "this country". True if Belligerence >= X.
[div type] = X									# X or more of that type of division.
[div type] = { country = TAG value = X }					# country TAG has X or more of that type of division
under_attack = [tag]							# Country is under attack
attack = [tag]							# Country [tag] has attacked this country.
difficulty = X # Difficulty at level X or harder. [0-4],  0 is Very Easy.
garrison = { country = [tag] province = [province] type = [air/land/naval] size = [number of divisions] area = [yes/no] } # No type field means "all division types". "area = yes" means "count divisions in all provinces in this controlled Area". Do not use "area = yes" for sea provinces!
land_percentage = { country = [tag] value = [value] } #true if we have at least value times the land forces of country
naval_percentage = { country = [tag] value = [value] } #true if we have at least value times the naval forces of country
air_percentage = { country = [tag] value = [value] } #true if we have at least value times the air forces of country
country = [tag] #true if this is country
can_change_policy = { type = [policy] value = [delta] } #true if the policy can be changed by delta
relation = { which = [tag] value = [value] } #true if relations to country which is at least value
province_revoltrisk = { province = [id] value = [value] } # true if revolt risk in province is at least value
incabinet = [id] #true if minister id is in the ruling cabinet
army = [divisions] #true if we have at least this many land divisions
day = [number] #true if the day (of the month) is at least this
nuke = [number] #true if we have at least this many nukes ready

COMMANDS:
type = endgame value = [type]   # (0=quit, 1=eliminated, 2=draw, 3=axis victory, 4=axis total vic, 5=allies-victory, 6=alliestotal, 7=commie, 8=commietotal)
type = 	extra_tc value = X
type =	alliance which = [tag] (-1 for random)
type =	inherit which = [tag] (-1 for random)
type =	country which = [tag] 					# Change the tag of the country.
type =	trigger which = [event id]				# Immediately trigger another event
type =	capital which = [prov id]				# Change capital
type =	addcore   which = [prov id]
type =	removecore which = [prov id]
type =	secedeprovince which = [tag]  value = [prov id]
type =	control which = [tag]  value = [prov id]
type =	sleepevent which = [event id]
type = setflag which = [keyname]				# Global flag, visible for all countries
type = clrflag which = [keyname]				# Global flag, visible for all countries
type = local_setflag which = [keyname]			# Local flag, visible only for this country
type = local_clrflag which = [keyname]			# Local flag, visible only for this country
type = steal_tech which = [country tag/-1 for random non-ally] 											# Steals a random tech. Results in a blueprint.
type = gain_tech which = [tech id/-1 for random tech/-2 for a random tech currently being researched]	# Results in a blueprint, not a technology.
type = vp value = X 	# Offmap Victory Points
type = research_sabotaged
type = regime_falls
type = belligerence which = [country] value = X 	# No which field means "this country".
type = relation which = [country] value = X				# Add X to the relation
type = set_relation which = [country] value = X		# Set relation to X
type = civil_war 									# Civil war with any possible revolter that has shared national provinces
type = civil_war which = [rebel country tag]		# NOTE: This MUST be a tag which has regular_id set to this country, or vice versa.
type = set_leader_skill which = [leader id/-1] value = [new value]
type = dissent value = [value to add]
type = resource which =[resource type] value = X # % of theoretical maximum daily production added to national resource pool.
type = supplies value = [value to add] where = [prov] # If no "where" field is used, it goes to the national pool.
type = oilpool value = [value to add] where = [prov] # If no "where" field is used, it goes to the national pool.
type = metalpool value = [value to add]
type = energypool value = [value to add]
type = rarematerialspool value = [value to add]
type = money value = [value to add]
type = province_keypoints which = [province id/-1] value = [keypoints to add]
type = peace which = [country tag] value = [0/1] # 1 means separate peace - i.e. pull out of alliance. 0 is full peace, for the whole alliance.
type = war which = [country tag]
type = manpowerpool value = [value to add] # manpower
type = relative_manpower value = [%] 	# Add a percentage of [yearly national manpower income * 50] to the pool
type = allow_dig_in						# Troops may now dig in
type = make_puppet which = [country tag]
type = coup_nation which = [country tag]
type = access which = [country tag] # Grant military access _to_ a country
type = sleepteam which = [tech team id]		# Makes this team unavailable (sets "start year" to 2001. Will complete current project though.)
type = waketeam which = [tech team id]		# Makes this team available (sets "start year" to previous year.)
type = sleepminister which = [minister id/-1]
type = sleepleader which = [leader id/-1]
type = switch_allegiance which = [unit id type/-1] value = [unit id id]	where = [country tag]	# -1 is a random land unit -2 random naval unit -3 random air unit (value will be ignored).
type = delete_unit which = [unit id type/-1/-2/-3] value = [unit id id] # -1 is a random land unit -2 random naval unit -3 random air unit (value will be ignored).
type = independence which = [country tag] value = [0/1] when = [0/1] # value 0 - units in the area change allegiance, 1 - they don't.  , when 0=normal, 1= force them to be democratic
type = ai which = [file name]	# Switches the ai file of a country.
type = build_division which = [division type] value = [brigade type]
type = construct which = [building type] where = [prov id/-1/-4] value = [additional size]	# -1 is random valid province. -4 is random national province that already has a building of the same type of at least size 1.
type = add_corps which = [name] value = [land/air/naval] when = [leader ID] where = [province_ID]
type = activate_division which = [div id type] value = [div id id]  [where = province] when = [0/1] # Activates a dormant division. If "when = 0" the division will deploy to the force pool if the target province is enemy controlled. If a preceding add_corps command has been used, the division will go to that unit, ignoring the 'when' and 'where' directives.
type = add_division which = [name] value = [div type] when = model where = [brigade type] # Brigade model is always the latest. If a preceding add_corps command has been used, the division will go to that unit, otherwise to the force pool.
type = remove_division which = [div id type/-1] value = [div id id]	# Remove a division from the scenario. -1 is a random division (the value field will be ignored).
type = remove_division which = "Exact Name" value = [country tag]	# Remove a named division from the scenario (for a certain country.)
type = damage_division which = [div id type/-1] value = [div id id] where = X	# where = Percent of Max Strength. Negative value is strength gain.
type = disorg_division which = [div id type/-1] value = [div id id] where = X	# where = Percent of Max Strength. Negative value is org gain.
type = end_access which = [country tag]	# Revoke military access for nation
type = leave_alliance
type = end_non_aggression which = [country] where = [country] 					# end a non aggression pact between these countries
type = non_aggression which = [country] where = [country]						# create a non aggression pact between these countries
type = end_trades which = [country] where = [country] 							# end all trade agreements between these countries
type = end_guarantee which = [country] where = [country] 						# country 1 no longer guarantees independence of country 2
type = guarantee which = [country] where = [country] 							# country 1 now guarantees independence of country 2
type = allow_convoy_escorts
type = transport_pool which = [country] value = X (adds/removes transports from pool)
type = escort_pool which = [country] value = X (adds/removes escorts from pool)
type = peacetime_ic_mod value = X(%) # Additive, and ONLY applied when a country is at peace. Default base value is 100%.
type = end_puppet	# End puppet status
type = end_mastery which = [country tag]	# End puppet mastery over another nation
type = convoy which = [startprov] value = [endprov] when = [resource_bits] 1=oil, 2=metal, 4=energy, 8=rare_materials, 16=supplies
type = domestic	which = [democratic/political_left/free_market/freedom/professional_army/defense_lobby/interventionism] value = X		# Change a domestic policy by X
type = set_domestic	which = [democratic/political_left/free_market/freedom/professional_army/defense_lobby/interventionism] value = X	# Set a domestic policy to X
type = deactivate which = [tech id] 		# Make this technology permanently unavailable for the country.
type = tc_mod value = X(%)								# Modify National Transport Capacity. Positive value = good.
type = tc_occupied_mod value = X(%)						# Modify Occupied Province Transport Capacity Drain. Positive value = good.
type = attrition_mod value = X(%)						# Modify the Attrition Penalty. Positive value = good.
type = supply_dist_mod value = X(%)						# Modify the Supply Distance Penalty. Positive value = good.
type = repair_mod value = X(%)							# Modify Reinforcement Efficiency Positive value = good.
type = research_mod value = X(%)						# Modify research efficiency. Positive value = good.
type = scrap_model which = [div type] value = [model]	# Make a model obsolete (no longer possible to build).
type = allow_building which = [building type]			# Allow the construction of a certain building type.
type = building_prod_mod which = [building type] value = X%		# Bonus to production time. Additive. NOTE: Positive value = reduced time.
type = convoy_prod_mod which = [escorts/transports] value = X%	# Bonus to production time. Additive. NOTE: Positive value = reduced time.
type = radar_eff value = X(%)							# Modify the efficiency of radar stations (bonus to friendly aircraft in the province.) Positive value = good.
type = info_may_cause which = [tech id]					# No effect, but informs player that this tech/event can open up new avenues of research.
type = gas_attack            							# Enable gas attacks
type = gas_protection        							# Enable gas protection
type = activate which = [tech id] 						# Will be used in Random events
type = task_efficiency which = [task type] value = X 	#Additive: 0.1 - 1.0
type = add_prov_resource which = [prov id/-1/-4] value = X where = [energy/metal/oil/rare_materials]  # -1 is random province, -4 is province with nuclear reactor.
type = max_positioning which = [div type] value = X 	#sets the value 0.1-1.0
type = min_positioning which = [div type] value = X 	#sets the value 0.1-1.0
type = max_reactor_size value = X			# Set the max size of nuclear reactor buildings.
type = abomb_production 					# Allow A-Bombs
type = double_nuke_prod						# Double A-Bomb production rate.
type = songs
type = lock_division which = [id type] value = [id id] # Lock this division (it may no longer move.)
type = unlock_division which = [id type] value = [id id] # Unlock this division (it may now move.)
type = free_ic/free_oil/free_supplies/free_money/free_metal/free_energy/free_rare_material/free_transport/free_convoy/free_manpower	#change offmap daily prod
type = change_policy which = [democratic/political_left/free_market/freedom/professional_army/defense_lobby/interventionism] value = X		# Change a domestic policy by X if it could normally be changed and also sets the cooldown period for changing policy
# AI event commands (no effect for human controlled nations)

type = ai_prepare_war	which = [tag]		# Tells the AI to move troops to the border with a country.

# Weather event commands
type = start_pattern which = [id type] value = [id id] where = [prov id] when = [raining/snowing/storm/blizzard]	# Start a new weather pattern centered on a given province
type = add_to_pattern which = [id type] value = [id id] where = [prov id] when = [raining/snowing/storm/blizzard]	# Add a province to an existing weather pattern.
type = end_pattern which = [id type] value = [id id] # This weather pattern will begin to dissipate
type = set_ground which = [prov id] value = [clear/muddy/winter]

# Commands to affect the chance of "combat events" occurring. Additive (%).
type = counterattack value = x
type = assault value = x
type = encirclement value = x
type = ambush value = x
type = delay value = x
type = tactical_withdrawal value = x
type = breakthrough value = x

# HQ effects
type = hq_supply_eff value = X(%)			# Additive
type = sce_frequency value = X				# Combat event chance multiplier. Default: 1.0

# Enable a certain type of mission
type = enable_task which = [task type]

# New models updates the current template for a division or brigade type.
command = { type = [new_model] which = [division or brigade type] value = [The model number] }

# Make a division or brigade type available for construction.
command = { type = activate_unit_type which = [division or brigade type] }

# Make a division or brigade type unavailable for construction.
command = { type = deactivate_unit_type which = [division or brigade type] }

# ACTIVATE NUCLEAR CAPABILITY OF MISSILES
command = { type = nuclear_carrier which = [flying_bomb/flying_rocket] }

# ACTIVATE MISSILE CAPABILITY OF SUBMARINES
command = { type = missile_carrier which = [submarine] }

# UNIT VALUES
# [unit value] = soft_attack/hard_attack/ground_defense/air_attack/air_defense/build_cost/build_time/manpower/speed/max_organization/transport_weight/supply_consumption/fuel_consumption/speed_cap_art/speed_cap_eng/speed_cap_at/speed_cap_aa
#

# Land unit bonuses
command = { type = [unit value]
			which = [land/division type/brigade type]
			value = [additive value modifier]
}
# Air unit bonuses
command = { type = [air_attack/strategic_attack/tactical_attack/naval_attack/air_defense/build_cost/build_time/manpower/speed/surface_detection/air detection/transport_capacity/supply_consumption/fuel_consumption/range]
			which = [air/division type/brigade type]
			value = [additive value modifier]
}
# Naval unit bonuses
command = { type = [naval_attack/air_attack/shore_attack/naval_defense/air_defense/build_cost/build_time/manpower/speed/surface_detection/air detection/visibility/transport_capacity/supply_consumption/fuel_consumption/range]
			which = [naval/division type/brigade type]
			value = [additive value modifier]
}

# NOTE: For "build_time" and "build_cost", it is possible to add the field "where = relative".
# This means that the value is parsed as a relative modifier (%) instead of an absolute addition.
# When doing this, THE BASE VALUE OF MODEL 0 IS USED FOR _ALL_ MODELS. So "value = -10" means
# "-10% of the value of model 0".

#
# UNIT COMBAT MODIFIERS
#
# Terrain combat modifiers
command = { type = [desert_attack/desert_defense/mountain_attack/mountain_defense/hill_attack/hill_defense/forest_attack/forest_defense/swamp_attack/swamp_defense/urban_attack/urban_defense/river_attack/shore_attack/paradrop_attack]
			which = [division type/brigade type]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Terrain movement modifiers
command = { type = [desert_move/mountain_move/hill_move/forest_move/swamp_move/urban_move/river_crossing]
			which = [division type/brigade type]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather land-combat modifiers
command = { type = [frozen_attack/frozen_defense/snow_attack/snow_defense/blizzard_attack/blizzard_defense/rain_attack/rain_defense/storm_attack/storm_defense/muddy_attack/muddy_defense]
			which = [division type/brigade type]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather land-movement modifiers
command = { type = [frozen_move/snow_move/blizzard_move/rain_move/storm_move/muddy_move]
			which = [division type/brigade type]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather air-combat modifiers
command = { type = [snow_attack/rain_attack]
			which = [interceptor/escort/multi_role/strategic_bomber/tactical_bomber/torpedo_plane]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather air-movement modifiers
command = { type = [snow_move/blizzard_move/rain_move/storm_move]
			which = [interceptor/escort/multi_role/strategic_bomber/tactical_bomber/torpedo_plane/transport_plane]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather naval-combat modifiers
command = { type = [snow_attack/rain_attack]
			which = [battleship/cruiser/destroyer/carrier/submarine/transports]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Weather naval-movement modifiers
command = { type = [snow_move/blizzard_move/rain_move/storm_move]
			which = [battleship/cruiser/destroyer/carrier/submarine/transports]
			value = [X] # Modifier in percent (+10 = 10%)
}
# Night modifiers
command = { type = night_move/night_attack/night_defense
			which = [division type/brigade type]
			value = [X] # Modifier in percent (+10 = 10%)
}

#
# SPECIAL UNIT BONUSES
#
# Minisubs (BBs and CVs attack ships in harbors with this bonus added to their sea attack value)
command = { type = minisub_bonus
			value = [X] # NOT INCREMENTAL. This is the new value.
}

#
# GLOBAL MODIFIERS
#
# Surprise modifiers
command = { type = surprise
			which = [naval/land/air]
			value = [X] # Modifier in percents
}
# Intelligence Operation modifiers
command = { type = intelligence
			which = [us/them]
			value = [X] # Modifier in percents
}
# Army Detection modifiers
command = { type = army_detection
			which = [us/them]
			value = [X] # Modifier in percents
}
# Provincial AA batteries
command = { type = AA_batteries
			value = [X] # Modifier in percents
}

#
# INDUSTRIAL MODIFIERS
#

# Industrial multipliers
# E.g. energy_to_oil 4 means four energy are needed to produce one oil. 0.5 means 1 energy produces 2 oil.
command = { type = industrial_multiplier
			which = [energy_to_oil/oil_to_rare_materials]
			value = [X]
}
# Industrial modifiers (additive)
# 'total' is the conversion factor from every built industry point into Industrial Capacity.
# 'supplies' is a modifier for the amount of supplies produced by every reserved IC unit.
command = { type = industrial_modifer
			which = [total/supplies]
			value = [X] # Percents
}

Building Types:
[ic, coastal_fort, land_fort, flak, infrastructure, air_base, naval_base, radar_station, nuclear_reactor, rocket_test]

Task Types:
[attack, rebase, strat_redeploy, support_attack, support_defense, reserves, anti_partisan_duty, air_superiority, ground_attack, runway_cratering, installation_strike, interdiction, naval_strike, port_strike, logistical_strike, strategic_bombardment, air_supply, airborne_assault, convoy_raiding, asw, naval_interdiction, shore_bombardment, amphibous_assault, sea_transport, naval_combat_patrol]
Resource Types:
[oil, metal, energy, rare_materials, supplies, money]
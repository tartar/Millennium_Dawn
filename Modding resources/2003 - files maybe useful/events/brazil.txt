#########################################################
#							#
# Events for Brazil (BRA)                               #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#11001 - 11300# Main events
#11301 - 11450# Political events
#11451 - 11500# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### Political events
#############################################

#############################################
###### Election events
#############################################

#############################################
###### Brazilian presidential election 2006
#############################################
event = {
         id = 11451
         random = no
         country = BRA
         trigger = {
		   OR = {
			ideology = social_conservative
                   	ideology = market_liberal
			ideology = social_liberal
			ideology = social_democrat
			ideology = left_wing_radical
		   }
		   atwar = no

         }
 
         name = "EVT_11451_NAME"
         desc = "EVT_11451_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 29 month = october year = 2006 }
 
           action_a = {
                  name = "Luiz In�cio Lula da Silva - Workers' Party"
                  ai_chance = 80
		  command = { type = headofstate which = 1101 } #Lula
		  command = { type = headofgovernment which = 1102 } #Lula
		  command = { type = foreignminister which = 1104 } #Amorim
		  command = { type = armamentminister which = 1130 } #Pires
		  command = { type = ministerofsecurity which = 1132 } #Genro
		  command = { type = ministerofintelligence which = 1133 } #Lacerda
		  command = { type = set_domestic which = political_left value = 9 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 6 }
           }
           action_b = {
                  name = "Geraldo Alckmin - Social Democratic Party"
                  ai_chance = 20
		  command = { type = headofstate which = 1127 } #Alckmin
		  command = { type = headofgovernment which = 1128 } #Alckmin
		  command = { type = foreignminister which = 1140 } #Guerra
		  command = { type = armamentminister which = 1123 } #Goldman
		  command = { type = ministerofsecurity which = 1147 } #Bivar
		  command = { type = ministerofintelligence which = 1133 } #Lacerda
		  command = { type = set_domestic which = political_left value = 7 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 7 }
		  
           }

}
#############################################
###### Brazilian presidential election 2010
#############################################
event = {
         id = 11452
         random = no
         country = BRA
         trigger = {
		   OR = {
			ideology = social_conservative
                   	ideology = market_liberal
			ideology = social_liberal
			ideology = social_democrat
			ideology = left_wing_radical
		   }
		   atwar = no

         }
 
         name = "EVT_11452_NAME"
         desc = "EVT_11452_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 3 month = october year = 2010 }
 
           action_a = {
                  name = "Dilma Rousseff - Workers' Party"
                  ai_chance = 50
		  command = { type = headofstate which = 1153 } #Rousseff
		  command = { type = headofgovernment which = 1154 } #Rousseff
		  command = { type = foreignminister which = 1104 } #Amorim
		  command = { type = armamentminister which = 1129 } #Jobim
		  command = { type = ministerofsecurity which = 1149 } #Frossard
		  command = { type = ministerofintelligence which = 1134 } #Trezza
		  command = { type = set_domestic which = political_left value = 9 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 6 }
           }
           action_b = {
                  name = "Jose S�rra - Social Democratic Party"
                  ai_chance = 30
		  command = { type = headofstate which = 1121 } #S�rra
		  command = { type = headofgovernment which = 1122 } #S�rra
		  command = { type = foreignminister which = 1142 } #Maia
		  command = { type = armamentminister which = 1139 } #Neves
		  command = { type = ministerofsecurity which = 1151 } #Mendes
		  command = { type = ministerofintelligence which = 1134 } #Trezza
		  command = { type = set_domestic which = political_left value = 7 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 7 }
		  
           }
	   action_a = {
                  name = "Ciro Gomes - Socialist Party"
                  ai_chance = 20
		  command = { type = headofstate which = 1155 } #Gomes
		  command = { type = headofgovernment which = 1156 } #Gomes
		  command = { type = foreignminister which = 1104 } #Amorim
		  command = { type = armamentminister which = 1129 } #Jobim
		  command = { type = ministerofsecurity which = 1149 } #Frossard
		  command = { type = ministerofintelligence which = 1134 } #Trezza
		  command = { type = set_domestic which = political_left value = 9 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 6 }
           }

}
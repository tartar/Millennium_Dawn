
##############################
# Country definition for MAL #
##############################

country =
{ tag                 = MAL
  # Resource Reserves
  energy              = 1000
  metal               = 1000
  rare_materials      = 500
  oil                 = 500
  supplies            = 500
  money               = 10
  manpower            = 35
  transports          = 0
  escorts             = 0
  capital             = 986
  diplomacy           = { }
  nationalprovinces   = { 975 976 977 978 979 980 985 986 994 }
  ownedprovinces      = { 975 976 977 978 979 980 985 986 994 }
  controlledprovinces = { 975 976 977 978 979 980 985 986 994 }
  techapps            = {
                                        #Industry:
                                        5010
                                        5020
                                        5030
                                        5040
                                        5050
                                        5070
                                        5090
                                        #Army Equip:
                                        2400
                                        2200
                                        2500
                                        2600
                                        2800
                                        #Army Org:
                                        1300
                                        1900
                                        1260
                                        1980
                                        #Army Doc:
                                        6100
                                        6110
                                        6160
                                        6010
                                        6020
                                        6600
                                        6610
                                        6910
                                        #Secret Tech:
                                        7330
                        }
  policy =
  { date              = { year = 0 month = march day = 0 }
    democratic        = 8
    political_left    = 4
    free_market       = 6
    freedom           = 9
    professional_army = 10
    defense_lobby     = 3
    interventionism   = 4
  }
}

#Whole file dedicated to Trump b/c of the number of decisions

USA_the_trump_agenda = {
	USA_donald_hold_maga_rally = {
		icon = decision
		
		days_re_enable = 30
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_donald_hold_maga_rally"
			add_stability = 0.01
			add_0_5_Republican_popularity = yes
			add_0_5_Nat_Populism_popularity = yes
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_donald_travel_ban = {
		icon = decision
		
		cost = 50
		
		fire_only_once = yes
		
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_donald_travel_ban"
			add_stability = -0.01
			add_0_5_Republican_popularity = yes
			add_1_Nat_Populism_popularity = yes
			country_event = donald_trump.0
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_donald_supreme_court_justice = {
		icon = decision
		
		visible = {
			has_country_flag = empty_scotus_seat
		}
		
		cost = 25
		days_remove = 30
		
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_donald_supreme_court_justice"
			
		}
		modifier = {
			stability_factor = -0.03
			political_power_cost = 0.5
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_donald_supreme_court_justice"
			clr_country_flag = empty_scotus_seat
			set_country_flag = republican_scotus_justice_added
			if = {
				limit = {
					has_country_flag = republican_scotus_justice_added
				}
				set_country_flag = second_republican_scotus_justice_added
			}
			if = {
				limit = {
					has_country_flag = second_republican_scotus_justice_added
				}
				set_country_flag = third_republican_scotus_justice_added
			}
			add_political_power = 15
			add_1_Republican_popularity = yes
			add_1_Nat_Populism_popularity = yes
			custom_effect_tooltip = add_republican_scotus_justice_tt
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_fire_james_comey = {
		icon = decision
		
		fire_only_once = yes
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_fire_james_comey"
			add_stability = 0.02
			add_0_25_Republican_popularity = yes
			add_0_25_Nat_Populism_popularity = yes
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_pull_out_of_the_paris_climate_accords = {
		icon = decision
		
		fire_only_once = yes
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_pull_out_of_the_paris_climate_accords"
			add_stability = -0.01
			add_0_25_Republican_popularity = yes
		}
		modifier = {
			production_speed_buildings_factor = 0.02
		}
		days_remove = 365
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_finish_keystone_and_dakota_pipelines = {
		icon = decision
		
		fire_only_once = yes
		cost = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_finish_keystone_and_dakota_pipelines"
			add_stability = -0.02
			add_0_25_Republican_popularity = yes
			set_country_flag = Keystone_Pipeline_Finished
		}
		modifier = {
			production_speed_buildings_factor = 0.02
		}
		days_remove = 365
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_america_first_energy_plan = {
		icon = decision
		visible = {
			has_completed_focus = USA_donald_trump_the_trump_economy
		}
		
		fire_only_once = yes
		cost = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_america_first_energy_plan"
			add_0_25_Nat_Populism_popularity = yes
			787 = {
				add_resource = { type = oil amount = 2 }
			}
			800 = {
				add_resource = { type = oil amount = 2 }
			}
			771 = {
				add_resource = { type = oil amount = 1 }
			}
			set_country_flag = afep
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_open_public_lands_to_fracking = {
		icon = decision
		visible = {
			has_country_flag = afep
		}
		
		fire_only_once = yes
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_open_public_lands_to_fracking"
			add_0_25_Nat_Populism_popularity = yes
			787 = {
				add_resource = { type = oil amount = 2 }
			}
			800 = {
				add_resource = { type = oil amount = 4 }
			}
			788 = {
				add_resource = { type = oil amount = 1 }
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	#Deal with Immigration
	USA_end_catch_and_release_policy = {
		icon = decision
		visible = {
			has_completed_focus = USA_donald_trump_deal_with_immigration
			has_country_flag = increased_border_security
		}
		
		fire_only_once = yes
		cost = 15

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_end_catch_and_release_policy"
			add_stability = -0.02
			add_0_5_Nat_Populism_popularity = yes
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_increase_border_security = {
		icon = decision
		visible = {
			has_completed_focus = USA_donald_trump_deal_with_immigration
		}
		
		fire_only_once = yes
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_increase_border_security"
			add_stability = 0.02
			add_0_25_Nat_Populism_popularity = yes
			subtract_from_variable = { treasury = 0.5 }
			custom_effect_tooltip = 0.5_billion_expense_tt
			set_country_flag = increased_border_security
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	USA_stop_the_caravans = {
		icon = decision
		visible = {
			has_completed_focus = USA_donald_trump_deal_with_immigration
			has_country_flag = increased_border_security
		}
		
		fire_only_once = yes
		cost = 30

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_stop_the_caravans"
			add_stability = 0.03
			add_0_25_Nat_Populism_popularity = yes
			###############################################################################################
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}

USA_america_first = {
	USA_finish_off_ISIS = {
		icon = decision
		
		fire_only_once = yes
		cost = 50

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_finish_off_ISIS"
			country_event = donald_trump.500
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}

USA_asteroid_mining = {
	USA_asteroid_mining_mission = {
		icon = GFX_decision_asteroid_mining
		available = {
			always = no
		}
		activation = {
			has_country_flag = asteroid_mining
		}
		is_good = yes
		days_mission_timeout = 365
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout USA_asteroid_mining_mission"
			country_event = donald_trump.12
		}
		fire_only_once = no
		days_remove = 0
	}
	USA_pull_small_asteroid_into_orbit = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_country_flag = asteroid_mining
		}
		days_re_enable = 30
		cost = 10
		fixed_random_seed = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_pull_small_asteroid_into_orbit"
			subtract_from_variable = { treasury = 2 }
			custom_effect_tooltip = 2_billion_expense_tt
		}
		days_remove = 45
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_pull_small_asteroid_into_orbit"
			hidden_effect = {
				random_list = {
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 5 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 10 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 15 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 20 }
					}
				}
			}
			custom_effect_tooltip = asteroid_mining_small_asteroid_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
	USA_pull_moderate_asteroid_into_orbit = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_country_flag = asteroid_mining
		}
		days_re_enable = 30
		cost = 25
		fixed_random_seed = no
		days_remove = 90
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_pull_moderate_asteroid_into_orbit"
			subtract_from_variable = { treasury = 5 }
			custom_effect_tooltip = 5_billion_expense_tt
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_pull_moderate_asteroid_into_orbit"
			hidden_effect = {
				random_list = {
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 20 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 30 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 40 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 50 }
					}
				}
			}
			custom_effect_tooltip = asteroid_mining_moderate_asteroid_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
	USA_pull_large_asteroid_into_orbit = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_country_flag = asteroid_mining
		}
		days_re_enable = 30
		cost = 50
		fixed_random_seed = no
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_pull_large_asteroid_into_orbit"
			subtract_from_variable = { treasury = 10 }
			custom_effect_tooltip = 10_billion_expense_tt
		}
		days_remove = 180
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_pull_large_asteroid_into_orbit"
			hidden_effect = {
				random_list = {
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 50 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 63 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 77 }
					}
					25 = {
						add_to_variable = { global.asteroid_mineral_count = 100 }
					}
				}
			}
			custom_effect_tooltip = asteroid_mining_large_asteroid_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
	USA_expand_our_operations = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_country_flag = asteroid_mining
		}
		cost = 25
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_expand_our_operations"
			subtract_from_variable = { treasury = 3 }
			custom_effect_tooltip = 3_billion_expense_tt
		}
		days_remove = 30
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_expand_our_operations"
			capital_scope = {
				add_resource = {
					type = tungsten
					amount = 1
				}
				add_resource = {
					type = steel
					amount = 1
				}
			}
			add_to_variable = { asteroid_mining_rate = 1 }
			add_to_variable = { global.asteroid_mining_rate = 1 }
			custom_effect_tooltip = asteroid_mining_expand_operations_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
	USA_sell_mining_equipment = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_country_flag = asteroid_mining
		}
		available = {
			check_variable = { asteroid_mining_rate > 0 }
		}
		cost = 10
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_sell_mining_equipment"
			add_to_variable = { treasury = 1 }
			custom_effect_tooltip = 1_billion_revenue_tt
		}
		days_remove = 5
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_sell_mining_equipment"
			capital_scope = {
				add_resource = {
					type = tungsten
					amount = -1
				}
				add_resource = {
					type = steel
					amount = -1
				}
			}
			subtract_from_variable = { asteroid_mining_rate = 1 }
			subtract_from_variable = { global.asteroid_mining_rate = 1 }
			custom_effect_tooltip = asteroid_mining_sell_equipment_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
	USA_start_asteroid_mining = {
		icon = GFX_decision_asteroid_mining
		visible = {
			has_global_flag = asteroid_mining
			NOT = { has_country_flag = asteroid_mining }
		}
		cost = 50
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision USA_start_asteroid_mining"
			subtract_from_variable = { treasury = 30 }
			custom_effect_tooltip = 30_billion_expense_tt
		}
		days_remove = 60
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove USA_start_asteroid_mining"
			add_to_variable = { asteroid_mining_rate = 1 }
			add_to_variable = { global.asteroid_mining_rate = 1 }
			set_country_flag = asteroid_mining
			custom_effect_tooltip = asteroid_mining_start_tt
		}
		ai_will_do = {
			factor = 1
		}
	}
}
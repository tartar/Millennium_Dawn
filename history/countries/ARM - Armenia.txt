capital = 709
set_research_slots = 4
2000.1.1 = {
	set_country_flag = dynamic_flag
	capital = 709
	oob = "ARM_2000"
	set_cosmetic_tag = ARM
	set_convoys = 20

	add_ideas = {
		gdp_1
		#tax_cost_14
		economic_boom
		#pop_050
		orthodox_christian
		unrestrained_corruption
		defence_04
		edu_02
		health_02
		social_01
		bureau_02
		police_03
		semi_consumption_economy
		draft_army
		volunteer_women
		international_bankers
		landowners
		The_Clergy
		#civil_law
		officer_military_school
	}

	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 80 }
	add_to_array = { influence_array = SOV.id }
	add_to_array = { influence_array_val = 100 }
	add_to_array = { influence_array = PER.id }
	add_to_array = { influence_array_val = 30 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 19 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 2 }
	startup_influence = yes

	set_technology = {
		legacy_doctrines = 1
		armoured_mass_assault = 1
		deep_echelon_advance = 1
		army_group_operational_freedom = 1
		massed_artillery = 1

		#K-3 Rifle
		infantry_weapons = 1

		#N-2 MRLS
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1

		night_vision_1 = 1
		night_vision_2 = 1
		special_forces = 1
		support_weapons = 1

		#For templates

		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1
		MBT_1 = 1

		basic_computing = 1
		integrated_circuit = 1
		computing1 = 1
		decryption1 = 1
		encryption1 = 1
		radar = 1
		internet1 = 1 	#1G
		internet2 = 1 	#2G
		gprs = 1
		fuel_silos = 1
	}

	set_variable = { var = debt value = 1 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 14 }

	#set_variable = { var = size_modifier value = 0.08 } #1 CIC
	

	set_popularities = {
		democratic = 47.0
		communism = 52.4
		fascism = 0.0
		neutrality = 0.0
		nationalist = 0.6
	}
	set_politics = {
		ruling_party = communism
		last_election = "1998.3.16"
		election_frequency = 60
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0.177 } #conservatism
	set_variable = { party_pop_array^2 = 0.009 } #liberalism
	set_variable = { party_pop_array^3 = 0.284 } #socialism
	set_variable = { party_pop_array^4 = 0.01 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0.514 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0.004 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0.002 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist
	add_to_array = { ruling_party = 6 }
	startup_politics = yes

	set_autonomy = {
		target = NKR
		autonomy_state = autonomy_puppet_state
	}
	give_military_access = NKR
	give_guarantee = NKR


	create_country_leader = {
		name = "Robert Kocharyan"
		picture = "Rober_Kocharyan.dds"
		ideology = Conservative
		traits = {
			emerging_Conservative
		}
	}

	create_field_marshal = {
		name = "Movses Hakobyan"
		picture = "Portrait_Movses_Hakobyan.dds"
		traits = { old_guard organisational_leader }
		id = 241
		skill = 4
		attack_skill = 4
		defense_skill = 1
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Haykaz Papik Baghmanyan"
		picture = "Portrait_Haykaz_Baghmanyan.dds"
		traits = { bearer_of_artillery }
		id = 242
		skill = 3
		attack_skill = 5
		defense_skill = 3
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kamo Qochunts"
		picture = "Portrait_Kamo_Qochunts.dds"
		traits = { urban_assault_specialist }
		id = 243
		skill = 3
		attack_skill = 1
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 1
	}
}

2017.1.1 = {
	capital = 709
	oob = "ARM_2017"

	# Starting tech
	set_technology = {
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1

		SP_R_arty_1 = 1
		SP_R_arty_2 = 1

		SP_arty_1 = 1
		SP_arty_2 = 1

		body_armor_1980 = 1

		microprocessors = 1
		computing2 = 1
		decryption2 = 1
		encryption2 = 1
		computing3 = 1
		decryption3 = 1
		encryption3 = 1
		computing4 = 1
		decryption4 = 1
		encryption4 = 1
		neural_networks_revival = 1
		data_mining = 1
		industrial_electrospun_polymeric_nanofibers = 1
		stereolitography = 1
		DNA_fingerprinting = 1
		modern_gmo = 1
		gene_therapy = 1
		wifi = 1
		internet3 = 1	#3G
		edge = 1
		umts = 1
		hsupa = 1
		internet4 = 1	#4G
		lte = 1

		construction1 = 1
		excavation1 = 1
	}

	remove_ideas = {
		gdp_1
		#tax_cost_14
		economic_boom
	}

	add_ideas = {
		stable_growth
		gdp_3
		#tax_cost_23
	}

	set_country_flag = military_advisors_AFG
	AFG = { add_to_variable = { foreign_advisors = 1 } }

	#set_country_flag = gdp_3
	set_country_flag = positive_international_bankers
	set_country_flag = negative_landowners

	set_variable = { var = debt value = 6 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 2 }
	set_variable = { var = tax_rate value = 23 }

	#Nat focus



	set_popularities = {
		democratic = 46.4
		communism = 53.6
		fascism = 0.0
		neutrality = 0.0
		nationalist = 0.0
	}
	set_politics = {
		ruling_party = communism
		last_election = "2013.2.18"
		election_frequency = 60
		elections_allowed = yes
	}

	start_politics_input = yes

	set_variable = { party_pop_array^0 = 0 } #Western_Autocracy
	set_variable = { party_pop_array^1 = 0 } #conservatism
	set_variable = { party_pop_array^2 = 0.398 } #liberalism
	set_variable = { party_pop_array^3 = 0.066 } #socialism
	set_variable = { party_pop_array^4 = 0.007 } #Communist-State
	set_variable = { party_pop_array^5 = 0 } #anarchist_communism
	set_variable = { party_pop_array^6 = 0.529 } #Conservative
	set_variable = { party_pop_array^7 = 0 } #Autocracy
	set_variable = { party_pop_array^8 = 0 } #Mod_Vilayat_e_Faqih
	set_variable = { party_pop_array^9 = 0 } #Vilayat_e_Faqih
	set_variable = { party_pop_array^10 = 0 } #Kingdom
	set_variable = { party_pop_array^11 = 0 } #Caliphate
	set_variable = { party_pop_array^12 = 0 } #Neutral_Muslim_Brotherhood
	set_variable = { party_pop_array^13 = 0 } #Neutral_Autocracy
	set_variable = { party_pop_array^14 = 0 } #Neutral_conservatism
	set_variable = { party_pop_array^15 = 0 } #oligarchism
	set_variable = { party_pop_array^16 = 0 } #Neutral_Libertarian
	set_variable = { party_pop_array^17 = 0 } #Neutral_green
	set_variable = { party_pop_array^18 = 0 } #neutral_Social
	set_variable = { party_pop_array^19 = 0 } #Neutral_Communism
	set_variable = { party_pop_array^20 = 0 } #Nat_Populism
	set_variable = { party_pop_array^21 = 0 } #Nat_Fascism
	set_variable = { party_pop_array^22 = 0 } #Nat_Autocracy
	set_variable = { party_pop_array^23 = 0 } #Monarchist
	add_to_array = { ruling_party = 6 }
	startup_politics = yes

	create_country_leader = {
		name = "Serzh Sargsyan"
		picture = "serzh_sargsyan.dds"
		ideology = Conservative
		traits = {
			emerging_Conservative
		}
	}
}

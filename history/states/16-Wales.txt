state = {
	id = 16
	name = "STATE_16"
	manpower = 2769919
	state_category = state_04
	
	resources = {
		aluminium  = 18.000
	}
	
	history = {
		owner = ENG
		victory_points = { 377 5 } #Cardiff
		victory_points = { 407 1 } #Swansea
		victory_points = { 9364 1 } #Wrexham
		
		add_extra_state_shared_building_slots = 2
		buildings = {
			infrastructure = 5
			internet_station = 2
			industrial_complex = 2
			377 = {
				naval_base = 3
			}
		}
		add_core_of = ENG
		add_core_of = WAS
		2017.1.1 = {
			add_manpower = 333481
			buildings = {
				internet_station = 4
				industrial_complex = 3
			}
		}
	}

	provinces = {
		253 311 377 407 3274 6363 9364 11361
	}
}
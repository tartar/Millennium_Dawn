state = {
	id = 478
	name = "STATE_478"
	manpower = 888573
	state_category = state_01

	resources = {
		oil = 2
	}

	history = {
		owner = RAJ
		victory_points = {
			10090 1
		}
		buildings = {
			infrastructure = 1
			internet_station = 0
		}
		add_core_of = RAJ
		2017.1.1 = {
			add_manpower = 349678
			add_extra_state_shared_building_slots = 1
			buildings = {
				internet_station = 0
				industrial_complex = 1
			}
		}
	}

	provinces = {
		997 3999 7122 10090 9819
	}
}
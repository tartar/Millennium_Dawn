state = {
	id = 59
	name = "STATE_59"

	resources = {
		aluminium = 9
	}
	history = {
		owner = FRA
		victory_points = {
			3671 5
		}
		victory_points = {
			512 5
		}
		victory_points = {
			6688 1
		}
		victory_points = {
			651 1
		}
		buildings = {
			infrastructure = 6
			internet_station = 2
			industrial_complex = 2
			arms_factory = 1
			air_base = 2
			synthetic_refinery = 1

		}
		add_core_of = FRA
		2017.1.1 = {
			add_manpower = 126000
			buildings = {
				internet_station = 4
				industrial_complex = 2

			}
		}
	}

	provinces = {
		512 651 3655 3671 6518 6533 6547 6688 9492 9629 11631
	}
	manpower = 2450000
	buildings_max_level_factor = 1.000
	state_category = state_03
}

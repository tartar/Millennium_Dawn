﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "Algerian National Navy"
	   naval_base = 1145
		task_force = {
			name = "Algerian National Navy"
			location = 1145
			ship = { name = "El Hadj Slimane" definition = attack_submarine_mtg  start_experience_factor = 0.25 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ALG creator = SOV version_name = "Kilo Class" } } }
			ship = { name = "Rais Hadj Mubarek" definition = attack_submarine_mtg  start_experience_factor = 0.25 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ALG creator = SOV version_name = "Kilo Class" } } }

			ship = { name = "Mourad Rais" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_2 = { amount = 1 owner = ALG creator = SOV version_name = "Koni Class" } } }
			ship = { name = "Rais Kellik" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_2 = { amount = 1 owner = ALG creator = SOV version_name = "Koni Class" } } }
			ship = { name = "Rais Korfu" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_2 = { amount = 1 owner = ALG creator = SOV version_name = "Koni Class" } } }

			ship = { name = "Rais Hamidou" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_1 = { amount = 1 owner = ALG creator = SOV version_name = "Nanuchka Class" } } }
			ship = { name = "Salah Rais" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_1 = { amount = 1 owner = ALG creator = SOV version_name = "Nanuchka Class" } } }
			ship = { name = "Rais Ali" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_1 = { amount = 1 owner = ALG creator = SOV version_name = "Nanuchka Class" } } }

			ship = { name = "Djebel Chenoua" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_3 = { amount = 1 owner = ALG version_name = "Djebel Chenoua Class" } } }
			ship = { name = "El Chihab" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_3 = { amount = 1 owner = ALG version_name = "Djebel Chenoua Class" } } }
			ship = { name = "El Kirch" definition = corvette_mtg start_experience_factor = 0.25 equipment = { corvette_hull_3 = { amount = 1 owner = ALG version_name = "Djebel Chenoua Class" } } }
		}
	}
}

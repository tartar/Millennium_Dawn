﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "Marina de Guinea Ecuatorial"
	   naval_base = 1903
		task_force = {
			name = "Marina de Guinea Ecuatorial"
			location = 1903
			ship = { name = "Wele Nzas" definition = frigate_mtg start_experience_factor = 0.10 equipment = { frigate_hull_5 = { amount = 1 owner = EGU creator = BUL version_name = "Wele Nzas Class" } } }
			ship = { name = "Bata" definition = corvette_mtg start_experience_factor = 0.10 equipment = { corvette_hull_4 = { amount = 1 owner = EGU creator = UKR version_name = "Bata Class" } } }
		}
	}
}

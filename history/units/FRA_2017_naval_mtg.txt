﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "Groupe Aéronaval"
	   naval_base = 911 		#Toulon
		task_force = {
			name = "Groupe Aéronaval"
			location = 911
			ship = { name = "Charles de Gaulle (R91)" definition = carrier_mtg start_experience_factor = 0.65 equipment = { carrier_hull_3 = { amount = 1 owner = FRA version_name = "Charles de Gaulle Class" } } air_wings = { } }
			ship = { name = "Forbin (D620)" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_4 = { amount = 1 owner = FRA version_name = "Horizon Class" } } }
			ship = { name = "Cassard (D614)" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Cassard Class" } } }
			ship = { name = "Jean Bart (D615)" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Cassard Class" } } }
			ship = { name = "Montcalm (D642)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Georges Leygues Class" } } }
			ship = { name = "Guépratte" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "La Fayette Class" } } }
			ship = { name = "Émeraude (S604)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
		}
	}
	fleet = {
	   name = "Force d'action navale Méditérannée"
	   naval_base = 911 		#Toulon
		task_force = {
			name = "Force d'action navale Méditérannée"
			location = 911
			ship = { name = "Mistral" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = FRA version_name = "Mistral Class" } } air_wings = { } }
			ship = { name = "Tonnerre" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = FRA version_name = "Mistral Class" } } air_wings = { } }
			ship = { name = "Dixmude" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = FRA version_name = "Mistral Class" } } air_wings = { } }
			ship = { name = "Chevalier Paul" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_4 = { amount = 1 owner = FRA version_name = "Horizon Class" } } }
			ship = { name = "Jean de Vienne (D643)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Georges Leygues Class" } } }
			ship = { name = "La Fayette (F710)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "La Fayette Class" } } }
			ship = { name = "Surcouf (F711)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "La Fayette Class" } } }
			ship = { name = "Courbet (F712)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "La Fayette Class" } } }
			ship = { name = "Aconit (F713)" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "La Fayette Class" } } }
			ship = { name = "L'Adroit" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_4 = { amount = 1 owner = FRA version_name = "Gowind Class" } } }
			#Sold to Argentina in 2018
		}
	}
	 fleet = {
	   name = "Force d'action navale Atlantique"
	   naval_base = 3552 	#Brest
		task_force = {
			name = "Force d'action navale Atlantique"
			location = 3552
			ship = { name = "Aquitaine" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_4 = { amount = 1 owner = FRA version_name = "Aquitaine Class" } } }
			ship = { name = "Provence" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_4 = { amount = 1 owner = FRA version_name = "Aquitaine Class" } } }
			ship = { name = "Languedoc" definition = destroyer_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_4 = { amount = 1 owner = FRA version_name = "Aquitaine Class" } } }
			ship = { name = "Primauguet (D644)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Georges Leygues Class" } } }
			ship = { name = "La Motte-Picquet (D645)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Georges Leygues Class" } } }
			ship = { name = "Latouche-Tréville (D646)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { destroyer_hull_2 = { amount = 1 owner = FRA version_name = "Georges Leygues Class" } } }
				}
	}
	 fleet = {
		   name = "Zone maritime - Océan Indien"
		   naval_base = 13017 	#La Réunion
		task_force = {
			name = "Zone maritime - Océan Indien"
			location = 13017
			ship = { name = "Floréal (F730)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
			ship = { name = "Nivôse (F732)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
		}
	}
	fleet = {
		   name = "Zone maritime - Polynésie"
		   naval_base = 12148 	#Tahiti
		task_force = {
			name = "Zone maritime - Polynésie"
			location = 12148
			ship = { name = "Prairial (F731)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
		}
	}
	fleet = {
		   name = "Zone maritime - Caraïbes"
		   naval_base = 177 	#La Martinique
		task_force = {
			name = "Zone maritime - Caraïbes"
			location = 177
			ship = { name = "Ventôse (F733" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
			ship = { name = "Germinal (F735)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
		}
	}
	fleet = {
		   name = "Zone maritime - Pacifique"
		   naval_base = 12132 	#La Martinique
		task_force = {
			name = "Zone maritime - Pacifique"
			location = 12132
			ship = { name = "Vendémiaire (F734)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_3 = { amount = 1 owner = FRA version_name = "Floréal Class" } } }
		}
	}
	fleet = {
		name = "Escadrille des sous-marins nucléaires d'attaque"
	   naval_base = 911 		#Toulon
		task_force = {
			name = "Escadrille des sous-marins nucléaires d'attaque"
			location = 911
			ship = { name = "Rubis (S601)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
			ship = { name = "Saphir (S602)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
			ship = { name = "Casablanca (S603)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
			ship = { name = "Améthyste (S605)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
			ship = { name = "Perle (S606)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_2 = { amount = 1 owner = FRA version_name = "Rubis Class" } } }
		}
	}
	fleet = {
		name = "Force océanique stratégique"
	   naval_base = 3552 		#Ile Longue (Near Brest)
		task_force = {
			name = "Force océanique stratégique"
			location = 3552
			ship = { name = "Le Triomphant (S616)" definition = missile_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = FRA version_name = "Triomphant Class" } } }
			ship = { name = "Le Téméraire (S617)" definition = missile_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = FRA version_name = "Triomphant Class" } } }
			ship = { name = "Le Viligant (S618)" definition = missile_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = FRA version_name = "Triomphant Class" } } }
			ship = { name = "Le Terrible (S617)" definition = missile_submarine_mtg start_experience_factor = 0.65 equipment = { missile_submarine_hull_3 = { amount = 1 owner = FRA version_name = "Triomphant Class" } } }
		}
	}
	#fleet = {
	#	   name = "Egypt"
	#	   naval_base = 911 	#Toulon
		#task_force = {
		#	name = "Egypt"
		#	location = 911
		#	ship = { name = "Test 1" definition = destroyer start_experience_factor = 0.65 equipment = { missile_destroyer_4 = { amount = 1 owner = FRA version_name = " Class" version_name = "Mohammed VI-Class" } } }
		#}
	#}
}

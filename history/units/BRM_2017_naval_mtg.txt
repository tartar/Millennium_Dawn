﻿units = {

	### Naval OOB ###
	fleet = {
	   name = "Myanmar Navy"
	   naval_base = 1330
		task_force = {
			name = "Myanmar Navy"
			location = 1330
			ship = { name = "Kyan Sittha" definition = frigate_mtg start_experience_factor = 0.35 equipment = { frigate_hull_4 = { amount = 1 owner = BRM version_name = "Kyan Sittha Class" } } }
			ship = { name = "Sin Phyushin" definition = frigate_mtg start_experience_factor = 0.35 equipment = { frigate_hull_4 = { amount = 1 owner = BRM version_name = "Kyan Sittha Class" } } }
			ship = { name = "Aung Zeya" definition = frigate_mtg start_experience_factor = 0.35 equipment = { frigate_hull_4 = { amount = 1 owner = BRM version_name = "Kyan Sittha Class" } } }

			ship = { name = "Mahar Bandoola" definition = frigate_mtg start_experience_factor = 0.35 equipment = { frigate_hull_2 = { amount = 1 owner = BRM creator = CHI version_name = "Type 053 Class" } } }
			ship = { name = "Mahar Thiha Thura" definition = frigate_mtg start_experience_factor = 0.35 equipment = { frigate_hull_2 = { amount = 1 owner = BRM creator = CHI version_name = "Type 053 Class"  } } }

			ship = { name = "Anawratha" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_4 = { amount = 1 owner = BRM version_name ="Anawratha Class" } } }
			ship = { name = "Bayinnaung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_4 = { amount = 1 owner = BRM version_name ="Anawratha Class" } } }
			ship = { name = "Tabinshwehti" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_4 = { amount = 1 owner = BRM version_name ="Anawratha Class" } } }

			ship = { name = "MaGa" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }
			ship = { name = "SaitTra" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }
			ship = { name = "DuWa" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }
			ship = { name = "ZeyHta" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }
			ship = { name = "HanTha" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }
			ship = { name = "BanDa" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Class" } } }

			ship = { name = "Yan Htet Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Nyein Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Khwinn Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Min Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Ye Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Pang Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Win Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Aye Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
			ship = { name = "Yan Zwe Aung" definition = corvette_mtg start_experience_factor = 0.35 equipment = { corvette_hull_1 = { amount = 1 owner = BRM creator = CHI version_name = "Type 037 Hainan Class" } } }
		}
	}
}

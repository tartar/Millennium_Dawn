﻿units = {

	### Naval OOB ###
	 fleet = {
	   name = "56 Flotile Frigate"
	   naval_base = 657	#Constanta
		task_force = {
			name = "56 Flotile Frigate"
			location = 657
			ship = { name = "Regele Ferdinand" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = ROM creator = ENG version_name = "Type 22 Class" } } }
			ship = { name = "Regina Maria" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = ROM creator = ENG version_name = "Type 22 Class" } } }
			ship = { name = "Marasesti" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = ROM version_name = "Marasesti Class" } } }
		}
	}
	fleet = {
		name = "Flotile Submarin"
		naval_base = 657	#Constanta
		task_force = {
			name = "Flotile Submarin"
			location = 657 #Constanta
			ship = { name = "Delfinul" definition = attack_submarine_mtg start_experience_factor = 0.50 equipment = { attack_submarine_hull_2 = { amount = 1 owner = ROM creator = SOV version_name = "Kilo Class" } } }
		}
	}
	fleet = {
		name = "50 Escadrila Corvette"
		naval_base = 657	#Mangalia
		task_force = {
			name = "50 Escadrila Corvette"
			location = 657 #Mangalia
			ship = { name = "Vice-Amiral Eugeniu Roșca" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM version_name = "Tetal-I Class" } } }
			ship = { name = "Amiral Petre Barbuneanu" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM version_name = "Tetal-I Class" } } }
			ship = { name = "Contraamiral Eustațiu Sebastian" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM version_name = "Tetal-II Class" } } }
			ship = { name = "Contraamiral Horia Macellariu" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM version_name = "Tetal-II Class" } } }
			ship = { name = "Zborul" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM creator = SOV version_name = "Tarantul Class" } } }
			ship = { name = "Pescarusul (F-189)" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM creator = SOV version_name = "Tarantul Class" } } }
			ship = { name = "Lastunul" definition = corvette_mtg start_experience_factor = 0.50 equipment = { corvette_hull_2 = { amount = 1 owner = ROM creator = SOV version_name = "Tarantul Class" } } }
		}
	}
}

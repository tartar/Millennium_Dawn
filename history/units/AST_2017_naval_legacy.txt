﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Fleet Base East"
		naval_base = 12406
		task_force = {
			name = "Fleet Base East"
			location = 12406
				ship = { name = "HMAS ANZAC" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Parramatta" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Ballarat" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Darwin" definition = frigate start_experience_factor = 0.60 equipment = { frigate_2 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Melbourne" definition = frigate start_experience_factor = 0.60 equipment = { frigate_2 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Newcastle" definition = frigate start_experience_factor = 0.60 equipment = { frigate_2 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Adelaide" definition = LHA start_experience_factor = 0.60 equipment = { LHA_2 = { amount = 1 owner = AST } } air_wings = { } }
				ship = { name = "HMAS Canberra" definition = LHA start_experience_factor = 0.60 equipment = { LHA_2 = { amount = 1 owner = AST } } air_wings = { } }

		}
	}
	fleet = {
		name = "Fleet Base West"
		naval_base = 8228
		task_force = {
			name = "Fleet Base West"
			location = 8228
				ship = { name = "HMAS Collins" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Farncomb" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Waller" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Dechaineux" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Sheean" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Rankin" definition = diesel_attack_submarine start_experience_factor = 0.60 equipment = { diesel_attack_submarine_3 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Arunta" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Warramunga" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Stuart" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Toowoomba" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }
				ship = { name = "HMAS Perth" definition = frigate start_experience_factor = 0.60 equipment = { missile_frigate_1 = { amount = 1 owner = AST } } }

		}
	}
}

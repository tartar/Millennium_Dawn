﻿units = {

	### Naval OOB ###
	fleet = {
		name = "3rd Ship Flotilla"
		naval_base = 11805
		task_force = {
			name = "3rd Ship Flotilla"
			location = 11805
			ship = { name = "Tridente" definition = attack_submarine_mtg equipment = { attack_submarine_hull_4 = { amount = 1 owner = POR version_name = "Tridente Class - Standard Loadout" } } }
			ship = { name = "Arpão" definition = attack_submarine_mtg equipment = { attack_submarine_hull_4 = { amount = 1 owner = POR version_name = "Tridente Class - VLS Harpoon Loadout" } } }
			ship = { name = "Dom Francisco de Almeida" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = POR version_name = "Bartolomeu Dias Class" } } }
			ship = { name = "Bartolomeu Dias" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = POR version_name = "Bartolomeu Dias Class" } } }
			ship = { name = "Corte-Real" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = POR version_name = "Vasco da Gama Class" } } }
			ship = { name = "Alvares Cabral" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = POR version_name = "Vasco da Gama Class" } } }
			ship = { name = "Vasco da Gama" definition = frigate_mtg equipment = { frigate_hull_3 = { amount = 1 owner = POR version_name = "Vasco da Gama Class" } } }
		}
	}
}

﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Grupo de Proyección 'Alfa'"
		naval_base = 1048
		task_force = {
			name = "Grupo de Proyección 'Alfa'"
			location = 1048
			ship = { name = "SPS Príncipe de Asturias (R-11)" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_1 = { amount = 1 owner = SPR version_name = "Príncipe de Asturias Class" } } air_wings = { } }
			ship = { name = "SPS Galicia" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = SPR version_name = "Galicia Class" } } air_wings = { } }
			ship = { name = "SPS Castilla" definition = helicopter_operator_mtg start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = SPR version_name = "Galicia Class" } } air_wings = { } }
		}
	}
	fleet = {
		name = "41. Escuadrón de escolta"
		naval_base = 1048
		task_force = {
			name = "41. Escuadrón de escolta"
			location = 1048
			ship = { name = "SPS Santa Maria (F-81)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #1" } } }
			ship = { name = "SPS Victoria (F-82)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #1" } } }
			ship = { name = "SPS Numancia (F-83)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #1" } } }
			ship = { name = "SPS Reina Sofia (F-84)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #1" } } }
			ship = { name = "SPS Navarra (F-85)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #2" } } }
			ship = { name = "SPS Canarias (F-86)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_2 = { amount = 1 owner = SPR version_name = "Santa María Class - Batch #2" } } }
		}
	}
	fleet = {
		name = "31. Escuadrón de escolta"
		naval_base = 758
		task_force = {
			name = "31. Escuadrón de escolta"
			location = 758
			ship = { name = "SPS Baleares (F-71)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_1 = { amount = 1 owner = SPR version_name = "Baleares Class" } } }
			ship = { name = "SPS Andalucia (F-72)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_1 = { amount = 1 owner = SPR version_name = "Baleares Class" } } }
			ship = { name = "SPS Cataluna (F-73)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_1 = { amount = 1 owner = SPR version_name = "Baleares Class" } } }
			ship = { name = "SPS Asturias (F-74)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_1 = { amount = 1 owner = SPR version_name = "Baleares Class" } } }
			ship = { name = "SPS Extremadura (F-75)" definition = frigate_mtg start_experience_factor = 0.65 equipment = { frigate_hull_1 = { amount = 1 owner = SPR version_name = "Baleares Class" } } }
		}
	}
	fleet = {
		name = "Flotilla submarina"
		naval_base = 6906
		task_force = {
			name = "Flotilla submarina"
			location = 6906
			ship = { name = "SPS Delfin (S-61)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SPR version_name = "Delfín Class" } } }
			ship = { name = "SPS Tonina (S-62)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SPR version_name = "Delfín Class" } } }
			ship = { name = "SPS Marsopa (S-63)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SPR version_name = "Delfín Class" } } }
			ship = { name = "SPS Narval (S-64)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_1 = { amount = 1 owner = SPR version_name = "Delfín Class" } } }
			ship = { name = "SPS Galerna (S-71)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = SPR version_name = "Galerna Class" } } }
			ship = { name = "SPS Siroco (S-72)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = SPR version_name = "Galerna Class" } } }
			ship = { name = "SPS Mistral (S-73)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = SPR version_name = "Galerna Class" } } }
			ship = { name = "SPS Tramontana (S-74)" definition = attack_submarine_mtg start_experience_factor = 0.65 equipment = { attack_submarine_hull_2 = { amount = 1 owner = SPR version_name = "Galerna Class" } } }
		}
	}
	fleet = {
		name = "Flota de patrulla artesanal"
		naval_base = 6906
		task_force = {
			name = "Flota de patrulla artesanal"
			location = 6906
			ship = { name = "SPS Descubierta (F-31)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Surface Warfare Batch" } } }
			ship = { name = "SPS Diana (F-31)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Surface Warfare Batch" } } }
			ship = { name = "SPS Infanta Elena (F-32)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Surface Warfare Batch" } } }
			ship = { name = "SPS Infanta Cristina (F-33)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Surface Warfare Batch" } } }
			ship = { name = "SPS Cazadora (F-34)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Anti-Submarine Batch" } } }
			ship = { name = "SPS Vencedora (F-35)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = SPR version_name = "Descubierta Class - Anti-Submarine Batch" } } }
			ship = { name = "SPS Chilreu (P-61)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_1 = { amount = 1 owner = SPR version_name = "Chilreu Patrol Class" } } }
			ship = { name = "SPS Serviola (P-71)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = SPR version_name = "Serviola Patrol Class" } } }
			ship = { name = "SPS Centinela (P-72)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = SPR version_name = "Serviola Patrol Class" } } }
			ship = { name = "SPS Vigia (P-73)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = SPR version_name = "Serviola Patrol Class" } } }
			ship = { name = "SPS Atalaya (P-74)" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_3 = { amount = 1 owner = SPR version_name = "Serviola Patrol Class" } } }
		}
	}
}

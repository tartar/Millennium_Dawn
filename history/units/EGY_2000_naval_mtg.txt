﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Ustul Albahr Al'abyad Almutawasit"		#Mediterranean fleet
		naval_base = 4076
		task_force = {
			name = "Ustul Albahr Al'abyad Almutawasit"		#Mediterranean fleet
			location = 4076
			ship = { name = "Sharm El-Sheikh" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = USA version_name = "Oliver Hazard Perry Class" } } }
			ship = { name = "Oliver Hazard Perry" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = USA version_name = "Oliver Hazard Perry Class" } } }
			ship = { name = "Domyat" definition = frigate_mtg start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = EGY creator = USA version_name = "Knox Class" } } }
			ship = { name = "Najim Al Zafer" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = CHI version_name = "Type 053H Class" } } }
			ship = { name = "El Suez" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = EGY version_name = "El Suez Class" } } }
		}
	}
	fleet = {
		name = "Ustul Albahr Al'ahmar"		#Red Sea fleet
		naval_base = 4073
		task_force = {
			name = "Ustul Albahr Al'ahmar"		#Red Sea fleet
			location = 4073
			ship = { name = "Alexandria" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = USA version_name = "Oliver Hazard Perry Class" } } }
			ship = { name = "Taba" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = USA version_name = "Oliver Hazard Perry Class" } } }
			ship = { name = "Rashid" definition = frigate_mtg start_experience_factor = 0.40 equipment = { frigate_hull_1 = { amount = 1 owner = EGY creator = USA version_name = "Knox Class" } } }
			ship = { name = "Al Nasser" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = EGY creator = CHI version_name = "Type 053H Class" } } }
			ship = { name = "About Qir" definition = corvette_mtg start_experience_factor = 0.65 equipment = { corvette_hull_2 = { amount = 1 owner = EGY version_name = "El Suez Class" } } }
		}
	}
	fleet = {
		name = "Ustul Alghawwasat"		#Submarine fleet
		naval_base = 4076
		task_force = {
			name = "Ustul Alghawwasat"		#Submarine fleet
			location = 4076
			ship = { name = "849" definition = attack_submarine_mtg start_experience_factor = 0.20 equipment = { attack_submarine_hull_1 = { amount = 1 owner = EGY creator = SOV version_name = "Romeo Class" } } }
			ship = { name = "852" definition = attack_submarine_mtg start_experience_factor = 0.20 equipment = { attack_submarine_hull_1 = { amount = 1 owner = EGY creator = SOV version_name = "Romeo Class" } } }
			ship = { name = "855" definition = attack_submarine_mtg start_experience_factor = 0.20 equipment = { attack_submarine_hull_1 = { amount = 1 owner = EGY creator = SOV version_name = "Romeo Class" } } }
			ship = { name = "858" definition = attack_submarine_mtg start_experience_factor = 0.20 equipment = { attack_submarine_hull_1 = { amount = 1 owner = EGY creator = SOV version_name = "Romeo Class" } } }
		}
	}
}

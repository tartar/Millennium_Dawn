﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Peruvian Navy"
		naval_base = 12997
		task_force = {
			name = "Peruvian Navy"
			location = 12997

			ship = { name = "BAP Angamos" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "BAP Antofagasta" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "BAP Pisagua" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "BAP Chipana" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "BAP Islay" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "BAP Arica" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = PRU creator = GER version_name = "Type 209 Class" } } }

			ship = { name = "BAP Almirante Grau" definition = cruiser_mtg start_experience_factor = 0.40  equipment = { cruiser_hull_1 = { amount = 1 owner = PRU creator = HOL version_name = "De Zeven Provincien Class" } } }

			ship = { name = "BAP Pisco" definition = helicopter_operator_mtg equipment = { helicopter_operator_hull_2 = { amount = 1 owner = PRU creator = KOR version_name = "Makassar Class" } } air_wings = { } }

			ship = { name = "BAP Villavisencio" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }
			ship = { name = "BAP Montero" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }
			ship = { name = "BAP Mariátegui" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }

			ship = { name = "BAP Aguirre" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }
			ship = { name = "BAP Palacios" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }
			ship = { name = "BAP Bolognesi" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }
			ship = { name = "BAP Quiñones" definition = frigate_mtg start_experience_factor = 0.40  equipment = { frigate_hull_1 = { amount = 1 owner = PRU creator = ITA version_name = "Lupo Class" } } }

			ship = { name = "BAP Velarde" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }
			ship = { name = "BAP Santillana" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }
			ship = { name = "BAP De los Heros" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }
			ship = { name = "BAP Herrera" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }
			ship = { name = "BAP Larrea" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }
			ship = { name = "BAP Sánchez Carrión" definition = corvette_mtg equipment = { corvette_hull_2 = { amount = 1 owner = PRU creator = FRA version_name = "PR-72P Class" } } }


		}
	}
}

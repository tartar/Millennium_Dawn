﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Colombian Navy"
		naval_base = 12703
		task_force = {
			name = "Colombian Navy"
			location = 12703
			ship = { name = "ARC Almirante Padilla" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Almirante Padilla Class" } } }
			ship = { name = "ARC Caldas" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Almirante Padilla Class" } } }
			ship = { name = "ARC Antioquia" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Almirante Padilla Class" } } }
			ship = { name = "ARC Independiente" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Almirante Padilla Class" } } }
			ship = { name = "ARC Nariño" definition = frigate_mtg start_experience_factor = 0.50 equipment = { frigate_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Almirante Padilla Class" } } }
		}
	}
	fleet = {
		name = "Submarine Squadrons"
		naval_base = 12703
		task_force = {
			name = "Submarine Squadrons"
			location = 12703
			ship = { name = "ARC Pijao" definition = attack_submarine_mtg start_experience_factor = 0.50 equipment = { attack_submarine_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "ARC Tayrona" definition = attack_submarine_mtg start_experience_factor = 0.50 equipment = { attack_submarine_hull_2 = { amount = 1 owner = COL creator = GER version_name = "Type 209 Class" } } }
		}
	}
}

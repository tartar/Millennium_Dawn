﻿units = {

	### Naval OOB ###
	 fleet = {
		name = "Surface Action Group"
		naval_base = 11811
		task_force = {
			name = "Surface Action Group"
			location = 11811
			#Oliver Hazard Perry-Class
			ship = { name = "TCG Gaziantep" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Giresun" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Gemlik" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Gökçeada" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Gediz" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Gökova" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			ship = { name = "TCG Göksu" definition = frigate_mtg start_experience_factor = 0.60 equipment = { frigate_hull_2 = { amount = 1 owner = TUR version_name = "Gabya Class" } } }
			#Barbaros-Class
			ship = { name = "TCG Barbaros" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_3 = { amount = 1 owner = TUR version_name = "Barbados Class" } } }
			ship = { name = "TCG Oruçreis" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_3 = { amount = 1 owner = TUR version_name = "Barbaros Class" } } }
			ship = { name = "TCG Salihreis" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_3 = { amount = 1 owner = TUR version_name = "Barbaros Class" } } }
			ship = { name = "TCG Kemalreis" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_3 = { amount = 1 owner = TUR version_name = "Barbaros Class" } } }
			#Yavuz class
			ship = { name = "TCG Yavuz" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Yavuz Class" } } }
			ship = { name = "TCG Turgutreis" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Yavuz Class" } } }
			ship = { name = "TCG Fatih" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Yavuz Class" } } }
			ship = { name = "TCG Yıldırım" definition = frigate_mtg start_experience_factor = 0.30 equipment = { frigate_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Yavuz Class" } } }
			#Ada-class
			ship = { name = "TCG Heybeliada" definition = corvette_mtg start_experience_factor = 0.30 equipment = { corvette_hull_4 = { amount = 1 owner = TUR version_name = "Ada Class" } } }
			ship = { name = "TCG Büyükada" definition = corvette_mtg start_experience_factor = 0.30 equipment = { corvette_hull_4 = { amount = 1 owner = TUR version_name = "Ada Class" } } }
			ship = { name = "TCG Burgazada" definition = corvette_mtg start_experience_factor = 0.30 equipment = { corvette_hull_4 = { amount = 1 owner = TUR version_name = "Ada Class" } } }
			#Burak-class
			ship = { name = "TCG Bozcaada" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }
			ship = { name = "TCG Bodrum" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }
			ship = { name = "TCG Bandırma" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }
			ship = { name = "TCG Beykoz" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }
			ship = { name = "TCG Bartın" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }
			ship = { name = "TCG Bafra" definition = corvette_mtg start_experience_factor = 0.60 equipment = { corvette_hull_2 = { amount = 1 owner = TUR creator = FRA version_name = "D'Estienne d'Orves Class" } } }

		}
	}
	 fleet = {
		name = "Submarine Group"
		naval_base = 11811
		task_force = {
			name = "Submarine Group"
			location = 11811
			#Type 209 Atlay-Class
			ship = { name = "TCG Batıray" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "TCG Yıldıray" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "TCG Doğanay" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class" } } }
			ship = { name = "TCG Dolunay" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class" } } }
			#Preveze class
			ship = { name = "TCG Preveze" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Preveze Variant" } } }
			ship = { name = "TCG Sakarya" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Preveze Variant" } } }
			ship = { name = "TCG 18 Mart" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Preveze Variant" } } }
			ship = { name = "TCG Anafartalar" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Preveze Variant" } } }
			#Gür-class
			ship = { name = "TCG Gür" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Gür Variant" } } }
			ship = { name = "TCG Çanakkale" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Gür Variant" } } }
			ship = { name = "TCG Burakreis" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Gür Variant" } } }
			ship = { name = "TCG I. İnönü" definition = attack_submarine_mtg start_experience_factor = 0.40 equipment = { attack_submarine_hull_2 = { amount = 1 owner = TUR creator = GER version_name = "Type 209 Class - Gür Variant" } } }

		}
	}
}
